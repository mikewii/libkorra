import struct

# Define the format of the Header struct
header_format = "<IB3sI3I"

# Define the size of the Header struct in bytes
header_size = struct.calcsize(header_format)

# Define the format of the SaveSlot struct
slot_format = "<32s544x4x1s3x"

# Define the size of the SaveSlot struct in bytes
slot_size = struct.calcsize(slot_format)

# Open the file containing the Header and SaveSlot structs
with open("/home/data/git/libkorra/file.bin", "rb") as f:
    # Read the bytes of the Header struct
    header_bytes = f.read(header_size)

    # Unpack the bytes of the Header struct into a tuple using the defined format
    header = struct.unpack(header_format, header_bytes)

    # Find the index of the first non-zero value in the isSlotUsed field of the Header tuple
    selected_slot_index = header[1:].index(b'\x01') if b'\x01' in header[1:] else None

    # If all values in the isSlotUsed field are zero, assume the first slot is selected
    if selected_slot_index is None:
        selected_slot_index = 0

    # Calculate the offset of the selectedSlot field in the Header tuple
    selected_slot_offset = struct.calcsize("<IB") + selected_slot_index

    # Seek to the offset of the selectedSlot field in the file
    f.seek(selected_slot_offset)

    # Read the relative position of the selected slot
    selected_slot_pos = header[6 + selected_slot_index] if selected_slot_index is not None else 0

    # Seek to the position of the selected slot in the file
    f.seek(selected_slot_pos)

    # Read the bytes of the selected slot struct
    selected_slot_bytes = f.read(slot_size)

# Unpack the bytes of the selected slot struct into a tuple using the defined format
selected_slot = struct.unpack(slot_format, selected_slot_bytes)

# Extract the name and gender fields from the selected slot tuple
name, gender = selected_slot[0].decode("ascii").rstrip("\0"), selected_slot[1][0]

# Print the values of the Header and selected slot
print(f"Header version: {header[0]}")
print(f"Header pData: {header[4]}")
print(f"Header pFlags: {header[5]}")
print(f"Header pSaveSlot: {header[6:]}")

# If the selected slot index is known, print the selected slot name and gender
if selected_slot_index is not None:
    print(f"Selected slot name: {name}")
    print(f"Selected slot gender: {'Male' if gender == 0 else 'Female'}")
else:
    print("No slot is selected.")
