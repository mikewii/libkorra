#include "enemyinfo.hpp"
#include "ui/ui_enemyinfo.h"

EnemyInfo::EnemyInfo(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EnemyInfo)
{
    ui->setupUi(this);

    connect(ui->numberBase, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &EnemyInfo::onBaseChanged);
}

EnemyInfo::~EnemyInfo()
{
    delete ui;
}

void EnemyInfo::setInfo(const mh::mh4u::sEnemy_s &info)
{
    ui->id->setValue(info.ID);
    ui->name->setText(mh::mh4u::sEnemy::GetStr(info.ID));
    ui->respawnCount->setText(QString::number(info.RespawnCount, 10));
    ui->spawnArea->setText(QString::number(info.SpawnArea, 10));

    ui->posX->setValue(info.Position.X);
    ui->posY->setValue(info.Position.Y);
    ui->posZ->setValue(info.Position.Z);
}

void EnemyInfo::onBaseChanged(int index)
{
    if (index == 0) {
        ui->id->setDisplayIntegerBase(10);
        ui->id->setPrefix("");
    } else {
        ui->id->setDisplayIntegerBase(16);
        ui->id->setPrefix("0x");
    }
}
