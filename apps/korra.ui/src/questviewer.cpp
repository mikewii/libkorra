#include "questviewer.hpp"
#include "ui_questviewer.h"

#include "enemyinfo.hpp"

#include <QFileDialog>
#include <QMessageBox>
#include <QGroupBox>

#include <libkorra/tools/ccontainer.hpp>
#include <libkorra/MH4U/Quest/Quest.hpp>

QuestViewer::QuestViewer(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QuestViewer)
{
    ui->setupUi(this);

    setWindowTitle("Quest viewer");

    connect(ui->openQuestFile, &QPushButton::clicked,
            this, &QuestViewer::onOpenQuestFile);
}

QuestViewer::~QuestViewer()
{
    delete ui;
}

void QuestViewer::onOpenQuestFile(bool /*checked*/)
{
    QString path = QFileDialog::getOpenFileName(this, tr("Open quest file"));

    if (path.isEmpty())
        return popupMessage("Path is empty", true);
    else if (!openQuest(path))
        return popupMessage("Failed to open!\nNot a quest file", true);
}

void QuestViewer::popupMessage(const QString &message, const bool error) const
{
    QMessageBox popup;

    popup.setStandardButtons(QMessageBox::Ok);
    popup.setDefaultButton(QMessageBox::Ok);

    if (error) {
        popup.setWindowTitle("Error");
        popup.setIcon(QMessageBox::Critical);
    } else {
        popup.setWindowTitle("Info");
        popup.setIcon(QMessageBox::Information);
    }

    popup.setText(message);

    popup.exec();
}

bool QuestViewer::openQuest(const QString &path)
{
    if (!mh::mh4u::sQuest::isQuestFile(path.toStdString()))
        return false;

    mh::tools::CContainer in(path.toStdString());

    mh::mh4u::sQuest* quest = reinterpret_cast<mh::mh4u::sQuest*>(in.data());

    /// General
    addGeneralInfo(quest);

    /// text
    addText(quest);

    /// enemies
    auto clearTable = [&](QTableWidget* table) { table->clearContents(); };
    auto resizeAndLabelTable = [&](QTableWidget* table) {
        if (table->verticalHeader()->count() > 0) {
            QStringList hlabels;

            for(auto i = 0; i <table->verticalHeader()->count(); i++)
                hlabels.append(QStringLiteral("Wave %1").arg(i + 1));

            table->setVerticalHeaderLabels(hlabels);
        }

        table->resizeColumnsToContents();
        table->resizeRowsToContents();
    };

    clearTable(ui->enemyLarge);
    clearTable(ui->enemySmall);

    addLargeEnemies(quest);
    addSmallEnemies(quest);

    resizeAndLabelTable(ui->enemyLarge);
    resizeAndLabelTable(ui->enemySmall);

    return true;
}

void QuestViewer::addGeneralInfo(mh::mh4u::sQuest *quest)
{
    ui->general->setInfo(quest);
}

void QuestViewer::addText(mh::mh4u::sQuest* quest)
{
    std::map<mh::mh4u::QuestLanguage, QuestText*> textMap = {
        { mh::mh4u::QuestLanguage::JAPANESE, ui->textMulti3 },
        { mh::mh4u::QuestLanguage::FRENCH, ui->textFrench },
        { mh::mh4u::QuestLanguage::SPANISH, ui->textSpanish },
        { mh::mh4u::QuestLanguage::GERMAN, ui->textGerman },
        { mh::mh4u::QuestLanguage::ITALIAN, ui->textItalian },
    };

    std::array<mh::mh4u::Text, 7> choices = {
        mh::mh4u::Text::TITLE,
        mh::mh4u::Text::MAIN_GOAL,
        mh::mh4u::Text::FAILURE,
        mh::mh4u::Text::SUMMARY,
        mh::mh4u::Text::MAIN_MONSTER,
        mh::mh4u::Text::CLIENT,
        mh::mh4u::Text::SUB_QUEST
    };

    foreach (const auto& map, textMap) {
        foreach (const auto& choice, choices) {
            QString text;

            text.append(quest->get_text(map.first, choice));

            map.second->setText(choice, text);
        }
    }
}

void QuestViewer::addLargeEnemies(mh::mh4u::sQuest *quest)
{
    QTableWidget* table = ui->enemyLarge;
    int row = 0; // wave

    foreach (const auto& wave, quest->getLargeEnemyVector()) {
        QWidget* container = new QWidget(table);
        QVBoxLayout* vbox = new QVBoxLayout(container);

        foreach (const auto& enemy, wave) {
            EnemyInfo* info = new EnemyInfo(this);

            info->setInfo(enemy);

            vbox->addWidget(info);
        }

        container->setLayout(vbox);

        if (table->rowCount() <= row)
            table->insertRow(row);

        table->setCellWidget(row, 0, container);

        ++row;
    }
}

void QuestViewer::addSmallEnemies(mh::mh4u::sQuest *quest)
{
    QTableWidget* table = ui->enemySmall;
    int row = 0; // wave

    foreach (const auto& wave, quest->getSmallEnemyVector()) {
        QWidget* container = new QWidget(table);
        QVBoxLayout* vbox = new QVBoxLayout(container);

        foreach (const auto& group, wave) {
            foreach (const auto& enemy, group) {
                EnemyInfo* info = new EnemyInfo(this);

                info->setInfo(enemy);

                vbox->addWidget(info);
            }
        }

        container->setLayout(vbox);

        if (table->rowCount() <= row)
            table->insertRow(row);

        table->setCellWidget(row, 0, container);

        ++row;
    }
}
