#include "gui_lmd.h"
#include "ui_gui_lmd.h"

#include <QFileDialog>
#include <QFile>
#include <QTextStream>

#include <libkorra/tools/file.hpp>

gui_lmd::gui_lmd(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::gui_lmd)
{
    ui->setupUi(this);
    lock();
}

gui_lmd::~gui_lmd()
{
    if (lmd)
    {
        delete lmd;
        lmd = nullptr;
    }

    delete ui;
}

void gui_lmd::open(const QString &path)
{
    mh::tools::CContainer container;

    const auto& res = mh::tools::File::file_to_cc(  path.toStdString()
                                                  , container
                                                  , mh::mh4u::lmd::Header::MAGIC);

    if (res) {
        QFileInfo info(path);

        filepath = path;
        const auto& filename = info.fileName();

        setWindowTitle(filename); // fromStdString for windows compatibility

        lmd = new mh::mh4u::lmd::cLMD(container);

        const auto& vStrings = lmd->Get_U16String_vector();

        original_strings.reserve(vStrings.size());
        replaced_strings.resize(vStrings.size());

        for (const auto& str : vStrings)
            original_strings.push_back(converter.to_bytes(str).c_str());

        populate();
        unlock();
    }
    else return; // notify
}

void gui_lmd::on_btn_Open_clicked(void)
{
    const QString fileName = QFileDialog::getOpenFileName(this, tr("Open .lmd File"));

    if (!fileName.isEmpty())
    {
        clear();
        open(fileName);
    }
}

void gui_lmd::populate(void)
{
    ui->list_Strings->addItems(original_strings);
    ui->list_Strings->item(0)->setSelected(true);
    ui->text_Original->setText(converter.to_bytes(lmd->Get_U16String_vector().at(0)).c_str());
}

void gui_lmd::lock(void) const
{
    ui->btn_Save->setEnabled(false);
    ui->btn_SaveAs->setEnabled(false);
    ui->btn_Export->setEnabled(false);
}

void gui_lmd::unlock(void) const
{
    ui->btn_Save->setEnabled(true);
    ui->btn_SaveAs->setEnabled(true);
    ui->btn_Export->setEnabled(true);
    ui->list_Strings->setCurrentRow(0);
}

void gui_lmd::clear(void)
{
    if (lmd)
    {
        delete lmd;
        lmd = nullptr;
    }

    if (!original_strings.isEmpty())
        original_strings.clear();

    if (!replaced_strings.isEmpty())
        replaced_strings.clear();

    ui->list_Strings->clear();
}

void gui_lmd::save(const QString &path)
{
    const auto& savepath = path.isEmpty() ? filepath : path;

    for (auto i = 0; i < replaced_strings.size(); i++)
    {
        const auto& str = replaced_strings.at(i).toStdU16String();

        if (!str.empty())
            lmd->replace_String(str, i);
    }

    mh::tools::CContainer container;

    lmd->write(container);
    save_change_color();

    mh::tools::File::CC_To_File(  savepath.toStdString()
                                , container);
}

void gui_lmd::save_change_color(void)
{
    for (auto i = 0; i < ui->list_Strings->count(); i++)
    {
        const auto& item    = ui->list_Strings->item(i);
        const auto& bkg     = item->background();

        if (bkg == Qt::GlobalColor::yellow)
            item->setBackground(Qt::GlobalColor::green);
    }
}


void gui_lmd::on_list_Strings_itemSelectionChanged(void)
{
    const auto id = ui->list_Strings->currentIndex().row();

    if (id != -1 && lmd) // TODO: lock instead of checking for lmd
    {
        ui->text_Original->setText(converter.to_bytes(lmd->Get_U16String_vector().at(id)).c_str());
        ui->text_Replace->setText(replaced_strings.at(id));
    }
}


void gui_lmd::on_text_Replace_textChanged(void)
{
    const auto& text = ui->text_Replace->toPlainText();
    const auto& id = ui->list_Strings->currentIndex().row();

    if (!text.isEmpty() && id != -1)
    {
        ui->list_Strings->item(id)->setText(text);
        replaced_strings.replace(id, text);
        ui->list_Strings->item(id)->setBackground(Qt::GlobalColor::yellow);
    }
    else
    {
        if (id != -1)
        {
            ui->list_Strings->item(id)->setText(converter.to_bytes(lmd->Get_U16String_vector().at(id)).c_str());
            ui->list_Strings->item(id)->setBackground(QBrush());
        }
    }
}


void gui_lmd::on_btn_Save_clicked(void)
{
    save();
}

void gui_lmd::on_btn_SaveAs_clicked(void)
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save file as"));

    save(fileName);
}

void gui_lmd::on_btn_Export_clicked(void)
{
    QFileInfo info(filepath);
    QString newName = info.path() + info.baseName() + ".txt";
    QFile file(newName);

    file.open(QIODevice::WriteOnly | QIODevice::Truncate);

    if (!file.isOpen())
        return;

    QTextStream stream(&file);

    for (const auto& text : qAsConst(original_strings)) {
        stream << '"' << text << "\",";
        stream << '\n';
    }
}

