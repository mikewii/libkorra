#include "qspinbox2.hpp"

QSpinBox2::QSpinBox2(QWidget *parent)
    : QSpinBox(parent)
{
}

QString QSpinBox2::textFromValue(int val) const
{
    QString text = QSpinBox::textFromValue(val);

    return text.toUpper();
}
