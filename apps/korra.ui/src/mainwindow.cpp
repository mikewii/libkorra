#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "gui_lmd.h"
#include "questviewer.hpp"

#include <QFileDialog>
#include <QStandardItemModel>
#include <QMessageBox>

#include <libkorra/tools/file.hpp>
#include <libkorra/tools/folder.hpp>
#include <libkorra/MH4U/Crypto.hpp>

QList<QStandardItem *> itemList;


QStandardItemModel model;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    for (auto* item : qAsConst(itemList))
    {
        if (item)
        {
            delete item;
            item = nullptr;
        }
    }

    for (auto* item : qAsConst(this->gui))
    {
        if (item)
        {
            delete item;
            item = nullptr;
        }
    }

    delete ui;
}

void MainWindow::on_btn_arc_open_clicked(void)
{
    QMessageBox msg;
    QString     path = QFileDialog::getOpenFileName(this, tr("Open File"));

    if (path.isEmpty())
        return;

    m_arc.clear();
    m_arc.set_arc_path(path.toStdString());

    if (!m_arc.read_header()) {
        msg.critical(nullptr, "Error", "Not an ARC file, or version is not supported!");
        this->ui->btn_arc_extract->setEnabled(false);
        return;
    }

    this->ui->btn_arc_extract->setEnabled(true);


    return;


    //m_arc_files.reserve(header.files_ammount);






//    if (fileName.isEmpty())
//        return;

//    QFile file(fileName);

//    File::file_to_cc(fileName.toStdString(), File::oneFile);

//    std::vector<Pair> vector;

//    ARC arc(File::oneFile, vector);

//    arc.ExtractAll();

//    itemList.clear();
//    itemList.reserve(vector.size());
//    model.clear();
//    for (const auto& pair : vector)
//    {
//        QStandardItem* item = new  QStandardItem;

//        item->setText(pair.info.Filename);

//        itemList.push_back(item);
//    }

//    for (auto i = 0; i < itemList.size(); i++)
//    {
//        model.setItem(i, 0, itemList.at(i));
//    }

//    this->ui->listArcFilelist->setModel(&model);
//    this->ui->btn_arc_extract->setEnabled(true);
}


void MainWindow::on_btn_lmdEditor_clicked(void)
{
    gui_lmd* editor = new gui_lmd(this);

    this->gui.push_back(editor);

    editor->setWindowFlag(Qt::WindowType::Window);
    editor->show();
}

void MainWindow::on_btn_arc_extract_clicked(void)
{
    QMessageBox msg;

    auto res = m_arc.decompress_all();

    const auto& out_path = m_arc.get_out_path();

    msg.information(nullptr, "Completed", QString("ARC have been extracted at:\n") + QString::fromStdString(out_path));
}

void MainWindow::on_btn_arc_create_clicked(void)
{
    QFileDialog::Options ops = QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks;
    QMessageBox msg;
    QString     path = QFileDialog::getExistingDirectory(this, tr("Open Directory"), nullptr, ops);

    if (path.isEmpty())
        return;

    m_arc.clear();

    m_arc.make_arc(path.toStdString());

}

void MainWindow::on_mh4uEditorsMaster_clicked(void)
{
    QuestViewer widget(this);

    widget.exec();
}

