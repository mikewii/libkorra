#include "questtext.hpp"
#include "ui_questtext.h"

QuestText::QuestText(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::QuestText)
{
    ui->setupUi(this);
}

QuestText::~QuestText()
{
    delete ui;
}

void QuestText::setText(const mh::mh4u::Text type, const QString &text)
{
    switch (type) {
    default:break;
    case mh::mh4u::Text::TITLE:
        return ui->title->setText(text);
    case mh::mh4u::Text::MAIN_GOAL:
        return ui->mainObjective->setPlainText(text);
    case mh::mh4u::Text::FAILURE:
        return ui->failure->setText(text);
    case mh::mh4u::Text::SUMMARY:
        return ui->summary->setPlainText(text);
    case mh::mh4u::Text::MAIN_MONSTER:
        return ui->mainMonsters->setPlainText(text);
    case mh::mh4u::Text::CLIENT:
        return ui->client->setText(text);
    case mh::mh4u::Text::SUB_QUEST:
        return ui->subQuest->setPlainText(text);
    }
}
