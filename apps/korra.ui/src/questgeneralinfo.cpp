#include "questgeneralinfo.hpp"
#include "ui_questgeneralinfo.h"

#include <libkorra/MH4U/Quest/Quest.hpp>

QuestGeneralInfo::QuestGeneralInfo(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::QuestGeneralInfo)
{
    ui->setupUi(this);

    connect(ui->base, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &QuestGeneralInfo::onBaseChanged);
}

QuestGeneralInfo::~QuestGeneralInfo()
{
    delete ui;
}

void QuestGeneralInfo::setInfo(const mh::mh4u::sQuest *quest)
{
    const auto* sFlags = quest->get_sFlags();

    if (!sFlags)
        return;

    ui->questTime->setText(QString::number(sFlags->getQuestTimeMinutes(), 10));

    ui->questId->setValue(sFlags->getQuestId());

    setType0(sFlags);
    setType1(sFlags);
    setZenny(sFlags);
}

void QuestGeneralInfo::setType0(const mh::mh4u::sFlags* sFlags)
{
    QString type0;
    foreach(const auto* str, mh::mh4u::sQuestType::GetStrVec(sFlags->getQuestType()))
        type0 += "[" + QString(str) + "] ";

    ui->questType0->setText(type0);
}

void QuestGeneralInfo::setType1(const mh::mh4u::sFlags* sFlags)
{
    QString type1;
    foreach(const auto* str, mh::mh4u::sQuestFlags0::GetStrVec(sFlags->getFlags0()))
        type1 += "[" + QString(str) + "] ";

    ui->questType1->setText(type1);
}

void QuestGeneralInfo::setZenny(const mh::mh4u::sFlags* sFlags)
{
    ui->postingFee->setText(QString::number(sFlags->getPostingFee(), 10));

    ui->rewardZenny->setText(QString::number(sFlags->getRewardZenny(), 10));

    ui->penaltyZenny->setText(QString::number(sFlags->getPenaltyZenny(), 10));

    ui->subRewardZenny->setText(QString::number(sFlags->getSubRewardZenny(), 10));
}

void QuestGeneralInfo::onBaseChanged(int index)
{
    if (index == 0) {
        ui->questId->setDisplayIntegerBase(10);
        ui->questId->setPrefix("");
    } else {
        ui->questId->setDisplayIntegerBase(16);
        ui->questId->setPrefix("0x");
    }
}
