#pragma once
#include <QDialog>

namespace mh {
namespace mh4u {

QT_FORWARD_DECLARE_CLASS(sQuest)

};
};

namespace Ui {
class QuestViewer;
}

class QuestViewer : public QDialog
{
    Q_OBJECT

public:
    explicit QuestViewer(QWidget *parent = nullptr);
    ~QuestViewer();

private slots:
    void onOpenQuestFile(bool checked);

private:
    Ui::QuestViewer *ui;

    void popupMessage(const QString &message, const bool error) const;

    bool openQuest(const QString& path);

    void addGeneralInfo(mh::mh4u::sQuest* quest);
    void addText(mh::mh4u::sQuest* quest);
    void addLargeEnemies(mh::mh4u::sQuest* quest);
    void addSmallEnemies(mh::mh4u::sQuest* quest);
};

