#pragma once
#include <QWidget>

namespace Ui {
class gui_gmd;
}

class gui_gmd : public QWidget
{
    Q_OBJECT

public:
    explicit gui_gmd(QWidget *parent = nullptr);
    ~gui_gmd();

private:
    Ui::gui_gmd *ui;
};
