#pragma once
#include <QWidget>

namespace mh {
namespace mh4u {

QT_FORWARD_DECLARE_CLASS(sQuest)
QT_FORWARD_DECLARE_CLASS(sFlags)

};
};

namespace Ui {
class QuestGeneralInfo;
}

class QuestGeneralInfo : public QWidget
{
    Q_OBJECT

public:
    explicit QuestGeneralInfo(QWidget *parent = nullptr);
    ~QuestGeneralInfo();

    void setInfo(const mh::mh4u::sQuest* quest);

private slots:
    void onBaseChanged(int index);

private:
    Ui::QuestGeneralInfo *ui;

    void setType0(const mh::mh4u::sFlags* sFlags);
    void setType1(const mh::mh4u::sFlags* sFlags);
    void setZenny(const mh::mh4u::sFlags* sFlags);
};

