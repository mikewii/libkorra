#pragma once
#include <QMainWindow>
#include <libkorra/tools/arc.hpp>
#include <libkorra/tools/ccontainer.hpp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btn_arc_create_clicked(void);
    void on_btn_arc_open_clicked(void);
    void on_btn_arc_extract_clicked(void);
    void on_btn_lmdEditor_clicked(void);

    void on_mh4uEditorsMaster_clicked();

private:
    mh::tools::ARC m_arc;
    std::vector<std::pair<std::string, mh::tools::CContainer>> m_arc_files;
    Ui::MainWindow  *ui;
    QList<QWidget*> gui;
};
