#pragma once
#include <QWidget>
#include <libkorra/MH4U/Quest/Quest.hpp>

namespace Ui {
class QuestText;
}

class QuestText : public QWidget
{
    Q_OBJECT

public:
    explicit QuestText(QWidget *parent = nullptr);
    ~QuestText();

    void setText(const mh::mh4u::Text type, const QString& text);

private:
    Ui::QuestText *ui;
};

