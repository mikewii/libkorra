#pragma once
#include <QSpinBox>
#include <QObject>

class QSpinBox2 : public QSpinBox
{
    Q_OBJECT
public:
    explicit QSpinBox2(QWidget *parent = nullptr);

    // QSpinBox interface
protected:
    QString textFromValue(int val) const override;
};

