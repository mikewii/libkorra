#pragma once
#include <QWidget>
#include <libkorra/MH4U/Quest/Quest.hpp>

namespace Ui {
class EnemyInfo;
}

class EnemyInfo : public QWidget
{
    Q_OBJECT

public:
    explicit EnemyInfo(QWidget *parent = nullptr);
    ~EnemyInfo();

    void setInfo(const mh::mh4u::sEnemy_s& info);

private slots:
    void onBaseChanged(int index);

private:
    Ui::EnemyInfo *ui;
};

