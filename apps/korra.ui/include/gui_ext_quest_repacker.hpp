#pragma once

#include <QAbstractListModel>

class gui_ext_quest_repacker : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit gui_ext_quest_repacker(QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
};
