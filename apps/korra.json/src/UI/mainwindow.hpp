#pragma once

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

QT_FORWARD_DECLARE_CLASS(DragNDrop)

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void onFileParseClicked(void);
    void onFileChooseClicked(void);
    void acceptFileDrop(const QUrl& url);

private:
    Ui::MainWindow *ui;
    DragNDrop* m_dnd;

    void connections(void);
    void parseFile(const QString& path);
    void parsedAs(const QString& str);
};

