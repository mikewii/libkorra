#include "mainwindow.hpp"
#include "ui_mainwindow.h"

#include "dragndrop.hpp"

#include <QDebug>
#include <QDropEvent>
#include <QMimeData>
#include <QFile>
#include <QDir>
#include <QFileDialog>

#include <map>

#include <nlohmann/json.hpp>

#include <libkorra/tools/ccontainer.hpp>

#include <libkorra/tools/arc.hpp>
#include "Tools/arc.hpp"

#include "MHXX/Extentions/parse.hpp"

static inline u32 probeMagic(const QString& path)
{
    QFile file(path);
    char magic[4];

    file.open(QFile::OpenModeFlag::ReadOnly);

    if (!file.isOpen())
        return 0;

    if (file.size() < static_cast<qint64>(sizeof(magic)))
        return 0;

    file.read(magic , sizeof(magic));

    return *reinterpret_cast<u32*>(magic);
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_dnd(new DragNDrop)
{
    DragNDrop dnd;

    ui->setupUi(this);

    setAcceptDrops(true);

    qApp->installEventFilter(m_dnd);

    connections();
}

MainWindow::~MainWindow()
{
    delete m_dnd;
    delete ui;
}

void MainWindow::onFileParseClicked(void)
{
    const auto& path = ui->filePath->text();

    if (path.isEmpty())
        return;

    parseFile(path);
}

void MainWindow::onFileChooseClicked(void)
{
    QString path = QFileDialog::getOpenFileName(this, tr("Open File"));

    if (!path.isEmpty())
        ui->filePath->setText(path);
}

void MainWindow::connections(void)
{
    connect(ui->fileParse, &QPushButton::clicked,
            this, &MainWindow::onFileParseClicked);

    connect(ui->fileChoose, &QPushButton::clicked,
            this, &MainWindow::onFileChooseClicked);

    connect(m_dnd, &DragNDrop::fileDrop,
            this, &MainWindow::acceptFileDrop);
}

void MainWindow::parseFile(const QString &path)
{
    nlohmann::ordered_json json;
    auto magic = probeMagic(path);

    if (   magic == mh::tools::ARCHeader::ARC_MAGIC
        || magic == mh::tools::ARCHeader::CRA_MAGIC) {
        mh::tools::CContainer container(path.toStdString());
        std::vector<mh::tools::CContainer> out;
        mh::tools::ARC arc(std::move(container));

        if (   arc.readZLIBData()
            && arc.uncompressAll_async(out)) {

        }

        json = mh::tools::json(arc);

        if (!json.empty())
            parsedAs(QStringLiteral("arc"));
    } else {
        auto parseAs = ui->parseAs->currentText();
        auto parseAsStdStr = parseAs.toStdString();

        if (supportedExtentions.contains(parseAsStdStr))
            json = supportedExtentions[parseAsStdStr](path.toStdString());

        if (!json.empty())
            parsedAs(parseAs);
    }

    ui->jsonOutput->setText(QString::fromStdString(json.dump(ui->jsonIndent->value())));
}

void MainWindow::parsedAs(const QString &str)
{
    if (!str.isEmpty())
        ui->statusbar->showMessage(QStringLiteral("Parsed as [%1]").arg(str));
}

void MainWindow::acceptFileDrop(const QUrl &url)
{
    QFileInfo info(url.toLocalFile());

    if (info.isFile())
        ui->filePath->setText(url.toLocalFile());
}

