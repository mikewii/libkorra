#pragma once

#include <QObject>

QT_FORWARD_DECLARE_CLASS(QDragEnterEvent)
QT_FORWARD_DECLARE_CLASS(QDropEvent)

class DragNDrop : public QObject
{
    Q_OBJECT

public:
    bool eventFilter(QObject *watched, QEvent *event) override;

    void handleDragEnter(QDragEnterEvent* event);
    void handleDrop(QDropEvent* event);

signals:
    void fileDrop(const QUrl&);
};
