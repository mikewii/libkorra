#pragma once
#include <sstream>
#include <iomanip>

template <typename T>
typename std::enable_if<   sizeof(typename std::decay<T>::type) != 1
                        && std::is_integral<T>::value, std::string>::type
asHex(const T t, const std::string& prefix = "0x")
{
    std::stringstream ss;

    ss << prefix << std::hex << t;

    return ss.str();
}

//template <typename T>
//typename std::enable_if<std::is_floating_point<T>::value, std::string>::type
//asHex(const T& t, const std::string& prefix = "0x")
//{
//    if (std::is_same<typename std::decay<T>::type, float>::value)
//        return asHex(reinterpret_cast<const uint32_t&>(t), prefix);
//    else
//        return asHex(reinterpret_cast<const uint64_t&>(t), prefix);
//}

template <typename T>
typename std::enable_if<std::is_same<typename std::decay<T>::type, float>::value, std::string>::type
asHex(const T t, const std::string& prefix = "0x")
{
    return asHex(reinterpret_cast<const uint32_t&>(t), prefix);
}

template <typename T>
typename std::enable_if<std::is_same<typename std::decay<T>::type, double>::value, std::string>::type
asHex(const T t, const std::string& prefix = "0x")
{
    return asHex(reinterpret_cast<const uint64_t&>(t), prefix);
}

template <typename T>
typename std::enable_if<sizeof(typename std::decay<T>::type) == 1, std::string>::type
asHex(const T t, const std::string& prefix = "0x")
{
    return asHex(static_cast<std::size_t>(t), prefix);
}

template <typename T>
typename std::enable_if<   sizeof(typename std::decay<T>::type) != 1
                        && std::is_enum<typename std::decay<T>::type>::value, std::string>::type
asHex(const T t, const std::string& prefix = "0x")
{
    return asHex(static_cast<std::size_t>(t), prefix);
}
