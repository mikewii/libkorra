#pragma once
#include <string>
#include <nlohmann/json_fwd.hpp>

namespace mh {
namespace mhxx {
namespace qdl {

class cQDL;

extern auto json(const cQDL& obj) -> nlohmann::ordered_json;
extern auto dump(const cQDL& obj, const int indent = -1) -> std::string;

}; /// namespace qdl
}; /// namespace mhxx
}; /// namespace mh
