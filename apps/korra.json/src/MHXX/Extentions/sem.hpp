#pragma once
#include <string>
#include <nlohmann/json_fwd.hpp>

namespace mh {
namespace mhxx {
namespace sem {

class cSEM;

extern auto json(const cSEM& obj) -> nlohmann::ordered_json;
extern auto dump(const cSEM& obj, const int indent = -1) -> std::string;

}; /// namespace sem
}; /// namespace mhxx
}; /// namespace mh
