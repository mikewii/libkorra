#pragma once
#include <string>
#include <nlohmann/json_fwd.hpp>

namespace mh {
namespace mhxx {
namespace sup {

class cSUP;

extern auto json(const cSUP& obj) -> nlohmann::ordered_json;
extern auto dump(const cSUP& obj, const int indent = -1) -> std::string;

}; /// namespace sup
}; /// namespace mhxx
}; /// namespace mh
