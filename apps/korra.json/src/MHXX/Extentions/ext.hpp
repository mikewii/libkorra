#pragma once
#include <string>
#include <nlohmann/json_fwd.hpp>

namespace mh {
namespace mhxx {
namespace ext {

class cEXT;

extern auto json(const cEXT& obj) -> nlohmann::ordered_json;
extern auto dump(const cEXT& obj, const int indent = -1) -> std::string;

}; /// namespace ext
}; /// namespace mhxx
}; /// namespace mh
