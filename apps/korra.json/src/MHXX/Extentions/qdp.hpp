#pragma once
#include <string>
#include <nlohmann/json_fwd.hpp>

namespace mh {
namespace mhxx {
namespace qdp {

class cQDP;

extern auto json(const cQDP& obj) -> nlohmann::ordered_json;
extern auto dump(const cQDP& obj, const int indent = -1) -> std::string;

}; /// namespace qdp
}; /// namespace mhxx
}; /// namespace mh
