#pragma once
#include <string>
#include <nlohmann/json_fwd.hpp>

namespace mh {
namespace mhxx {
namespace rem {

class cREM;

extern auto json(const cREM& obj) -> nlohmann::ordered_json;
extern auto dump(const cREM& obj, const int indent = -1) -> std::string;

}; /// namespace rem
}; /// namespace mhxx
}; /// namespace mh
