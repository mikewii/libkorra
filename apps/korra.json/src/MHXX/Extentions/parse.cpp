#include "MHXX/Extentions/parse.hpp"

#include <nlohmann/json.hpp>

#include <libkorra/tools/ccontainer.hpp>

#include <libkorra/MHXX/Extentions/esl.hpp>
#include <libkorra/MHXX/Extentions/ext.hpp>
#include <libkorra/MHXX/Extentions/gmd.hpp>
#include <libkorra/MHXX/Extentions/qdl.hpp>
#include <libkorra/MHXX/Extentions/qdp.hpp>
#include <libkorra/MHXX/Extentions/rem.hpp>
#include <libkorra/MHXX/Extentions/sem.hpp>
#include <libkorra/MHXX/Extentions/sup.hpp>

#include "MHXX/Extentions/esl.hpp"
#include "MHXX/Extentions/ext.hpp"
#include "MHXX/Extentions/gmd.hpp"
#include "MHXX/Extentions/qdl.hpp"
#include "MHXX/Extentions/qdp.hpp"
#include "MHXX/Extentions/rem.hpp"
#include "MHXX/Extentions/sem.hpp"
#include "MHXX/Extentions/sup.hpp"

template < typename T
         , bool(*parseFun)(const mh::tools::CContainer&, T&) noexcept
         , nlohmann::ordered_json(*jsonFun)(const T&)>
nlohmann::ordered_json commonParse(const std::string& path)
{
    mh::tools::CContainer container(path);
    T obj;

    parseFun(container, obj);

    return jsonFun(obj);
}

std::map<std::pair<std::string, u32>, SupportedExtentions::function> SupportedExtentions::map = {
    {  { "esl", mh::mhxx::esl::RESOURCE_HASH }
     , &commonParse<mh::mhxx::esl::cESL,mh::mhxx::esl::parse, mh::mhxx::esl::json> },
    {  { "ext", mh::mhxx::ext::RESOURCE_HASH }
     , &commonParse<mh::mhxx::ext::cEXT, mh::mhxx::ext::parse, mh::mhxx::ext::json> },
    {  { "gmd", mh::mhxx::gmd::RESOURCE_HASH }
     , &commonParse<mh::mhxx::gmd::cGMD, mh::mhxx::gmd::parse, mh::mhxx::gmd::json> },
    {  { "qdl", mh::mhxx::qdl::RESOURCE_HASH }
     , &commonParse<mh::mhxx::qdl::cQDL, mh::mhxx::qdl::parse, mh::mhxx::qdl::json> },
    {  { "qdp", mh::mhxx::qdp::RESOURCE_HASH }
     , &commonParse<mh::mhxx::qdp::cQDP, mh::mhxx::qdp::parse, mh::mhxx::qdp::json> },
    {  { "rem", mh::mhxx::rem::RESOURCE_HASH }
     , &commonParse<mh::mhxx::rem::cREM, mh::mhxx::rem::parse, mh::mhxx::rem::json> },
    {  { "sem", mh::mhxx::sem::RESOURCE_HASH }
     , &commonParse<mh::mhxx::sem::cSEM, mh::mhxx::sem::parse, mh::mhxx::sem::json> },
    {  { "sup", mh::mhxx::sup::RESOURCE_HASH }
     , &commonParse<mh::mhxx::sup::cSUP, mh::mhxx::sup::parse, mh::mhxx::sup::json> }
};

auto SupportedExtentions::contains(const std::string &val) const noexcept -> bool
{
    for (auto it = map.begin(); it != map.end(); it++)
        if (val == it->first.first)
            return true;

    return false;
}

auto SupportedExtentions::contains(const u32 val) const noexcept -> bool
{
    for (auto it = map.begin(); it != map.end(); it++)
        if (val == it->first.second)
            return true;

    return false;
}

auto SupportedExtentions::operator [] (const u32 val) const noexcept(false) -> SupportedExtentions::function&
{
    for (auto it = map.begin(); it != map.end(); it++)
        if (val == it->first.second)
            return it->second;

    throw std::out_of_range("map out of range");
}

auto SupportedExtentions::operator [] (const std::string &val) const noexcept(false) -> SupportedExtentions::function&
{
    for (auto it = map.begin(); it != map.end(); it++)
        if (val == it->first.first)
            return it->second;

    throw std::out_of_range("map out of range");
}
