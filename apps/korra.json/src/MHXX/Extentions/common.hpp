#pragma once
#include <nlohmann/json_fwd.hpp>
#include <libkorra/MHXX/Quest/Common.hpp>

namespace mh {
namespace mhxx {

extern auto parseItemIDHEX(const sItems::ID& obj) -> nlohmann::ordered_json;
extern auto parseEnemyIDHEX(const sEnemy::ID& obj) -> nlohmann::ordered_json;
extern auto parseTargetHEX(const sTarget& obj) -> nlohmann::ordered_json;
extern auto parseSupplyHEX(const sSupply& obj) -> nlohmann::ordered_json;
extern auto parseBossHEX(const sBoss& obj) -> nlohmann::ordered_json;
extern auto parseEmHEX(const sEm& obj) -> nlohmann::ordered_json;
extern auto parseAppearHEX(const sAppear& obj) -> nlohmann::ordered_json;
extern auto parseGMDLinkHEX(const sGMDLink& obj) -> nlohmann::ordered_json;

}; /// namespace mhxx
}; /// namespace mh
