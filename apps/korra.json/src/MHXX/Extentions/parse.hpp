#pragma once
#include <libkorra/types.h>
#include <nlohmann/json_fwd.hpp>
#include <functional>

class SupportedExtentions
{
public:
    using function = std::function<nlohmann::ordered_json(const std::string&)>;

    auto contains(const std::string& val) const noexcept -> bool;
    auto contains(const u32 val) const noexcept -> bool;

    auto operator [] (const std::string& val) const noexcept(false) -> function&;
    auto operator [] (const u32 val) const noexcept(false) -> function&;

private:
    static std::map<std::pair<std::string, u32>, function> map;
};

inline SupportedExtentions supportedExtentions;
