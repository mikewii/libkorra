#include "common.hpp"
#include "helpers.hpp"
#include <nlohmann/json.hpp>

namespace mh {
namespace mhxx {

auto parseItemIDHEX(const sItems::ID& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    j["ID"] = asHex(obj);
    j["IDStr"] = sItems::getStr(obj);

    return j;
}

auto parseEnemyIDHEX(const sEnemy::ID& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    j["ID"] = asHex(obj.ID);
    j["IDSub"] = asHex(obj.IDSub);
    j["IDStr"] = sEnemy::getStr(obj);

    return j;
}

auto parseTargetHEX(const sTarget& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    j["IsClearParam"] = asHex(obj.IsClearParam);

    j["ID::TargetEm"] = parseEnemyIDHEX(obj.TargetEm);
    j["ID::TargetItem"] = parseItemIDHEX(obj.TargetItem);

    j["ClearNum"] = asHex(obj.ClearNum);

    return j;
}

auto parseSupplyHEX(const sSupply& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    j["Label"] = asHex(obj.Label);
    j["Type"] = asHex(obj.Type);

    j["ID::TargetEm"] = parseEnemyIDHEX(obj.TargetEm);
    j["ID::TargetItem"] = parseItemIDHEX(obj.TargetItem);

    j["TargetNum"] = asHex(obj.TargetNum);

    return j;
}

auto parseBossHEX(const sBoss& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    j["EmType"] = parseEnemyIDHEX(obj.EmType);
    j["EmSubType"] = asHex(obj.EmSubType);
    j["AuraType"] = asHex(obj.AuraType);
    j["RestoreNum"] = asHex(obj.RestoreNum);
    j["VitalTblNo"] = asHex(obj.VitalTblNo);
    j["AttackTblNo"] = asHex(obj.AttackTblNo);
    j["OtherTblNo"] = asHex(obj.OtherTblNo);
    j["Difficulty"] = asHex(obj.Difficulty);
    j["Scale"] = asHex(obj.Scale);
    j["ScaleTbl"] = asHex(obj.ScaleTbl);
    j["StaminaTbl"] = asHex(obj.StaminaTbl);

    return j;
}

auto parseEmHEX(const sEm& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    j["EmSetType"] = asHex(obj.EmSetType);

    j["ID::TargetEm"] = parseEnemyIDHEX(obj.TargetEm);
    j["ID::TargetItem"] = parseItemIDHEX(obj.TargetItem);

    j["EmSetTargetNum"] = asHex(obj.EmSetTargetNum);

    return j;
}

auto parseAppearHEX(const sAppear& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    j["AppearType"] = asHex(obj.AppearType);
    j["AppearTargetType"] = asHex(obj.AppearTargetType);
    j["AppearTargetNum"] = asHex(obj.AppearTargetNum);

    return j;
}

auto parseGMDLinkHEX(const sGMDLink& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    j["ProgNo"] = asHex(obj.ProgNo);
    j["Resource"] = asHex(obj.Resource);
    j["GMDFileName"] = obj.GMDFileName;

    return j;
}

}; /// namespace mhxx
}; /// namespace mh
