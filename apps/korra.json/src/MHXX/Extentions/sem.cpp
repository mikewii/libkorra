#include "sem.hpp"
#include <libkorra/MHXX/Extentions/sem.hpp>
#include <nlohmann/json.hpp>

namespace mh {
namespace mhxx {
namespace sem {

auto json(const cSEM& obj) -> nlohmann::ordered_json
{
    if (!obj.isValid())
        return {};

    nlohmann::ordered_json j;
    nlohmann::ordered_json jPosition;

    const auto& data = obj.getSetEmMain();

    j["Magic"] = data.Magic;
    j["Version"] = data.Version;
    j["WaveNo"] = data.WaveNo;
    j["AreaNo"] = data.AreaNo;

    jPosition["X"] = data.Position.X;
    jPosition["Y"] = data.Position.Y;
    jPosition["Z"] = data.Position.Z;
    jPosition["R"] = data.Position.R;

    j["Position"] = jPosition;

    return j;
}

auto dump(const cSEM &obj, const int indent) -> std::string
{
    return json(obj).dump(indent);
}

}; /// namespace sem
}; /// namespace mhxx
}; /// namespace mh
