#include "qdp.hpp"
#include <libkorra/MHXX/Extentions/qdp.hpp>
#include <nlohmann/json.hpp>

namespace mh {
namespace mhxx {
namespace qdp {

auto json(const cQDP& obj) -> nlohmann::ordered_json
{
    if (!obj.isValid())
        return {};

    nlohmann::ordered_json j;

    const auto& data = obj.getQuestPlus();

    j["Magic"] = data.Magic;
    j["Version"] = data.Version;

    j["IsFence"] = data.isFence;
    j["IsFenceFromStart"] = data.isFenceFromStart;
    j["FenceOpenTime"] = data.FenceOpenTime;
    j["FenceStartTime"] = data.FenceStartTime;
    j["FenceReuseTime"] = data.FenceReuseTime;

    j["IsDragonator"] = data.isDragonator;
    j["DragonatorStartTime"] = data.DragonatorStartTime;
    j["DragonatorReuseTime"] = data.DragonatorReuseTime;

    j["FortHpS"] = data.FortHpS;
    j["FortHpL"] = data.FortHpL;

    return j;
}

auto dump(const cQDP &obj, const int indent) -> std::string
{
    return json(obj).dump(indent);
}

}; /// namespace qdp
}; /// namespace mhxx
}; /// namespace mh
