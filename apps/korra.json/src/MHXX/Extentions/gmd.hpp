#pragma once
#include <string>
#include <nlohmann/json_fwd.hpp>

namespace mh {
namespace mhxx {
namespace gmd {

class cGMD;

extern auto json(const cGMD& obj) -> nlohmann::ordered_json;
extern auto dump(const cGMD& obj, const int indent = -1) -> std::string;
extern auto dump_advanced2(const cGMD& obj, const int indent = -1) -> std::string;

}; /// namespace gmd
}; /// namespace mhxx
}; /// namespace mh
