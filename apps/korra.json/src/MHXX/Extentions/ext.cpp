#include "ext.hpp"
#include "helpers.hpp"
#include "common.hpp"

#include <libkorra/MHXX/Extentions/ext.hpp>
#include <nlohmann/json.hpp>

namespace mh {
namespace mhxx {
namespace ext {

template <typename T>
auto parseEnum(const typename T::e& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    j["Value"] = asHex(obj);
    j["AsStr"] = T::getStr(obj);

    return j;
}

auto parseHeaderHEX(const Header& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    j["Magic"] = asHex(obj.Magic);
    j["Version"] = asHex(obj.Version);

    j["Index"] = asHex(obj.Index);
    j["QuestID"] = asHex(obj.QuestID);

    j["QuestType0"] = parseEnum<sQuestType0>(obj.QuestType0);
    j["QuestType1"] = parseEnum<sQuestType1>(obj.QuestType1);
    j["QuestLv"] = parseEnum<sQuestLv>(obj.QuestLv);

    j["BossLv"] = parseEnum<sEnemyLv>(obj.BossLv);

    j["MapNo"] = parseEnum<sMaps>(obj.MapNo);

    j["StartType"] = parseEnum<sStartType>(obj.StartType);

    j["QuestTime"] = asHex(obj.QuestTime);
    j["QuestLives"] = asHex(obj.QuestLives);

    j["AcEquipSetNo"] = asHex(obj.AcEquipSetNo);

    j["BGMType"] = parseEnum<sBGMType>(obj.BGMType);

    j["EntryType[0]"] = parseEnum<sEntryType>(obj.EntryType[0]);
    j["EntryType[1]"] = parseEnum<sEntryType>(obj.EntryType[1]);

    j["EntryTypeCombo"] = asHex(obj.EntryTypeCombo);

    j["ClearType"] = parseEnum<sClearType>(obj.ClearType);

    j["GekitaiHp"] = asHex(obj.GekitaiHP);

    j["TargetMain[0]"] = parseTargetHEX(obj.TargetMain[0]);
    j["TargetMain[1]"] = parseTargetHEX(obj.TargetMain[1]);
    j["TargetSub"] = parseTargetHEX(obj.TargetSub);

    j["CarvingLv"] = parseEnum<sCarvingLv>(obj.CarvingLv);
    j["GatherLv"] = parseEnum<sGatheringLv>(obj.GatheringLv);
    j["FishingLv"] = parseEnum<sFishingLv>(obj.FishingLv);

    j["EntryFee"] = asHex(obj.EntryFee);
    j["VillagePoints"] = asHex(obj.VillagePoints);
    j["MainRewardMoney"] = asHex(obj.MainRewardMoney);
    j["SubRewardMoney"] = asHex(obj.SubRewardMoney);
    j["ClearRemVillagePoint"] = asHex(obj.ClearRemVillagePoint);
    j["FailedRemVillagePoint"] = asHex(obj.FailedRemVillagePoint);
    j["SubRemVillagePoint"] = asHex(obj.SubRemVillagePoint);
    j["ClearRemHunterPoint"] = asHex(obj.ClearRemHunterPoint);
    j["SubRemHunterPoint"] = asHex(obj.SubRemHunterPoint);

    j["RemAddFrame[0]"] = asHex(obj.RemAddFrame[0]);
    j["RemAddFrame[1]"] = asHex(obj.RemAddFrame[1]);
    j["RemAddLotMax"] = asHex(obj.RemAddLotMax);

    j["Supply[0]"] = parseSupplyHEX(obj.Supply[0]);
    j["Supply[1]"] = parseSupplyHEX(obj.Supply[1]);

    j["Boss[0]"] = parseBossHEX(obj.Boss[0]);
    j["Boss[1]"] = parseBossHEX(obj.Boss[1]);
    j["Boss[2]"] = parseBossHEX(obj.Boss[2]);
    j["Boss[3]"] = parseBossHEX(obj.Boss[3]);
    j["Boss[4]"] = parseBossHEX(obj.Boss[4]);

    j["SmallEmHPTbl"] = asHex(obj.SmallEmHPTbl);
    j["SmallEmAttackTbl"] = asHex(obj.SmallEmAttackTbl);
    j["SmallEmOtherTbl"] = asHex(obj.SmallEmOtherTbl);

    j["Em[0]"] = parseEmHEX(obj.Em[0]);
    j["Em[1]"] = parseEmHEX(obj.Em[1]);

    j["IsBossRushType"] = asHex(obj.IsBossRushType);

    j["Appear[0]"] = parseAppearHEX(obj.Appear[0]);
    j["Appear[1]"] = parseAppearHEX(obj.Appear[1]);
    j["Appear[2]"] = parseAppearHEX(obj.Appear[2]);
    j["Appear[3]"] = parseAppearHEX(obj.Appear[3]);
    j["Appear[4]"] = parseAppearHEX(obj.Appear[4]);

    j["InvaderAppearChance"] = asHex(obj.InvaderAppearChance);
    j["InvaderStartTime"] = asHex(obj.InvaderStartTime);
    j["InvaderStartRandChance"] = asHex(obj.InvaderStartRandChance);

    j["StrayLimit[0]"] = asHex(obj.StrayLimit[0]);
    j["StrayLimit[1]"] = asHex(obj.StrayLimit[1]);
    j["StrayLimit[2]"] = asHex(obj.StrayLimit[2]);

    j["StrayRand2[0]"] = asHex(obj.StrayRand2[0]);
    j["StrayRand2[1]"] = asHex(obj.StrayRand2[1]);
    j["StrayRand2[2]"] = asHex(obj.StrayRand2[2]);

    j["sp_ExtraTicketNum"] = asHex(obj.SPExtraTicketNum);

    j["Icon[0]"] = parseEnum<sIcon>(obj.Icon[0]);
    j["Icon[1]"] = parseEnum<sIcon>(obj.Icon[1]);
    j["Icon[2]"] = parseEnum<sIcon>(obj.Icon[2]);
    j["Icon[3]"] = parseEnum<sIcon>(obj.Icon[3]);
    j["Icon[4]"] = parseEnum<sIcon>(obj.Icon[4]);

    return j;
}

auto parseGMDLinksHEX(const std::vector<sGMDLink>& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    for (auto i = 0u; i < obj.size(); i++) {
        std::stringstream ss;
        const auto& link = obj[i];

        ss << "0x" << std::hex << i;

        j[ss.str()] = parseGMDLinkHEX(link);
    }

    return j;
}

auto parseHeader1HEX(const Header1& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    j["Padding0"] = asHex(obj.Padding0);

    j["Padding1[0]"] = asHex(obj.Padding1[0]);
    j["Padding1[1]"] = asHex(obj.Padding1[1]);
    j["Padding1[2]"] = asHex(obj.Padding1[2]);
    j["Padding1[3]"] = asHex(obj.Padding1[3]);

    j["VillagePointG"] = asHex(obj.VillagePointG);

    j["Flag"] = asHex(obj.Flag.Raw);

    return j;
}

auto json(const cEXT& obj) -> nlohmann::ordered_json
{
    if (!obj.isValid())
        return {};

    nlohmann::ordered_json j;

    const auto& header = obj.getHeader0();
    const auto& gmdLinks = obj.getGMDLinks();
    const auto& header1 = obj.getHeader1();

    j["header"] = parseHeaderHEX(header);
    j["GMDLinks"] = parseGMDLinksHEX(gmdLinks);
    j["header1"] = parseHeader1HEX(header1);

    return j;
}

auto dump(const cEXT &obj, const int indent) -> std::string
{
    return json(obj).dump(indent);
}

}; /// namespace ext
}; /// namespace mhxx
}; /// namespace mh
