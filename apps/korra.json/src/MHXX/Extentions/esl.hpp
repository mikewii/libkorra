#pragma once
#include <string>
#include <nlohmann/json_fwd.hpp>

namespace mh {
namespace mhxx {
namespace esl {

class cESL;

extern auto json(const cESL& obj) -> nlohmann::ordered_json;
extern auto dump(const cESL& obj, const int indent = -1) -> std::string;

}; /// namespace esl
}; /// namespace mhxx
}; /// namespace mh
