#pragma once
#include <string>
#include <nlohmann/json_fwd.hpp>

namespace mh {
namespace tools {

class ARC;

extern auto json(const ARC& obj) -> nlohmann::ordered_json;
extern auto dump(const ARC& obj, const int indent = -1) -> std::string;

}; /// namespace tools
}; /// namespace mh
