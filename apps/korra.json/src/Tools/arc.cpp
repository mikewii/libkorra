#include "Tools/arc.hpp"
#include "helpers.hpp"
#include <libkorra/tools/arc.hpp>
#include <nlohmann/json.hpp>

namespace mh {
namespace tools {

auto parseHeaderHEX(const ARCHeader& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    j["Magic"] = asHex(obj.Magic);
    j["Version"] = asHex(obj.Version);
    j["FilesNum"] = asHex(obj.FilesNum);

    return j;
}

auto parseARCFileHeaderHEX(const ARCFileHeader& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    j["FilePath"] = obj.FilePath;
    j["ResourceHash"] = asHex(obj.ResourceHash);
    j["CompressedSize"] = asHex(obj.CompressedSize);
    j["DecompressedSize"] = asHex(obj.UncompressedSize);
    j["Flags"] = asHex(obj.Flags);
    j["pZData"] = asHex(obj.pZData);

    return j;
}

auto parseARCFileHeadersHEX(const std::vector<ARCFileHeader>& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    for (auto i = 0u; i < obj.size(); i++) {
        const ARCFileHeader& header = obj[i];
        std::stringstream ss;

        ss << "0x" << std::hex << i;

        j[ss.str()] = parseARCFileHeaderHEX(header);
    }

    return j;
}

auto json(const ARC& obj) -> nlohmann::ordered_json
{
    nlohmann::ordered_json j;

    j["Header"] = parseHeaderHEX(obj.getHeader());
    j["ARCFileHeaders"] = parseARCFileHeadersHEX(obj.getARCFileHeaders());

    return j;
}

auto dump(const ARC& obj, const int indent) -> std::string
{
    return json(obj).dump(indent);
}

}; /// namespace tools
}; /// namespace mh
