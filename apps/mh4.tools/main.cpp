#include <boost/locale.hpp>
#include <boost/locale/generator.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

#include <cxxopts.hpp>

#include <iostream>
#include <functional>
#include <set>

#include <libkorra/tools/ccontainer.hpp>
#include <libkorra/tools/file.hpp>
#include <libkorra/MH4U/Crypto.hpp>
#include <libkorra/MH4U/Extdata.hpp>

namespace fs = boost::filesystem;

void decodeData(const boost::filesystem::path& path)
{
    std::cout << "Decode data at " << path << std::endl;

    mh::tools::CContainer in;
    fs::path outPath = path;

    outPath.replace_extension(".dec");

    mh::tools::File::file_to_cc(path, in);

    if (mh::mh4u::Crypto().xorDecode(in)) {
        in.write(outPath.string());
        std::cout << "Data saved at " << outPath << std::endl;
    }
}

void encodeData(const boost::filesystem::path& path)
{
    std::cout << "Encode data at " << path << std::endl;

    mh::tools::CContainer in;
    fs::path outPath = path;

    outPath.replace_extension(".enc");

    mh::tools::File::file_to_cc(path, in);

    mh::mh4u::Crypto().xorEncode(in);

    in.write(outPath.string());

    std::cout << "Data saved at " << outPath << std::endl;
}

void decodeQuest(const boost::filesystem::path& path)
{
    std::cout << "Decode quests at " << path << std::endl;

    mh::mh4u::extdata::DLCExtractor_Helper extractor(path);
    auto outPath = path;

    extractor.set_out_folder(outPath.append("quest"));

    if (extractor.extract(false, true))
        std::cout << "quests saved at " << outPath << std::endl;
}

void encodeQuest(const boost::filesystem::path& path)
{
    std::cout << "Encode quests at " << path << std::endl;

    mh::mh4u::extdata::DLCRepacker_Helper repacker(path);

    repacker.prepare(true);
    repacker.repack(true);
    repacker.write(true);
}

struct SetComparator {
    bool operator()(const std::set<std::string>& lhs,
                    const std::set<std::string>& rhs) const {
        auto trimmedSet = +[](const std::set<std::string>& set) {
            std::set<std::string> out;

            for (auto str : set) {
                boost::algorithm::trim(str);
                boost::algorithm::to_lower(str);

                out.insert(str);
            }

            return out;
        };

        return trimmedSet(lhs) < trimmedSet(rhs);
    }
};

int main(int argc, char* argv[])
{
    using function = std::function<void(const boost::filesystem::path&)>;

    static const std::map<std::set<std::string>, function, SetComparator> map = {
        {{"quest", "decode"}, &decodeQuest},
        {{"quest", "encode"}, &encodeQuest},
        {{"save", "decode"}, &decodeData},
        {{"save", "encode"}, &encodeData},
    };

    std::locale::global(boost::locale::generator().generate(""));
    boost::filesystem::path::imbue(std::locale());

    cxxopts::Options options("mh4.tools", "decrypt/encrypt mh4 extdata files");

    options.add_options()
        ("op", "Operation: decode/encode", cxxopts::value<std::string>())
        ("type", "File type: quest/save", cxxopts::value<std::string>())
        ("path", "Input file/folder path", cxxopts::value<std::string>())
        ("h,help", "Print help and exit.");

    options.positional_help("<path>");
    options.parse_positional({"path"});

    try {
        auto result = options.parse(argc, argv);

        std::string path, op, type;

        if (result.count("help")) {
            std::cout << options.help() << std::endl;
            std::exit(0);
        }

        if (result.count("path")) {
            path = result["path"].as<std::string>();

            if (!boost::filesystem::exists(path)) {
                std::cout << "Not a valid path" << std::endl;
                std::exit(1);
            }
        } else {
            std::cout << "No file provided" << std::endl;
            std::exit(1);
        }

        if (result.count("type"))
            type = result["type"].as<std::string>();

        if (result.count("op"))
            op = result["op"].as<std::string>();

        const auto& val = {type, op};

        if (map.count(val) > 0) {
            map.at(val)(path);
        } else {
            std::cout << "Unrecognized operation type" << std::endl;
            std::cout << options.help() << std::endl;
            std::exit(1);
        }
    } catch (const std::exception& e) {
        std::cerr << "Error parsing options: " << e.what() << std::endl;
        return 1;
    }

    return 0;
}
