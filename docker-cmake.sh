#!/bin/bash

# $1 cmake project path
# $2 image
# $3 [clean] clean build

USERID=$(id -u)
GROUPID=$(id -g)
PROJECT_PATH="$1"
IMAGE="$2"
CMAKE="x86_64-w64-mingw32.static-cmake"
BUILD_DIR="build-win"
BUILD_TYPE="Release"
CORES=$(nproc)

CMD="cd /project"
CMD_ARGS=(
    "mkdir -p $BUILD_DIR"
    "cd $BUILD_DIR"
    "$CMAKE .. -DCMAKE_BUILD_TYPE=$BUILD_TYPE"
    "$CMAKE --build . --parallel $CORES"
)

if [ "$3" = "clean" ]; then
    echo "Running clean first"
    CMD+=" && rm -rf $BUILD_DIR"
fi
for ARG in "${CMD_ARGS[@]}"; do
    CMD+=" && ${ARG}"
done

echo "::AS=$USERID:$GROUPID"
echo "::PROJECT=$PROJECT_PATH"
echo "::IMAGE=$IMAGE"
echo "::CMD=$CMD"
echo "::STARTING::"

sudo docker run --rm --user $USERID:$GROUPID --cap-drop ALL -v "$PROJECT_PATH":/project "$IMAGE" /bin/bash -c "$CMD"
