#pragma once
#include <libkorra/types.h>
#include <array>

namespace mh {
namespace mhxx {

struct sEnemy {
    static const u32 EM_MAX = 139;

    enum {
        NORMAL      = 0,
        SUB         = 1,        // ex Crimson fatalis
        RARE        = 1 << 1,   // ex Gold rathian
        DEVIANT     = 1 << 2,
        VARIANT     = SUB | DEVIANT,

        ITEM        = 1 << 3,   // some delivery quests have that
        SMALL       = 1 << 4
    };

    struct ID {
        u8 ID;
        u8 IDSub;
    };

    struct Em {
        const char*     Name;
        const sEnemy::ID ID;
    };

    static const std::array<const sEnemy::Em, sEnemy::EM_MAX> str;

    static const char* getStr(const sEnemy::ID& id);
};

}; /// namespase mhxx
}; /// namespace mh
