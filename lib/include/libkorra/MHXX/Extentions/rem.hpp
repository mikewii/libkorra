#pragma once
#include <libkorra/types.h>
#include <libkorra/tools/ccontainer_fwd.hpp>

namespace mh {
namespace mhxx {
namespace rem {

inline constexpr const u32 RESOURCE_HASH = 0x5B3C302D;

struct sRewardFlag {
    s8  Flag;
    s8  ItemNum;
};

struct sRewardItem {
    s16 ID;
    s8  Ammount;
    s8  Rate;       // -1 mark last item in sRewardItem_s ?
};

struct Header {
    static constexpr const u32 MAGIC          = 0x3F800000;
    static constexpr const u32 VERSION        = 1;
    static constexpr const u32 ITEMS_MAX      = 40;
    static constexpr const u32 FLAGS_MAX      = 4;
    static constexpr const u32 FLAGNUMS_MAX   = 8;

    u32         Magic;
    u32         Version;

    sRewardFlag Flags[FLAGS_MAX];        // 4 rows?
    s8          FlagNums[FLAGNUMS_MAX];

    sRewardItem Items[ITEMS_MAX];        // 10 per 1 row?
};

// RewardEm
class cREM
{
private:
    bool m_valid = false;
    Header m_header;

public:
    auto isValid(void) const noexcept -> bool;

    auto getFlag(const u32 id, sRewardFlag& flag) const noexcept -> bool;
    auto getFlagNum(const u32 id, u8& flagNum) const noexcept -> bool;
    auto getRewardItem(const u32 id, sRewardItem& rewardItem) const noexcept -> bool;

    auto setFlag(const u32 id, const sRewardFlag flag) noexcept -> bool;
    auto setFlagNum(const u32 id, const s8 flagNum) noexcept -> bool;
    auto setRewardItem(const u32 id, const sRewardItem item) noexcept -> bool;

    auto getRewardEm(void) noexcept -> Header&;
    auto getRewardEm(void) const noexcept -> const Header&;

private:
    auto readHeader(const CContainer& container) noexcept -> bool;

    friend auto parse(const CContainer& container, cREM& obj) noexcept -> bool;
};

extern auto parse(const CContainer& container, cREM& obj) noexcept -> bool;

}; /// namespase rem
}; /// namespase mhxx
}; /// namespace mh
