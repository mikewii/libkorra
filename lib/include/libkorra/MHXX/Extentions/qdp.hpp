#pragma once
#include <libkorra/types.h>
#include <libkorra/attributes.h>
#include <libkorra/tools/ccontainer_fwd.hpp>

namespace mh {
namespace mhxx {
namespace qdp {

inline constexpr const u32 RESOURCE_HASH = 0x26BEC21C;

struct Header {
    static constexpr const u32 MAGIC      = 0x3F800000; //nope
    static constexpr const u32 VERSION    = 1;

    u32     Magic;
    u32     Version;

    bool    isFence;            // Rail
    bool    isFenceFromStart;
    u16     FenceOpenTime;
    u16     FenceStartTime;
    u16     FenceReuseTime;

    bool    isDragonator;       // Gekiryu
    u16     DragonatorStartTime;
    u16     DragonatorReuseTime;

    u16     FortHpS;                // fort on laoshan quest?
    u16     FortHpL;

} PACKED;

// QuestPlus
class cQDP
{
private:
    bool m_valid = false;
    Header m_header;

public:
    auto isValid(void) const noexcept -> bool;

    auto getIsFence(void) const noexcept -> bool;
    auto getIsFenceFromStart(void) const noexcept -> bool;
    auto getFenceOpenTime(void) const noexcept -> u16;
    auto getFenceStartTime(void) const noexcept -> u16;
    auto getFenceReuseTime(void) const noexcept -> u16;
    auto getIsDragonator(void) const noexcept -> bool;
    auto getDragonatorStartTime(void) const noexcept -> u16;
    auto getDragonatorReuseTime(void) const noexcept -> u16;
    auto getFortHpS(void) const noexcept -> u16;
    auto getFortHpL(void) const noexcept -> u16;

    void setIsFence(const bool value) noexcept;
    void setIsFenceFromStart(const bool value) noexcept;
    void setFenceOpenTime(const u16 num) noexcept;
    void setFenceStartTime(const u16 num) noexcept;
    void setFenceReuseTime(const u16 num) noexcept;
    void setIsDragonator(const bool value) noexcept;
    void setDragonatorStartTime(const u16 num) noexcept;
    void setDragonatorReuseTime(const u16 num) noexcept;
    void setFortHpS(const u16 num) noexcept;
    void setFortHpL(const u16 num) noexcept;

    auto getQuestPlus(void) noexcept -> Header&;
    auto getQuestPlus(void) const noexcept -> const Header&;

private:
    auto readHeader(const CContainer& container) noexcept -> bool;

    friend auto parse(const CContainer& container, cQDP& obj) noexcept -> bool;
};

extern auto parse(const CContainer& container, cQDP& obj) noexcept -> bool;

}; /// namespase qdp
}; /// namespase mhxx
}; /// namespace mh
