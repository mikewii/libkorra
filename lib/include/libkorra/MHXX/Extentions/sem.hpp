#pragma once
#include <libkorra/types.h>
#include <libkorra/tools/ccontainer_fwd.hpp>

namespace mh {
namespace mhxx {
namespace sem {

inline constexpr const u32 RESOURCE_HASH = 0x2553701D;

template <typename T>
struct Geometry4 {
    T X;
    T Y;
    T Z;
    T R;
};

struct Header {
    static constexpr const u32 MAGIC = 0x3F800000;
    static constexpr const u32 VERSION = 1;

    u32                 Magic;      // maybe
    u32                 Version;    // maybe
    u32                 WaveNo;
    u32                 AreaNo;
    Geometry4<float>    Position;
};

// SetEmMain
class cSEM
{
private:
    bool m_valid = false;
    Header m_header;

public:
    auto isValid(void) const noexcept -> bool;

    auto getWaveNo(void) const noexcept -> u32;
    auto getAreaNo(void) const noexcept -> u32;
    auto getPosition(void) const noexcept -> Geometry4<float>;

    void setWaveNo(const u32 num) noexcept;
    void setAreaNo(const u32 num) noexcept;
    void setPosition(const Geometry4<float>& pos) noexcept;

    auto getSetEmMain(void) noexcept -> Header&;
    auto getSetEmMain(void) const noexcept -> const Header&;

private:
    auto readHeader(const CContainer& container) noexcept -> bool;

    friend auto parse(const CContainer& container, cSEM& obj) noexcept -> bool;
};

extern auto parse(const CContainer& container, cSEM& obj) noexcept -> bool;

}; /// namespase sem
}; /// namespase mhxx
}; /// namespace mh
