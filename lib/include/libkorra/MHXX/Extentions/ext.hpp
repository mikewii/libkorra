#pragma once
#include <libkorra/types.h>
#include <libkorra/tools/ccontainer_fwd.hpp>
#include <libkorra/MHXX/Quest/Common.hpp>
#include <vector>

namespace mh {
namespace mhxx {
namespace ext {

inline constexpr const u32 RESOURCE_HASH = 0x1BBFD18E;
inline constexpr const u32 MHXX_EXT_SIZE = 0xF9;
inline constexpr const u32 MHGU_EXT_SIZE = 0x149;

struct Header {
    static constexpr const u32 MAGIC      = 0x434B0000;
    static constexpr const u32 VERSION    = 1;

    u32             Magic = MAGIC;
    u32             Version = VERSION;

    u32             Index;                  // ?
    u32             QuestID;

    sQuestType0::e  QuestType0;
    sQuestType1::e  QuestType1;
    sQuestLv::e     QuestLv;
    sEnemyLv::e     BossLv;
    sMaps::e        MapNo;
    sStartType::e   StartType;

    u8              QuestTime;
    u8              QuestLives;

    u8              AcEquipSetNo;           // null in every quest | arena related?

    sBGMType::e     BGMType;

    sEntryType::e   EntryType[2];
    u8              EntryTypeCombo;         //  null in every quest
    sClearType::e   ClearType;
    u8              GekitaiHP; // fierce team / small mons? / repel, oppose

    sTarget         TargetMain[2];
    sTarget         TargetSub;

    sCarvingLv::e   CarvingLv;
    sGatheringLv::e GatheringLv;
    sFishingLv::e   FishingLv;

    u32             EntryFee;
    u32             VillagePoints;
    u32             MainRewardMoney;
    u32             SubRewardMoney;
    u32             ClearRemVillagePoint;
    u32             FailedRemVillagePoint;
    u32             SubRemVillagePoint;
    u32             ClearRemHunterPoint;
    u32             SubRemHunterPoint;

    // done
    // flags for reward panels
    u8              RemAddFrame[2]; // 0 - 4
    s8              RemAddLotMax;   // -2 -1 0 1 2 4

    // done, aligned
    sSupply         Supply[2];  // 0 available from start, 1 available later on quest

    // done, aligned
    sBoss           Boss[5];

    // done, aligned
    u8              SmallEmHPTbl;      // also known as Zako / bits?
    u8              SmallEmAttackTbl;  // bits?
    u8              SmallEmOtherTbl;   // bits? // staminaTbl?

    // done, aligned
    sEm             Em[2];
    u8              IsBossRushType;     // 0 - 8

    // done, aligned
    sAppear         Appear[5];

    u8              InvaderAppearChance;       // invader boss id?
    u8              InvaderStartTime;
    u8              InvaderStartRandChance;
    u8              StrayLimit[3];              // bits? // invader tables?
    u8              StrayRand2[3];              // chance 0 - 100 // invader table chance?
    u8              SPExtraTicketNum;          // 0 - 10 // wtf is this?

    sIcon::e        Icon[5];                    // 0 - 186
} PACKED;

struct Header1 {
    u8          Padding0;
    u8          Padding1[4];

    s32         VillagePointG;

    union {
        u8 Raw;

        // possible values: 0, 1, 2, 3, 4, 8, 12, 16
        struct {
            u8 b0 : 1;
            u8 b1 : 1;
            u8 b2 : 1;
            u8 b3 : 1;
            u8 b4 : 1;
        };
    } Flag;

} PACKED;

class cEXT
{
private:
    bool m_valid = false;
    Header m_header0;
    Header1 m_header1;
    std::vector<sGMDLink> m_gmdLinks;

public:
    auto isValid(void) const noexcept -> bool;

    auto dump(CContainer& container) noexcept -> void;

    auto getIndex(void) const noexcept -> u32;
    auto setIndex(const u32 val) noexcept -> void;

    auto getQuestID(void) const noexcept -> u32;
    auto setQuestID(const u32 val) noexcept -> void;

    auto getQuestType0(void) const noexcept -> sQuestType0::e;
    auto setQuestType0(const sQuestType0::e val) noexcept -> void;

    auto getQuestType1(void) const noexcept -> sQuestType1::e;
    auto setQuestType1(const sQuestType1::e val) noexcept -> void;

    auto getQuestLv(void) const noexcept -> sQuestLv::e;
    auto setQuestLv(const sQuestLv::e val) -> void;

    auto getBossLv(void) const noexcept -> sEnemyLv::e;
    auto setBossLv(const sEnemyLv::e val) -> void;

    auto getMapNo(void) const noexcept -> sMaps::e;
    auto setMapNo(const sMaps::e val) noexcept -> void;

    auto getStartType(void) const noexcept -> sStartType::e;
    auto setStartType(const sStartType::e val) noexcept -> void;

    auto getQuestTime(void) const noexcept -> u8;
    auto setQuestTime(const u8 val) noexcept -> void;

    auto getQuestLives(void) const noexcept -> u8;
    auto setQuestLives(const u8 val) noexcept -> void;

    auto getAcEquipSetNo(void) const noexcept -> u8;
    auto setAcEquipSetNo(const u8 val) noexcept -> void;

    auto getBGMType(void) const noexcept -> sBGMType::e;
    auto setBGMType(const sBGMType::e val) noexcept -> void;

    auto getEntryType(const std::size_t id) const noexcept(false) -> sEntryType::e;
    auto getEntryType0(void) const noexcept -> sEntryType::e;
    auto getEntryType1(void) const noexcept -> sEntryType::e;

    auto setEntryType(const std::size_t id, const sEntryType::e val) noexcept -> bool;
    auto setEntryType0(const sEntryType::e val) noexcept -> void;
    auto setEntryType1(const sEntryType::e val) noexcept -> void;

    auto getEntryTypeCombo(void) const noexcept -> u8;
    auto setEntryTypeCombo(const u8 val) noexcept -> void;

    auto getClearType(void) const noexcept -> sClearType::e;
    auto setClearType(const sClearType::e val) noexcept -> void;

    auto getGekitaiHP(void) const noexcept -> u8;
    auto setGekitaiHP(const u8 val) noexcept -> void;

    auto getTargetMain(const std::size_t id) const noexcept(false) -> sTarget;
    auto getTargetMain0(void) const noexcept -> sTarget;
    auto getTargetMain1(void) const noexcept -> sTarget;

    auto setTargetMain(const std::size_t id, const sTarget val) noexcept -> bool;
    auto setTargetMain0(const sTarget val) noexcept -> void;
    auto setTargetMain1(const sTarget val) noexcept -> void;

    auto getTargetSub(void) const noexcept -> sTarget;
    auto setTargetSub(const sTarget val) noexcept -> void;

    auto getCarvingLv(void) const noexcept -> sCarvingLv::e;
    auto setCarvingLv(const sCarvingLv::e val) noexcept -> void;

    auto getGatheringLv(void) const noexcept -> sGatheringLv::e;
    auto setGatheringLv(const sGatheringLv::e val) noexcept -> void;

    auto getFishingLv(void) const noexcept -> sFishingLv::e;
    auto setFishingLv(const sFishingLv::e val) noexcept -> void;

    auto getEntryFee(void) const noexcept -> u32;
    auto setEntryFee(const u32 val) noexcept -> void;

    auto getVillagePoints(void) const noexcept -> u32;
    auto setVillagePoints(const u32 val) noexcept -> void;

    auto getMainRewardMoney(void) const noexcept -> u32;
    auto setMainRewardMoney(const u32 val) noexcept -> void;

    auto getSubRewardMoney(void) const noexcept -> u32;
    auto setSubRewardMoney(const u32 val) noexcept -> void;

    auto getClearRemVillagePoint(void) const noexcept -> u32;
    auto setClearRemVillagePoint(const u32 val) noexcept -> void;

    auto getFailedRemVillagePoint(void) const noexcept -> u32;
    auto setFailedRemVillagePoint(const u32 val) noexcept -> void;

    auto getSubRemVillagePoint(void) const noexcept -> u32;
    auto setSubRemVillagePoint(const u32 val) noexcept -> void;

    auto getClearRemHunterPoint(void) const noexcept -> u32;
    auto setClearRemHunterPoint(const u32 val) noexcept -> void;

    auto getSubRemHunterPoint(void) const noexcept -> u32;
    auto setSubRemHunterPoint(const u32 val) noexcept -> void;

    auto getRemAddFrame(const std::size_t id) const noexcept(false) -> u8;
    auto getRemAddFrame0(void) const noexcept -> u8;
    auto getRemAddFrame1(void) const noexcept -> u8;

    auto setRemAddFrame(const std::size_t id, const u8 val) noexcept -> bool;
    auto setRemAddFrame0(const u8 val) noexcept -> void;
    auto setRemAddFrame1(const u8 val) noexcept -> void;

    auto getSupply(const std::size_t id) const noexcept(false) -> sSupply;
    auto getSupply0(void) const noexcept -> sSupply;
    auto getSupply1(void) const noexcept -> sSupply;

    auto setSupply(const std::size_t id, const sSupply val) noexcept -> bool;
    auto setSupply0(const sSupply val) noexcept -> void;
    auto setSupply1(const sSupply val) noexcept -> void;

    auto getBoss(const std::size_t id) const noexcept(false) -> sBoss;
    auto getBoss0(void) const noexcept -> sBoss;
    auto getBoss1(void) const noexcept -> sBoss;
    auto getBoss2(void) const noexcept -> sBoss;
    auto getBoss3(void) const noexcept -> sBoss;
    auto getBoss4(void) const noexcept -> sBoss;

    auto setBoss(const std::size_t id, const sBoss val) noexcept -> bool;
    auto setBoss0(const sBoss val) noexcept -> void;
    auto setBoss1(const sBoss val) noexcept -> void;
    auto setBoss2(const sBoss val) noexcept -> void;
    auto setBoss3(const sBoss val) noexcept -> void;
    auto setBoss4(const sBoss val) noexcept -> void;

    auto getSmallEmHPTbl(void) const noexcept -> u8;
    auto setSmallEmHPTbl(const u8 val) noexcept -> void;

    auto getSmallEmAttackTbl(void) const noexcept -> u8;
    auto setSmallEmAttackTbl(const u8 val) noexcept -> void;

    auto getSmallEmOtherTbl(void) const noexcept -> u8;
    auto setSmallEmOtherTbl(const u8 val) noexcept -> void;

    auto getAppear(const std::size_t id) const noexcept(false) -> sAppear;
    auto getAppear0(void) const noexcept -> sAppear;
    auto getAppear1(void) const noexcept -> sAppear;
    auto getAppear2(void) const noexcept -> sAppear;
    auto getAppear3(void) const noexcept -> sAppear;
    auto getAppear4(void) const noexcept -> sAppear;

    auto setAppear(const std::size_t id, const sAppear val) const noexcept -> bool;
    auto setAppear0(const sAppear val) const noexcept -> void;
    auto setAppear1(const sAppear val) const noexcept -> void;
    auto setAppear2(const sAppear val) const noexcept -> void;
    auto setAppear3(const sAppear val) const noexcept -> void;
    auto setAppear4(const sAppear val) const noexcept -> void;

    auto getInvaderAppearChance(void) const noexcept -> u8;
    auto setInvaderAppearChance(const u8 val) noexcept -> void;

    auto getInvaderStartTime(void) const noexcept -> u8;
    auto setInvaderStartTime(const u8 val) noexcept -> void;

    auto getInvaderStartRandChance(void) const noexcept -> u8;
    auto setInvaderStartRandChance(const u8 val) noexcept -> void;

    auto getStrayLimit(const std::size_t id) const noexcept(false) -> u8;
    auto getStrayLimit0(void) const noexcept -> u8;
    auto getStrayLimit1(void) const noexcept -> u8;
    auto getStrayLimit2(void) const noexcept -> u8;

    auto setStrayLimit(const std::size_t id, const u8 val) noexcept -> bool;
    auto setStrayLimit0(const u8 val) noexcept -> void;
    auto setStrayLimit1(const u8 val) noexcept -> void;
    auto setStrayLimit2(const u8 val) noexcept -> void;

    auto getStrayRand2(const std::size_t id) const noexcept(false) -> u8;
    auto getStrayRand2_0(void) const noexcept -> u8;
    auto getStrayRand2_1(void) const noexcept -> u8;
    auto getStrayRand2_2(void) const noexcept -> u8;

    auto setStrayRand2(const std::size_t id, const u8 val) noexcept -> bool;
    auto setStrayRand2_0(const u8 val) noexcept -> void;
    auto setStrayRand2_1(const u8 val) noexcept -> void;
    auto setStrayRand2_2(const u8 val) noexcept -> void;

    auto getSPExtraTicketNum(void) const noexcept -> u8;
    auto setSPExtraTicketNum(const u8 val) noexcept -> void;

    auto getIcon(const std::size_t id) const noexcept(false) -> sIcon::e;
    auto getIcon0(void) const noexcept -> sIcon::e;
    auto getIcon1(void) const noexcept -> sIcon::e;
    auto getIcon2(void) const noexcept -> sIcon::e;
    auto getIcon3(void) const noexcept -> sIcon::e;
    auto getIcon4(void) const noexcept -> sIcon::e;

    auto setIcon(const std::size_t id, const sIcon::e val) noexcept -> bool;
    auto setIcon0(const sIcon::e val) noexcept -> void;
    auto setIcon1(const sIcon::e val) noexcept -> void;
    auto setIcon2(const sIcon::e val) noexcept -> void;
    auto setIcon3(const sIcon::e val) noexcept -> void;
    auto setIcon4(const sIcon::e val) noexcept -> void;

    auto getsGMDLinkCount(void) const noexcept -> std::vector<sGMDLink>::size_type;
    auto getGMDLink(const std::size_t id) const noexcept(false) -> sGMDLink;

    auto setGMDLink(const std::size_t id, const sGMDLink val) -> bool;
    auto setGMDLink0(const sGMDLink val) -> void;
    auto setGMDLink1(const sGMDLink val) -> void;
    auto setGMDLink2(const sGMDLink val) -> void;
    auto setGMDLink3(const sGMDLink val) -> void;
    auto setGMDLink4(const sGMDLink val) -> void;

    // TODO: Header1



    auto getHeader0(void) noexcept -> Header&;
    auto getHeader0(void) const noexcept -> const Header&;

    auto getGMDLinks(void) noexcept -> std::vector<sGMDLink>&;
    auto getGMDLinks(void) const noexcept -> const std::vector<sGMDLink>&;

    auto getHeader1(void) noexcept -> Header1&;
    auto getHeader1(void) const noexcept -> const Header1&;

private:
    auto readHeader(const CContainer& container) noexcept -> bool;
    auto readLinks(const CContainer& container) noexcept -> bool;
    auto readHeader1(const CContainer& container) noexcept -> bool;

    auto fixGMDLinkAmmount(void) -> void;

    friend auto parse(const CContainer& container, cEXT& obj) noexcept -> bool;
};

extern auto parse(const CContainer& container, cEXT& obj) noexcept -> bool;

}; /// namespase ext
}; /// namespase mhxx
}; /// namespace mh
