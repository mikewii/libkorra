#pragma once
#include <libkorra/types.h>
#include <libkorra/tools/ccontainer_fwd.hpp>
#include <libkorra/MHXX/Items/Items.hpp>

namespace mh {
namespace mhxx {
namespace sup {

inline constexpr const u32 RESOURCE_HASH = 0x54539AEE;

struct sSupplyItem {
    sItems::ID ID;
    s8 Ammount;
    s8 Unk;      // posible: rate, available after time, flags
};

struct Header {
    const constexpr static u32 MAGIC      = 0x3F800000;
    const constexpr static u32 VERSION    = 1;
    const constexpr static u32 ITEMS_MAX  = 40;

    u32 Magic;
    u32 Version;

    sSupplyItem Items[ITEMS_MAX];
};

// Supply
class cSUP
{
private:
    bool m_valid = false;
    Header m_header;

public:
    auto isValid(void) const noexcept -> bool;

    auto getItem(const u32 id) noexcept(false) -> sSupplyItem&;
    auto getItem(const u32 id) const noexcept(false) -> const sSupplyItem&;
    auto getItem(const u32 id, sSupplyItem& item) const noexcept -> bool;
    auto setItem(const u32 id, const sSupplyItem& item) noexcept -> bool;

    auto getSupply(void) noexcept -> Header&;
    auto getSupply(void) const noexcept -> const Header&;
    auto setSupply(const Header& supply) noexcept -> void;

private:
    auto readHeader(const CContainer& container) noexcept -> bool;

    friend auto parse(const CContainer& container, cSUP& obj) noexcept -> bool;
};

extern auto parse(const CContainer& container, cSUP& obj) noexcept -> bool;

}; /// namespase sup
}; /// namespase mhxx
}; /// namespace mh
