#pragma once
#include <libkorra/tools/ccontainer_fwd.hpp>
#include <libkorra/types.h>
#include <string>

namespace mh {
namespace mhxx {
namespace qdl {

inline constexpr const u32 RESOURCE_HASH = 0x00E0BB1C;

// TODO: remove...wtf is this?
#define QDL_ERROR 0xF0F0F0F0

enum sQDLItemOrder {
    Boss0 = 0,
    Boss1,
    Boss2,
    Boss3,
    Boss4,
    Intruder, // righ order?
    SmallEm0,
    SmallEm1,
    RewardMainA,
    RewardMainB,
    RewardExtraA,
    RewardExtraB,
    RewardSub,
    Supply,
    QuestPlus,

    COUNT
};

struct sQDLItem {
    static constexpr const u32 QDL_NAME_MAX = 15;

    u32     Resource;
    char    Name[QDL_NAME_MAX + 1];

    std::string getName(void) const noexcept;
    bool        setName(const std::string& str) noexcept;
};

struct Header {
    static constexpr const u32 MAGIC      = 0x434B0000;
    static constexpr const u32 VERSION    = 1;
    static constexpr const u32 ITEMS_MAX  = sQDLItemOrder::COUNT; // 15

    u32         Magic;
    u32         Version;

    sQDLItem    Items[ITEMS_MAX];
};

// QuestDataLink
class cQDL
{
private:
    bool m_valid = false;
    Header m_header;

public:
    auto isValid(void) const noexcept -> bool;

    auto getItem(const sQDLItemOrder id) const noexcept -> sQDLItem;
    auto setItem(const sQDLItemOrder id, const sQDLItem& item) noexcept -> void;

    auto getQuestDataLink(void) noexcept -> Header&;
    auto getQuestDataLink(void) const noexcept -> const Header&;

private:
    auto readHeader(const CContainer& container) noexcept -> bool;

    friend auto parse(const CContainer& container, cQDL& obj) noexcept -> bool;
};

extern auto parse(const CContainer& container, cQDL& obj) noexcept -> bool;

}; /// namespase qdl
}; /// namespase mhxx
}; /// namespace mh
