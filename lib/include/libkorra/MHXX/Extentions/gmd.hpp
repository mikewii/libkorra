#pragma once
#include <libkorra/types.h>
#include <libkorra/tools/ccontainer_fwd.hpp>
#include <vector>
#include <string>

namespace mh {
namespace mhxx {
namespace gmd {

inline constexpr const u32 RESOURCE_HASH = 0x242BB29A; // mhxx

struct Header {
    static constexpr const u32 MAGIC   = 0x00444D47; // GMD
    static constexpr const u32 VERSION = 0x00010302; // u8 major 2, u8 minor 3, u16 patch 1

    u32     Magic;
    u32     Version;

    u32     Padding0;
    float   Unk;
    u32     Padding1;

    u32     LabelsNum;
    u32     ItemsNum;   // can be higher than labelsnum
    u32     LabelsSize;
    u32     ItemsSize;

    u32     FilenameSize;
};

struct sGMDAdvanced1 {
    /*
     * 0 is index
     * 1
     * 2
     * 3
     * 4 is relative pointer after advanced2
     * 5
    */

    u32 Unk[5];
};

struct sGMDAdvanced2 {
    u32 Unk[256];
};

class cGMD
{
private:
    bool m_valid = false;
    Header m_header;
    std::string m_filename;

    std::vector<sGMDAdvanced1> m_advanced1;
    sGMDAdvanced2 m_advanced2;

    std::vector<std::string> m_labels;
    std::vector<std::string> m_items;

public:
    auto isValid(void) const noexcept -> bool;

    auto isAdvanced(void) const noexcept -> bool;

    auto dump(CContainer& container) -> void;

    auto getItemsNum(void) const noexcept -> u32;
    auto getLabelsNum(void) const -> u32;

    auto getLabel(u32 id) -> std::string&;
    auto getLabel(u32 id) const -> const std::string&;
    auto getLabel(u32 id, std::string& str) const noexcept -> bool;

    auto getItem(u32 id) -> std::string&;
    auto getItem(u32 id) const -> const std::string&;
    auto getItem(u32 id, std::string& str) const noexcept -> bool;

    auto appendLabel(const std::string& str) noexcept -> void;
    auto replaceLabel(u32 id, const std::string& str) noexcept -> bool;
    auto removeLabel(u32 id) noexcept -> bool;

    auto appendItem(const std::string& str) noexcept -> void;
    auto replaceItem(u32 id, const std::string& str) noexcept -> bool;
    auto removeItem(u32 id) noexcept -> bool;

    auto getGMDHeader(void) noexcept -> Header&;
    auto getGMDHeader(void) const noexcept -> const Header&;

    auto getFilename(void) noexcept -> std::string&;
    auto getFilename(void) const noexcept -> const std::string&;
    auto setFilename(const std::string& str) noexcept -> void;

    auto getAdvanced1(void) noexcept -> std::vector<sGMDAdvanced1>&;
    auto getAdvanced1(void) const noexcept -> const std::vector<sGMDAdvanced1>&;

    auto getAdvanced2(void) noexcept -> sGMDAdvanced2&;
    auto getAdvanced2(void) const noexcept -> const sGMDAdvanced2&;

    auto getLabels(void) noexcept -> std::vector<std::string>&;
    auto getLabels(void) const noexcept -> const std::vector<std::string>&;

    auto getItems(void) noexcept -> std::vector<std::string>&;
    auto getItems(void) const noexcept -> const std::vector<std::string>&;

private:
    auto getExpectedSize(void) const noexcept -> u32;

    auto readHeader(const CContainer& container) noexcept -> bool;
    auto readFilename(const CContainer& container) noexcept -> bool;
    auto readAdvanced1(const CContainer& container) noexcept -> void;
    auto readAdvanced2(const CContainer& container) noexcept -> void;
    auto readLabels(const CContainer& container) noexcept -> void;
    auto readItems(const CContainer& container) noexcept -> void;

    // Returns total size
    auto updateHeader(bool updateMagicAndVersion = true) noexcept -> u32;

    friend auto parse(const CContainer& container, cGMD& obj) noexcept -> bool;
};

extern auto parse(const CContainer& container, cGMD& obj) noexcept -> bool;

}; /// namespase gmd
}; /// namespase mhxx
}; /// namespace mh
