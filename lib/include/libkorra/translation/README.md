# Translation section
This is instructions for folks who willing to help translate libkorra for other languages.\
There will be support only for languages used in game itself, as i dont see need in support for other languages.\
Currently there is only need to translate MH4U/MH4G.\
If you willing to help, please read instructions down below.

## Instructions
### MH4U/MH4G Translation:
Choose a language you can help with and follow link,\
there you'll see .txt files containing english text required to translate\
[French](MH4U/French)\
[Gernam](MH4U/Greman)\
[Italian](MH4U/Italian)\
[Spanish](MH4U/Spanish)\
[Japanese](MH4U/Japanese)\
[Korean](MH4U/Korean)

## How to submit translation
If you have github account, preferable way is to make pull request\
or you can contact me at Discord mikewii#1590
