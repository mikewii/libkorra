#pragma once
#include <libkorra/types.h>
#include <array>

namespace mh {
namespace mh4u {

struct AppLanguage {
    using size = u8;

    enum e: size {
        EN = 0,     ///< English
        FR,         ///< French
        DE,         ///< German
        IT,         ///< Italian
        ES,         ///< Spanish
        JP,         ///< Japanese
        KO,         ///< Korean

        Count
    };
};

extern AppLanguage::e CurrentLanguage;
extern const std::array<std::array<const char*, AppLanguage::e::Count>, 124> EnemyNames_s;
extern const std::array<std::array<const char*, AppLanguage::e::Count>, 124> IconNames_s;
extern const std::array<std::array<const char*, AppLanguage::e::Count>, 33> EnemyPartNames_s;

}; /// namespace mh4u
}; /// namespace mh
