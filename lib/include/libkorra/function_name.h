#pragma once

#ifndef __FUNCTION_NAME__
#ifdef WIN32
#define __FUNCTION_NAME__   __FUNCTION__
#else
#define __FUNCTION_NAME__   __func__
#endif
#endif

#ifndef __FUNCTION_SIGNATURE__
#ifdef WIN32
#define __FUNCTION_SIGNATURE__ __FUNCSIG__
#else
#define __FUNCTION_SIGNATURE__ __PRETTY_FUNCTION__
#endif
#endif

#include <string>

#ifndef __ERROR__
#define __ERROR__ "[error]"
#endif

#ifndef __OUT_OF_RANGE__
#define __OUT_OF_RANGE__ "[out of range]"
#endif

#ifndef __FUNCTION_SIGNATURE_OUT_OF_RANGE__
#define __FUNCTION_SIGNATURE_OUT_OF_RANGE__ __ERROR__ " " __OUT_OF_RANGE__ " " + std::string(__FUNCTION_SIGNATURE__)
#endif
