#pragma once
/// Aligns a struct (and other types?) to m, making sure that the size of the struct is a multiple of m.
#define ALIGN(m)        __attribute__((aligned(m)))
/// Packs a struct (and other types?) so it won't include padding bytes.
#define PACKED          __attribute__((packed))

#define USED            __attribute__((used))
#define UNUSED          __attribute__((unused))
#define DEPRECATED      __attribute__((deprecated))
#define NAKED           __attribute__((naked))
#define NORETURN        __attribute__((noreturn))
#define ALWAYS_INLINE   __attribute__((always_inline))
#define WEAK            __attribute__((weak))
