#pragma once
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/directory.hpp>

namespace mh {
namespace tools {

namespace fs = boost::filesystem;

class Folder {
public:
    Folder(const fs::path& path);

    const std::vector<fs::path> Get_ListOfFiles(const bool recursive = false) const;

private:
    fs::directory_entry m_path;
};

}; /// namespace tools
}; /// namespace mh
