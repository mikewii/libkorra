#pragma once
#include <libkorra/types.h>

namespace mh {
namespace tools {

union uVersion {
    u32 Raw;

    struct V3 {
        u8 Major;
        u8 Minor;
        u16 Patch;

        constexpr V3(const u8 major, const u8 minor, const u16 patch)
            : Major{major}, Minor{minor}, Patch{patch} {}
    } V3;

    struct V4 {
        u8 Major;
        u8 Minor;
        u8 Patch;
        u8 Tweak;

        constexpr V4(const u8 major, const u8 minor, const u8 patch, const u8 tweak)
            : Major{major}, Minor{minor}, Patch{patch}, Tweak{tweak} {}
    } V4;

    constexpr uVersion()
        : Raw{0} {}
    constexpr uVersion(const u32 version)
        : Raw{version} {}
    constexpr uVersion(const u8 major, const u8 minor, const u16 patch)
        : V3(major, minor, patch) {}
    constexpr uVersion(const u8 major, const u8 minor, const u8 patch, const u8 tweak)
        : V4(major, minor, patch, tweak) {}

    constexpr bool operator == (const uVersion version) const noexcept { return Raw == version.Raw; }
    constexpr bool operator == (const u32 version) const noexcept { return Raw == version; }
};

}; /// namespace tools
}; /// namespace mh
