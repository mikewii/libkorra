#pragma once
#include <libkorra/tools/fs.hpp>
#include <libkorra/tools/ccontainer.hpp>
#include <libkorra/tools/pair.hpp>

#include <vector>
#include <queue>
#include <boost/thread/mutex.hpp>
#include <boost/filesystem/fstream.hpp>

#include <zlib.h>

#include <map>

namespace mh {
namespace tools {

enum class ARCVersion:u16 {
    None        = 0,
    LP1         = 7,
    LP2         = 8,
    MH3U_3DS    = 16,
    MH4U_MHXX   = 17,
    MH4U_1      = 19
};

struct ARCHeader {
    constexpr static const u32 ARC_MAGIC = 0x00435241; /// "ARC\0"
    constexpr static const u32 CRA_MAGIC = 0x41524300; /// "\0CRA"

    u32         Magic;
    ARCVersion  Version;
    u16         FilesNum;

    auto BESwap(void) noexcept -> void;

    auto isARC(void) const noexcept -> bool;
    auto isCRA(void) const noexcept -> bool;
};

struct ARCFileHeader {
    static const u32 ARC_FILEPATH_MAX = 64;

    char    FilePath[ARC_FILEPATH_MAX];
    u32     ResourceHash;
    u32     CompressedSize;

    union {
        u32 Raw;

        struct {
            u32 UncompressedSize : 29;
            u32 Flags : 3;
        };
    };

    u32     pZData; // 78 9C - zlib header

    auto toLittleEndian(void) noexcept -> void;
    auto toBigEndian(void) noexcept -> void;

    auto setFilePath(const std::string& path) noexcept -> bool;
    auto getFilePath(void) const noexcept -> const char*;
    auto getFilePathUnix(void) const noexcept -> std::string;
    auto fixFilePath(void) noexcept -> void;
};

class ARC
{
private:
    bool m_valid;
    bool m_isBE;

    ARCHeader m_header;
    u32 m_padding;
    std::vector<ARCFileHeader> m_arcFileHeaders;
    std::vector<CContainer> m_zlibData;
    CContainer m_container;


    // index, {header, z}
    std::map<std::size_t, std::pair<ARCFileHeader, CContainer>> map;


    std::string                     m_arc_path;
    std::string                     m_out_path;
    std::string                     m_out_folder;
    std::vector<ARCFileHeader>   m_file_headers;
    bool                            m_is_big_endian;

    std::vector<ARCFileHeader>   m_file_out_headers;
    std::vector<fs::path>           m_file_out_paths;
    fs::path                        m_out_file_path = "/home/mw/test/test.arc";
    fs::fstream                     m_out_file;

    std::vector<u32>                m_file_header_locks;
    std::queue<u32>                 m_file_headers_index_queue;
    std::vector<int>                m_res;

    boost::mutex                    m_mutex;

    std::vector<Pair>*              m_pairVec = nullptr;
    const CContainer*               m_container_old = nullptr;

public:
    ARC() = default;
    explicit ARC(CContainer&& container);
    explicit ARC(const CContainer&& container) = delete;

    auto isValid(void) const noexcept -> bool;

    auto readZLIBData(void) noexcept -> bool;
    auto readZLIBData(const CContainer& container) noexcept -> bool;

    ///
    /// \brief uncompress
    /// \param id - index of ARCFileHeader to operate on
    /// \param data - pointer to data
    /// \param output - container to output uncompressed data to
    /// \return success
    ///
    auto uncompress(const std::size_t id,
                    const void* data,
                    CContainer& output) noexcept -> bool;

    auto uncompress(const ARCFileHeader& header,
                    const CContainer& zlibData,
                    CContainer& output) noexcept -> bool;

    auto uncompressAll(std::vector<CContainer>& output) -> bool;
    auto uncompressAll_async(std::vector<CContainer>& output,
                             const std::size_t threadNum = 0) -> bool;

    auto clear(void) noexcept -> void;

    void set_arc_path(const std::string& path) { m_arc_path = path; }
    void set_out_file_path(const std::string& path) { m_out_file_path = path; }

    void make_arc(const std::string& path);
    void make_header(const std::string& path);
    void prepare_write_header(void);
    void write_header(const std::string& path);

    int     decompress(const ARCFileHeader& file_header, const fs::path& dir = "");
    void    decompress_t(void);

    int     compress(ARCFileHeader& file_header, const fs::path& input_file_path, const int level = Z_DEFAULT_COMPRESSION);

    void    Decompress(const u32 id);
    int     Decompress(Pair& sourcePair, Pair& destPair);
    int     Compress(const Pair& sourcePair, Pair& destPair);

    int decompress_all(void);
    void    ExtractAll(void);

    const std::string& get_out_path(void) const { return this->m_out_path; }


    void    print_Header(void);
    void    print_PairsInfo(void);
    void    print_FileInfo(ARCFileHeader* f, u32 n);

    u32     GetFilesCount(void) const { return ARC::m_header.FilesNum; }

    bool is_big_endian(void) const { return this->m_is_big_endian; }

    // Making ARC
    void MakeARC(CContainer& container, std::vector<Pair>& vPair, ARCVersion version = ARCVersion::None);
    void MakeARC_File_s_Header(CContainer& _cc, std::vector<Pair>& _list, u32 _padding, u32 _zDataStart);
    void CopyZData(CContainer& _cc, std::vector<Pair>& _list, u32 _zDataStart);

    auto read_header(void) -> bool;


    auto isBigEndian(void) const noexcept -> bool;
    auto toLittleEndian(void) noexcept -> void;
    auto toBigEndian(void) noexcept -> void;

    auto getHeader(void) noexcept -> ARCHeader&;
    auto getHeader(void) const noexcept -> const ARCHeader&;

    auto getARCFileHeaders(void) noexcept -> std::vector<ARCFileHeader>&;
    auto getARCFileHeaders(void) const noexcept -> const std::vector<ARCFileHeader>&;

private:
    auto checkMagic(const void* data) const noexcept -> bool;
    auto checkVersion(const void* data) const noexcept -> bool;
    auto checkARCHeaderSize(const std::size_t size) const noexcept -> bool;
    auto checkARCFileHeadersSize(const std::size_t size) const noexcept -> bool;
    auto checkZLIBDataSize(const std::size_t size) const noexcept -> bool;

    auto read(const CContainer& container) noexcept -> bool;

    auto readHeader(const void* data) noexcept -> bool;
    auto readARCFileHeaders(const void* data) noexcept -> void;
    auto readZLIBData(const void* data) noexcept -> void;





    void    Read(const CContainer& container);

    const fs::path create_root_folder(void);

    const std::string extract_resource_hash(std::string& path) const;
};

}; /// namespace tools
}; /// namespace mh
