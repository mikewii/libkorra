#pragma once
#include <libkorra/types.h>
#include <string>

namespace mh {
namespace tools {

class CContainer
{
private:
    u8* m_root;
    u8* m_data;
    std::size_t m_size;
    std::size_t m_reservedBefore;
    std::size_t m_reservedAfter;

    static constexpr auto RESERVED_BEFORE = 0x100;
    static constexpr auto RESERVED_AFTER = RESERVED_BEFORE;

public:

    CContainer();
    ~CContainer();
    CContainer(CContainer& other);
    CContainer(const CContainer& other);
    CContainer(CContainer&& other);
    CContainer(const CContainer&& other) = delete;
    explicit CContainer(const size_t size);
    explicit CContainer(const void* data, const std::size_t size);
    explicit CContainer(const std::string& path);

    auto operator = (CContainer&& other) noexcept -> CContainer&;
    auto operator = (const CContainer&& other) noexcept -> CContainer& = delete;
    auto operator = (const CContainer& other) -> CContainer&;
    auto operator == (const CContainer& other) const noexcept -> bool;

    auto swap(CContainer& other) noexcept -> void;

    friend auto swap(CContainer& left, CContainer& right) noexcept -> void
    {
        left.swap(right);
    }

    auto clear(void) -> void;

    auto size(void) const noexcept -> std::size_t;
    auto resize(std::size_t size, bool zeroed = false) -> bool;

    /* for in-memory ops */
    auto setDataPointer(u8* pointer) noexcept -> void;
    auto setSize(const std::size_t size) noexcept-> void;

    // Expanding and shrinking
    auto addBefore(std::size_t size) -> bool;
    auto addAfter(std::size_t size) -> bool;
    auto subBefore(std::size_t size) -> bool;
    auto subAfter(std::size_t size) -> bool;

    auto read(const std::string& path) -> void;
    auto write(const std::string& path) const -> void;

    // Access
    auto data(void) noexcept -> u8*;
    auto data(void) const noexcept -> u8*;

    template <typename T, typename ActualType = typename std::decay<T>::type>
    constexpr auto castAs(const std::size_t index = 0) -> T&
    {
        return reinterpret_cast<ActualType&>(m_data[index * sizeof(T)]);
    }

    template <typename T, typename ActualType = typename std::decay<T>::type>
    constexpr auto castAs(const std::size_t index = 0) const -> const T&
    {
        return reinterpret_cast<const ActualType&>(m_data[index * sizeof(T)]);
    }

    template <typename T, typename ActualType = typename std::decay<T>::type>
    constexpr auto castAt(const std::size_t index = 0) -> T&
    {
        return reinterpret_cast<ActualType&>(m_data[index]);
    }

    template <typename T, typename ActualType = typename std::decay<T>::type>
    constexpr auto castAt(const std::size_t index = 0) const -> const T&
    {
        return reinterpret_cast<const ActualType&>(m_data[index]);
    }

private:
    auto allocate(const std::size_t size, const bool zeroed = false) -> bool;
    auto free(void) -> void;
};

}; /// namespace tools
}; /// namespace mh
