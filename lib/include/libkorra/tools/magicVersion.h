#include <libkorra/tools/magic.h>
#include <libkorra/tools/version.h>

namespace mh {
namespace tools {

struct MagicVersion {
    sMagic Magic;
    uVersion Version;

    constexpr MagicVersion(const u32 magic, const u32 version)
        : Magic{magic}, Version{version} {}

    constexpr bool operator () (const void* data) const noexcept
    {
        const MagicVersion* obj = static_cast<const MagicVersion*>(data);

        return    obj->Magic == Magic
               && obj->Version == Version;
    }
};

}; /// namespace tools
}; /// namespace mh
