#pragma once
#include <libkorra/types.h>
#include <libkorra/attributes.h>
#include <libkorra/tools/ccontainer_fwd.hpp>

#include <climits>
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>

namespace mh {
namespace tools {

namespace endianess {
template <typename T>
constexpr T swap(const T u)
{
    static_assert (CHAR_BIT == 8, "CHAR_BIT != 8");

    union {
        T raw;
        u8 byte[sizeof(T)];
    } source, dest;

    source.raw = std::move(u);

    for (std::size_t i = 0; i < sizeof(T); i++)
        dest.byte[i] = source.byte[sizeof(T) - i - 1];

    return dest.raw;
}
}; /// endianess

namespace convert {
template <typename T>
std::string to_string(const T& u,
                      const bool uppercase = false)
{
    std::stringstream ss;

    union mask {
        T u;
        uint8_t u8[sizeof(T)];
    };

    const mask& mask = reinterpret_cast<const union mask&>(u);

    for (size_t i = 0; i < sizeof(T); i++) {
        if (uppercase)
            ss << std::uppercase;

        ss << std::hex
           << std::setfill('0')
           << std::setw(2)
           << static_cast<u32>(mask.u8[i]);
    }

    return ss.str();
}
}; /// convert

extern void *copyBytes(void* dest,
                       const void* src,
                       const size_t size);

extern void* Copy_UTF16_String(void* dest, const std::u16string& str);

/**
 * @brief Calculate sum of bytes
 * @param _data     CContainer to work on
 * @return          Sum of bytes
 */
u32 calculate_checksum(const CContainer& cc);

/**
 * @brief Calculate sum of bytes
 * @param _data     Pointer to data to work on
 * @param _size     Size of _data
 * @return          Sum of bytes
 */
u32 CalculateChecksum(u8* _data, u32 _size);

/**
 * @brief Prints address of first different byte between data
 * @param _data0    Pointer to data to check agains
 * @param _data1    Pointer to data to check agains
 * @param _size     Size of data, _data0 and _data1 must be same size
 * @return          Pointers to diff data in both data0 and data1
 */
std::pair<u8*, u8*> FindDiff(u8* _data0, u8* _data1, u32 _size);

#ifndef N3DS

class Filter {
public:
    static bool Is_InVector(const std::string& path, const std::vector<std::string>& vector);
};

class Collector {
public:
    struct Info {
        u32 quest_id;
        u32 quest_evel;
        s32 value;
        std::vector<std::string> strings;

        void append_string(const std::string& str) { this->add_string(str); }
        void add_string(const std::string& str) { this->strings.push_back(str); }
    };

    enum class Op {
        Equal,
        NotEqual,
        Less,
        Greater,
        LessEqual,
        GreaterEqual,
        BitPresent,

        Unique,

        Collect
    };

    Collector(){}
    Collector(const std::string& name) : m_output_filename(name) {}

    void Disable(void) { this->m_active = false; }
    bool IsActive(void) const { return this->m_active; }

    void set_filtering_operation(const Collector::Op operation) { Collector::m_filtering_operation = operation; }

    void set_filter_value(const u32 value);
    void set_filter_string(const std::string& str);

    void set_output_path(const std::string& path);

    void Add(const Collector::Info& in);

    void Show(const bool sorted = true);

private:
    bool                    m_active = true;
    Op                      m_filtering_operation = Op::Equal;

    s32                     m_filter_value = 0;
    std::string             m_flter_str;

    std::string             m_output_path;
    std::string             m_output_filename = "collected_data.txt";
    std::vector<Info>       m_info_vec;
    std::vector<s32>        m_vec_unique_ids;
    std::vector<std::string> m_str_formats;

    bool Flush(void);

    void prepare_str_formats(const bool counter = false);
    std::string get_formatted_string(Info& info);

    void clear(void);
};
#endif

}; /// namespace tools
}; /// namespace mh
