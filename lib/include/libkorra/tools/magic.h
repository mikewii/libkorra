#pragma once
#include <libkorra/types.h>

namespace mh {
namespace tools {

struct sMagic {
    u32 Raw;

    constexpr sMagic(const u32 magic)
        : Raw{magic} {}

    constexpr bool operator == (const sMagic& magic) const noexcept { return Raw == magic.Raw; }
    constexpr bool operator == (const u32 magic) const noexcept { return Raw == magic; }
};

}; /// namespace tools
}; /// namespace mh
