#pragma once
#include <libkorra/types.h>
#include <libkorra/tools/ccontainer.hpp>

#define FNAME_SIZE 64

namespace mh {
namespace tools {

struct PairInfo {
    char    Filename[FNAME_SIZE];
    u32     ResourceHash;
    u32     XORLock;
    u32     DecSize = 0;
    bool    isDecompressed = false;

    void print(void) const;
    void print_Filename(void) const;
};

struct Pair {
    PairInfo    info;
    CContainer  cc{};
};

class PairInfoKeeper // for transfering info from one pair to another
{
public:

    /**
     * @brief Saves info from provided Pair
     * @param _pp   Pair to work on
     */
    void Set_PairInfo(const Pair& _pp);

    /**
     * @brief Writes saved info to provided Pair
     * @param _pp   Pair to work on
     */
    bool GetPairInfo(Pair& _pp);

    bool isPairInfoSet(void) const { return m_isSet; }

private:
    PairInfo    m_info;
    bool        m_isSet = false;
};

}; /// namespace tools

using Pair = tools::Pair;

}; /// namespace mh
