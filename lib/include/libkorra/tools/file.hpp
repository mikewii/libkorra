#pragma once
#include <libkorra/tools/ccontainer.hpp>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

/* TODO: file operation overrides for:
 * ctrulib
 * FILE*
 * std
 * boost
 * Qt
 */

namespace mh {
namespace tools {

namespace fs = boost::filesystem;

class File
{
public:
    /**
     * @brief Read file to CContainer
     * @param _fpath    Path with filename
     * @param _cc       CContainer to hold data
     */
    static bool file_to_cc(const fs::path& path, CContainer& cc, const u32 magic = 0);
    static bool file_to_cc_size(const fs::path& path, CContainer& cc, const u32 size);

    /**
     * @brief Write data from CContainer to file
     * @param _fpath    Path with filename
     * @param _cc       CContainer to work with
     */
    static void CC_To_File(const fs::path& path, const CContainer& cc, const bool flush = false);

    static void Data_To_File(const fs::path& path, const void* data, const int size, const bool flush = false);

private:
    static bool Probe(fs::fstream& file, const u32 magic);
};

}; /// namespace tools
}; /// namespace mh
