#pragma once
#include <libkorra/types.h>
#include <libkorra/MH4U/Equipment/Hunter/Equipment.hpp>

namespace mh {
namespace mh4u {

struct sDate {
    u8  Day;
    u8  Month;
    u16	Year;
};

struct sCardList {
    sDate   AcquireDate;
    utf16   Comment[11 + 1];  // or 13(1) no padding
    u32     padding1;
    u32     Union;
    u8		Type;
    u8		padding[3];
    u64     GCID;
};

struct sMonsterLog {
    u16	Hunts; //shown as = hunts + caps
    u16	Captures;
    u16	BigCrown; // Absolute Percent
    u16	SmallCrown; // Absolute Percent

    struct {
        u16	isDiscovered	: 1;
        u16	isBigCrown		: 2; // 0 = none 1 = silver, 2 = gold
        u16	isSmallCrown	: 1; // 0 = none 1 = gold
    };
};

struct sJournal {
    sDate   CompleteDate;
    s16     Type;// 12 cases
    u16     QuestLevel;
    u8      MilestoneType[3]; // 39 cases
    u8      padding; // ?
    u32     PlayerN[3];
    u32     FaintN[3];
    utf16   Player_2_Name[10 + 2];
    u8      padding2;
    utf16   Player_3_Name[10 + 2];
    u8      padding3;
    utf16   Player_4_Name[10 + 2];
    u8      padding4;
    utf16   QuestName[20 + 1];
    u8      padding5;
    u8      PlayerWeapon[4];
    u16     padding6;
};

struct sGuildCard {
    u8      BTM_BackgroundID;
    u8      Title_middleID;
    u16     Title_startID;
    u16     Title_endID;
    u16     HR;
    utf16   PlayerName[10 + 2];
    u16     CaravanLow;
    u16     GHallLow;
    u16     GHlaaHigh;
    u16     GuildQuests;
    u16     Arena;
    u16     CaravanHigh;
    u16     GRank;
    u16     padding1; // ?

    uEquipment  Weapon;
    uEquipment  Head;
    uEquipment  Chest;
    uEquipment  Arms;
    uEquipment  Waiste;
    uEquipment  Legs;
    uEquipment	Talisman;

    struct {
        u32 Unity; // TODO: find division in unity number
    };

    u32     StreetPassTags;
    utf16   Greeting[26 + 1]; // or 62(1)

    u8  Gender;
    u8  Face;
    u8  Voice;
    u8  EyeColor;
    u8  FaceFeature;
    u8  Clothing;
    u8  Hair;
    u8  isRegistered;
    u8  Scene;
    u8  Pose;
    u8  ClothingColor[2]; // X:Y
    u8  HairColor[2];
    u8  FaceFeatureColor[2];
    u8  SkinTone[2];
    u8  unk1[8];
    u8  unk2[64];
    u32 unk3;

    sJournal    JournalNote[10];
    sMonsterLog MonsterLog[59];

    // TODO: finish
};

}; /// namespace mh4u
}; /// namespace mh
