#pragma once
#include <libkorra/translation/MH4U/translation.hpp>
#include <string>
#include <vector>

namespace mh {
namespace mh4u {

struct sSpawnArea {
    using size = u8;

    enum e : size {
        AREA1 = 1,
        AREA2,
        AREA3,
        AREA4,
        AREA5,
        AREA6,
        AREA7,
        AREA8,
        AREA9,
        AREA10
    };
};

struct sQuestType {
    using size = u8;

    enum e : size {
        NONE            = 0,
        SLAY            = 1 << 0,
        CAPTURE         = 1 << 1,
        HUNTING         = SLAY | CAPTURE,
        GATHERING       = 1 << 2,
        HUNT_MULTIPLE   = 1 << 3,
        SPECIAL         = 1 << 4    /* repel, return to wagon */
    };

    static const std::array<const char*, 6> str;

    static const std::vector<const char*> GetStrVec(const e bit);
};

struct sQuestFlags0 {
    using size = u8;

    enum e : size {
        NONE                = 0,
        HUNT_A_TON          = 1 << 0,
        INTRUDER            = 1 << 1,
        REPEL               = 1 << 2,
        TUTORIAL_DELIVERY   = 1 << 3,
        URGENT_QUEST        = 1 << 4,
        KEY_QUEST           = 1 << 5,
        // 1 << 6 not used
        UNK = 1 << 7
        /* this is either:
         * LOCKED - quest locked untill prerequisite completed
         * UNLOCKS - quest unlocks another quest
         * VILLAGE_REQUEST - quest is village request
         * or something else
        */
    };

    static const std::array<const char*, 7> str;

    static const std::vector<const char*> GetStrVec(const e bit);
};

struct sQuestFlags1 {
    using size = u8;

    enum e : size {
        NONE                    = 0,
        EXPEDITION              = 1 << 0, // expedition related
        DELIVER_PAW_PASS_TICKET = 1 << 1,
        ARENA                   = 1 << 2,
        // 1 << 3 not used
        RUSTED_KUSHALA_ICON     = 1 << 4,
        // 1 << 5 not used
        SUB_GOAL                = 1 << 6,
        ARENA_SLAY_ALL          = 1 << 7
    };
};

struct sQuestFlags2 {
    using size = u8;

    enum e : size {
        // 0 2 4 6 8 10 12 14

        // 0 unknown
        // 1 << 0 not used
        // 1 << 1 village request? | Advanced village request?
        LOCKED      = 1 << 2, /* require prerequisite quests completed to unlock */
        UNK1        = 1 << 3 /* Dah'ren Mohran quests*/
    };
};

struct sObjectiveType0 {
    using size = u8;

    enum e : size {
        NONE        = 0,
        HUNT        = 1 << 0,
        DELIVER     = 1 << 1,
        BREAK_PART  = 1 << 2,

        CAPTURE     = 1 << 7 | HUNT
//        SLAY        = 1 << 8 | HUNT
    };
};

struct sObjectiveType1 {
    using size = u8;

    enum e : size {
        NONE                    = 0,
        DELIVERY_DROP           = 1 << 0, // ?
        BREAK_PART             = 1 << 1,

        TOPPLE_WHILE_MOUNTED    = 1 << 5,
        SUPPRESS_FRENZY         = 1 << 6
    };
};

struct sEnemy {
    using size = u16;

    enum e : size {
        NONE = 0,
        RATHIAN,
        RATHALOS,
        PINK_RATHIAN,
        AZURE_RATHALOS,
        GOLD_RATHIAN,
        SILVER_RATHALOS,
        YIAN_KUT_KU,
        BLUE_YIAN_KUT_KU,
        GYPCEROS,
        PURPLE_GYPCEROS,
        TIGREX,
        BRUTE_TIGREX,
        GENDROME,
        IODROME,
        GREAT_JAGGI,
        VELOCIDROME,
        CONGALALA,
        EMERALD_CONGALALA,
        RAJANG,
        KECHA_WACHA,
        TETSUCABRA,
        ZAMTRIOS,
        NAJARALA,
        DALAMADUR_HEAD,
        SELTAS,
        SELTAS_QUEEN,
        NERSCYLLA,
        GORE_MAGALA,
        SHAGARU_MAGALA,
        YIAN_GARUGA,
        KUSHALA_DAORA,
        TEOSTRA,
        AKANTOR,
        KIRIN,
        OROSHI_KIRIN,
        KHEZU,
        RED_KHEZU,
        BASARIOS,
        RUBY_BASARIOS,
        GRAVIOS,
        BLACK_GRAVIOS,
        DEVILJHO,
        SAVAGE_DEVILJHO,
        BRACHYDIOS,
        FURIOUS_RAJANG,
        DAH_REN_MOHRAN,
        LAGOMBI,
        ZINOGRE,
        STYGIAN_ZINOGRE,
        GARGWA,
        RHENOPLOS,
        APTONOTH,
        POPO,
        SLAGTOTH_GREEN,
        SLAGTOTH_RED,
        JAGGI,
        JAGGIA,
        VELOCIPREY,
        GENPREY,
        IOPREY,
        REMOBRA,
        DELEX,
        CONGA,
        KELBI,
        FELYNE,
        MELYNX,
        ALTAROTH,
        BNAHABRA_BLUE,
        BNAHABRA_YELLOW,
        BNAHABRA_GREEN,
        BNAHABRA_RED,
        ZAMITE,
        KONCHU_YELLOW,
        KONCHU_GREEN,
        KONCHU_BLUE,
        KONCHU_RED,
        FATALIS,
        CRIMSON_FATALIS,
        WHITE_FATALIS,
        MOLTEN_TIGREX,
        ROCK_GREY_GREEN,
        RUSTED_KUSHALA_DAORA,
        DALAMADUR_TAIL,
        ROCK_DARK_DIRTY,
        ROCK_BLACK,
        ROCK_ICY1,
        ROCK_ICY2,
        SEREGIOS,
        GOGMAZIOS,
        ASH_KECHA_WACHA,
        BERSERK_TETSUCABRA,
        TIGERSTRIPE_ZAMTRIOS,
        TIDAL_NAJARALA,
        DESERT_SELTAS,
        DESERT_SELTAS_QUEEN,
        SHROUDED_NERSCYLLA,
        CHAOTIC_GORE_MAGALA,
        RAGING_BRACHYDIOS,
        DIABLOS,
        BLACK_DIABLOS,
        MONOBLOS,
        WHITE_MONOBLOS,
        CHAMELEOS,
        ROCK_BROWN,
        CEPHADROME,
        CEPHALOS,
        DAIMYO_HERMITAUR,
        PLUM_D_HERMITAUR,
        HERMITAUR,
        SHAH_DALAMADUR_HEAD,
        SHAH_DALAMADUR_TAIL,
        APEX_RAJANG,
        APEX_DEVILJHO,
        APEX_ZINOGRE,
        APEX_GRAVIOS,
        UKANLOS,
        FLAME_FATALIS,
        APCEROS,
        APEX_DIABLOS,
        APEX_TIDAL_NAJARALA,
        APEX_TIGREX,
        APEX_SEREGIOS,
        REINFORCEMENT
    };

    static const char* GetStr(const e id);
};

struct sIcons {
    using size = u8;

    enum e : size {
        QUESTION_MARK,
        RATHIAN,
        PINK_RATHIAN,
        GOLD_RATHIAN,
        RATHALOS,
        AZURE_RATHALOS,
        SILVER_RATHALOS,
        YIAN_KUT_KU,
        BLUE_YIAN_KUT_KU,
        GYPCEROS,
        PURPLE_GYPCEROS,
        TIGREX,
        BRUTE_TIGREX,
        MOLTEN_TIGREX,
        GENDROME,
        IODROME,
        GREAT_JAGGI,
        VELOCIDROME,
        CONGALALA,
        EMERALD_CONGALALA,
        RAJANG,
        FURIOUS_RAJANG,
        KECHA_WACHA,
        TETSUCABRA,
        ZAMTRIOS,
        NAJARALA,
        SELTAS,
        SELTAS_QUEEN,
        NERSCYLLA,
        GORE_MAGALA,
        SHAGARU_MAGALA,
        YIAN_GARUGA,
        KUSHALA_DAORA,
        RUSTED_KUSHALA_DAORA,
        TEOSTRA,
        AKANTOR,
        KIRIN,
        OROSHI_KIRIN,
        KHEZU,
        RED_KHEZU,
        BASARIOS,
        RUBY_BASARIOS,
        GRAVIOS,
        BLACK_GRAVIOS,
        DEVILJHO,
        SAVAGE_DEVILJHO,
        BRACHYDIOS,
        DAHREN_MOHRAN,
        LAGOMBI,
        ZINOGRE,
        STYGIAN_ZINOGRE,
        GARGWA,
        RHENOPLOS,
        APTONOTH,
        POPO,
        SLAGTOTH,
        RED_SLAGTOTH,
        JAGGI,
        JAGGIA,
        VELOCIPREY,
        GENPREY,
        IOPREY,
        REMOBRA,
        DELEX,
        CONGA,
        KELBI,
        FELIYNE,
        MELYNX,
        ALTAROTH,
        BNAHABRA,
        ZAMITE,
        KONCHU,
        DALAMADUR_HEAD,
        DALAMADUR_TAIL,
        NONE0,
        NONE1,
        NONE2,
        THING,
        ROUND_FACE,
        NONE3,
        NONE4,
        NONE5,
        NONE6,
        NONE7,
        DANGER,
        QUESTION_MARK_2,
        TREASURE_CHEST,
        CROSS,
        FRENZY,
        EGG,
        ROCK,
        FISH,
        BONES,
        BUG_NET,
        MUSHROOM,
        HONEY,
        HARVEST,
        DRAGON_EMBLEM,
        NONE_DEFAULT,
        SEREGIOS,
        ASH_KECHA_WACHA,
        BERSERK_TETSUKABRA,
        TIGERSTRIPE_ZAMTRIOS,
        TIDAL_NAJARALA,
        DESERT_SELTAS,
        DESERT_SELTAS_QUEEN,
        SHROUDED_NERSCYLLA,
        CHAOTIC_GORE_MAGALA,
        RAGING_BRACHYDIOS,
        DIABLOS,
        BLACK_DIABLOS,
        MONOBLOS,
        WHITE_MONOBLOS,
        CHAMELEOS,
        CEPHADROME,
        CEPHALOS,
        DAIMYO_HERMITAUR,
        PLUM_DAIMYO_HERMITAUR,
        HERMITAUR,
        UKANLOS,
        APCEROS,
        SHAH_DALAMADUR_HEAD,
        SHAH_DALAMADUR_TAIL,
        APEX,
    };

    static const char* GetStr(const e id);
};

struct sEnemyParts {
    using size = u16;

    enum e : size {
        PART0 = 0,
        PART1,
        PART2,
        PART3,
        PART4,
        PART5,
        PART7 = 7
    };

    enum PartName : size {
        NONE = 0,
        HEAD,
        HORN,
        HORNS,
        CREST,
        COMB,
        JAW,
        CHIN,
        FEELERS,
        EARS,
        BLOWHOLE,
        BODY,
        CHEST,
        BELLY,
        BACK,
        SHELL,
        POISON_SPIKES,
        HIDE,
        OUTER_HIDE,
        TOP_FIN,
        DORSAL_FIN,
        WING,
        WINGS,
        WINGARM,
        L_WINGARM,
        R_WINGARM,
        WINGTALON,
        CLAW,
        LEG_CLAW,
        HIND_LEG,
        FRONT_LEG,
        FRONT_LEGS,
        TAIL,
    };

    static const std::array<const std::array<PartName, 7>, 124> parts_table; // TODO: find unused parts

    static const char* GetStr(const sEnemy::e enemy_id, const e part_id);
    static const char* GetStr(const PartName id);
    static const std::array<PartName, 7> GetAllParts(const sEnemy::e id);
    static const std::array<const char*, 7> GetAllPartsStr(const sEnemy::e id);
};

struct sQuestStars {
    using size = u16;

    enum e : size {
        Star1 = 1,
        Star2,
        Star3,
        Star4,
        Star5,
        Star6,
        Star7,
        Star8,
        Star9,
        Star10
    };

    static const std::array<const char*, 10> str;

    static const char* getStr(const e id);
};

struct sMap {
    using size = u8;

    enum e : size {
        TEST_AREA = 0, // not?
        ANCESTAL_STEPPE,
        SUNKEN_HOLLOW,
        PRIMAL_FOREST,
        FROZEN_SEAWAY,
        HEAVEN_S_MOUNT,
        GREAT_DESERT,
        TOWER_SUMMIT,
        SPEARTIP_CRAG,
        INGLE_ISLE,
        CASTLE_SCHRADE,
        ARENA,
        SLAYGROUND,
        EVERWOOD,
        GREAT_SEA,
        VOLCANIC_HOLLOW,
        SANCTUARY,
        DUNES_DAY,
        DUNES_NIGHT,
        BATTLEQUARTERS,
        POLAR_FIELD,
        GREAT_SEA_STORM
    };

    static const std::array<const char*, 22> str;

    static const char* getStr(const e id);
};

struct sRequirements {
    using size = u8;

    enum e : size {
        NONE = 0,
        HR_2_OR_HIGHER,
        HR_3_OR_HIGHER,
        HR_4_OR_HIGHER,
        HR_5_OR_HIGHER,
        HR_6_OR_HIGHER,
        HR_7_OR_HIGHER,
        HR_8_OR_HIGHER,
        HR_10_OR_HIGHER,
        HR_20_OR_HIGHER,
        HR_50_OR_HIGHER,
        ONLY_GREATSWORDS,
        ONLY_LANCES,
        ONLY_HAMMERS,
        ONLY_SNS,
        ONLY_LBG_OR_HBG,
        ONLY_DUAL_BLADES,
        ONLY_LONGSWORD,
        ONLY_GUNLANCE,
        ONLY_HUNTING_HORNS,
        ONLY_BOWS,
        ONLY_SWITCHAXES,
        ONLY_INSECT_GLAIVES,
        ONLY_CHARGE_BLADES,
        ONLY_RARE_1_WEAPONS,
        NO_ARMOR_OR_CHARM,
        NO_ITEMS,
        ONLY_1_PLAYER,
        ONLY_2_PLAYERS,
        ONLY_3_PLAYERS,
        PALICO_NEEDED,
        NO_PALICO,
        G1_PERMIT,
        G2_PERMIT,
        G3_PERMIT,
        G_SPECIAL_PERMIT,
        G1_RARE_1_ONLY_AMMO
    };

    static const std::array<const char*, 37> str;

    static const char* getStr(const e id);
};

struct sPlayerSpawnType {
    using size = u8;

    enum e : size {
        BASE_CAMP = 0,
        RANDOM,
        ELDER_DRAGON_FIGHT
    };

    static const std::array<const char*, 3> str;
    static const char* getStr(const e id);
};

struct sGatheringLv {
    using size = u8;

    enum e : size { // 0 1 2 3 4
        TRAINING = 0,
        LR,
        HR,
        G,
        ARENA
    };

    static const std::array<const char*, 5> str;
    static const char* getStr(const e id);
};

struct sCarvingLv {
    using size = u8;

    enum e : size {
        ARENA = 0,
        LR,
        HR,
        G
    };

    static const std::array<const char*, 4> str;
    static const char* getStr(const e id);
};

struct sMonsterAILv {
    using size = u8;

    enum e : size {
        TRAINING = 0,
        LR,
        LP_PLUS,
        HR,
        HR_PLUS,
        G,
        GQ140
    };
    static const std::array<const char*, 7> str;
    static const char* getStr(const e id);
};

struct sArenaFenceEnabled {
    using size = u8;

    enum e : size {
        OFF = 0,
        ON
    };

    static const std::array<const char*, 2> str;
    static const char* getStr(const e id);
};

struct sArenaFenceStatus {
    using size = u8;

    enum e : size {
        DOWN = 0,
        UP
    };

    static const std::array<const char*, 2> str;
    static const char* getStr(const e id);
};

}; /// namespace mh4u
}; /// namespace mh
