#pragma once
#include <libkorra/tools/ccontainer.hpp>
#include <libkorra/MH4U/Quest/Quest.hpp>

namespace mh {
namespace mh4u {

class QuestEditor : virtual protected Crypto
{
    QuestEditor();
    QuestEditor(const CContainer& cc);
    explicit QuestEditor(const fs::path& path);
    ~QuestEditor();

    void initialize(void);

private:
    CContainer m_data;
    sQuest* m_p_header;

    sQuest m_header;
    sFlags m_flags;

    sItemBox m_supply_box;
    sItemBox m_refill_box_1;
    sItemBox m_refill_box_2;
    sItemBox m_refill_box_3;
    sItemBox m_main_reward_box_a;
    sItemBox m_main_reward_box_b;
    sItemBox m_sub_reward_box;

    std::array<sSupplyBoxItem, SUPPLY_BOX_MAX_ITEMS> m_supply_box_items;
    std::array<sSupplyBoxItem, SUPPLY_BOX_MAX_ITEMS> m_refill_box_1_items;
    std::array<sSupplyBoxItem, SUPPLY_BOX_MAX_ITEMS> m_refill_box_2_items;
    std::array<sSupplyBoxItem, SUPPLY_BOX_MAX_ITEMS> m_refill_box_3_items;
    std::array<sSupplyBoxItem, MAIN_REWARD_BOX_A_MAX_ITEMS> m_main_reward_box_a_items;
    std::array<sSupplyBoxItem, MAIN_REWARD_BOX_B_MAX_ITEMS> m_main_reward_box_b_items;
    std::array<sSupplyBoxItem, SUB_REWARD_BOX_MAX_ITEMS> m_sub_reward_box_items;

    std::vector<std::vector<sEnemy_s>> m_large_enemies;
    std::vector<std::vector<std::vector<sEnemy_s>>> m_small_enemies;

    void read_header(void);
};

}; /// namespace mh4u
}; /// namespace mh
