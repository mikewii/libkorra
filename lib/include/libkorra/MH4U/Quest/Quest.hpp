#pragma once
#include <libkorra/tools/fs.hpp>
#include <array>

#include <libkorra/MH4U/Crypto.hpp>
#include <libkorra/MH4U/Quest/Common.hpp>
#include <libkorra/MH4U/Items/Items.hpp>

namespace mh {
namespace mh4u {

#define QUEST_SIZE 0x2010
#define XOR_MH4U_SIZE 8
#define EXT_QUEST_FILES_AMMOUNT 5
#define EXT_QUEST_DATA_AMMOUNT 40
#define EXT_QUEST_DATA_PADDING 0xf0
#define EXT_QUEST_DATA_SIZE (QUEST_SIZE * EXT_QUEST_DATA_AMMOUNT) + XOR_MH4U_SIZE + 0x80

/*
 * reward box screen 1 have 8*2 and 8*4 cells
 * reward box screen 2 have 4*1(green, sub) 4*1(grey) 2*8(red, base) 3*8(blue, wound/cap)
*/
#define SUPPLY_BOX_MAX_ITEMS 5 * 8
#define MAIN_REWARD_BOX_A_MAX_ITEMS 2 * 8
#define MAIN_REWARD_BOX_B_MAX_ITEMS 4 * 8
#define SUB_REWARD_BOX_MAX_ITEMS 4 * 1

template <typename T>
struct Geometry3 {
    T X;
    T Y;
    T Z;
};

enum QuestLanguage {
    JAPANESE = 0,
    KOREAN = JAPANESE,
    ENGLISH = JAPANESE,
    FRENCH,
    SPANISH,
    GERMAN,
    ITALIAN
};

enum Text {
    TITLE = 0,
    MAIN_GOAL,
    FAILURE,
    SUMMARY,
    MAIN_MONSTER,
    CLIENT,
    SUB_QUEST
};

//////
enum EnemyWaveType {
    LargeEnemy = 0,
    SmallEnemy,
};

struct ArenaFence {
    sArenaFenceEnabled::e    IsEnabled;
    sArenaFenceStatus::e     Status;
    u8                      UpTimeSeconds;
    u8                      DownTimeSeconds;
};

struct sEnemy_s {
    sEnemy::e           ID;
    u8                  Padding0[2];

    /* [q0060245] Taiko: Big Bellied Bruisers and
     * [q0000351] have it set to 0
     * but monster still spawns, all other quests have it set at least to 1
     */
    u32                 RespawnCount;
    u8                  SpawnCondition; // for large monsters it always on 255
    sSpawnArea::e        SpawnArea;

    /*
     * [0] 0 1 3 4 5 6      on small monsters only
     * [1] 0 1 2 ... 101    size related?
     * [2] 0 1              size related?
     * [3] 0 1 2 3
     * [4] 0 7 8 9
    */
    u8                  unk[5];

    u8                  FrenzyStatus;   // 0 1 2 3 9 22 24
    Geometry3<float>    Position;

    /*
     * [0] 91 109 182 40778 65172 65354 on small monsters only
     * [1] various up to u16 max
     * [2] 91 182 21481 65354
    */
    u32                 Unk1[3];
};

struct sIntruder {
    u32         AppearChance;
    sEnemy_s    Intruder;
};

struct sSmallMonsterArea {
    u32 p_SmallMonster;
};

union sEnemyWave {
    // ends with 0x00000000 0x00000000 0x00000000 0xFFFF0000 0xFFFFFFFF and(?) 0x00000000 0xFF000000
    // or just null terminated with 6 u32, 6th must be null
    // if null terminated other wave pointers might be placed right after
    // fpr 1 0x00000000 0x00000000 0x00000000 0xFFFF0000 0xFFFFFFFF and(?) 0x00000000 0xFF000000
    // for 2 0x00000000 0x00000000 0xFFFF0000 0xFFFFFFFF and(?) 0x00000000 0xFF000000
    // for 3 0x00000000 0x00000000 0x00000000 0xFFFF0000 0xFFFFFFFF and(?) 0x00000000 0xFF000000
    u32 p_large_enemy_list;
    u32 p_intruder; // 16 byte aligned, terminated by 0xFFFF0000 0xFFFFFFFF
    u32 p_small_enemy_groups;
};

enum ItemBoxID {
    SUPPLY_BOX = 0,
    REFILL_SUPPLIES_1,
    REFILL_SUPPLIES_2,
    REFILL_SUPPLIES_3,
    MAIN_REWARD_BOX_A,
    MAIN_REWARD_BOX_B,
    SUB_REWARD_BOX
};

struct sRewardBoxItem {
    u16         Chance;
    sItems::ID  ID;
    u16         Ammount;
};

struct sSupplyBoxItem {
    sItems::ID  ID;
    u16         Ammount;
};

struct sItemBox {
    u8  ID;
    u8  SlotsNum;
    u16 Unk;

    u32 pItemsBox;
};

struct sText { // null terminated
    u32 p_Title;
    u32 p_MainGoal;
    u32 p_Failure;
    u32 p_Summary;
    u32 p_MainMonster;
    u32 p_Client;
    u32 p_SubQuest;

    u32 get_by_id(const size_t id) const;
};

struct sTextLanguages {
    u32 p_sText_English;
    u32 p_sText_French;
    u32 p_sText_Spanish;
    u32 p_sText_German;
    u32 p_sText_Italian;

    u32 get_by_id(const size_t id) const;
};

struct Objective {

    // type0 1 == hunt
    // type0 4 + type1 64 == suppress frenzy | use enemy_id and count
    // type0 4 + type1 32 == topple while mounted
    // type0 4 + type1 2 == break part | use enemy_id
    u8 type0; // 0 1 2 4 129
    u8 type1; // 0 1 2 8 32 64
    u16 padding0;

    union {
        u16 id;
        sEnemy::e enemy_id;
    };

    union {
        u16 count;
        sEnemyParts::e part;   // 0 1 2 3 4 5 7
    };
};

struct sFlags {
    sQuestType::e    QuestType;
    sQuestFlags0::e  Flags0;
    sQuestFlags1::e  Flags1;
    sQuestFlags2::e  Flags2;

    u32 PostingFee;
    u32 RewardZenny;
    u32 PenaltyZenny;
    u32 SubRewardZenny;

    u32 QuestTime; // minutes
    u32 IntruderProbabilityPercent; // 0 20 30 50 60 70 80 100
    u32 p_sTextLanguages;
    u16 QuestID;
    sQuestStars::e QuestStars;
    sMap::e map;
    sRequirements::e requirements[2];
    u8  padding0;   // null on all quests
    u8  dialog_id;

    u8  padding1[2];

    u8  unk0;   // 1 2 3 - 1 arena single, 2 arena multiple, 3 Steak Your Ground | Gypceros Overload

    Objective main1;
    Objective main2;
    Objective sub;

    u32 p_ChallengePresets;
    u16 quest_icon_id[5];
    u16 unk1;



    sQuestType::e    getQuestType(void) const { return this->QuestType; }
    void            setQuestType(const sQuestType::e type) { this->QuestType = type; }

    sQuestFlags0::e  getFlags0(void) const { return this->Flags0; }
    void            setFlags0(const sQuestFlags0::e flags) { this->Flags0 = flags; }
    sQuestFlags1::e  getFlags1(void) const { return this->Flags1; }
    void            setFlags1(const sQuestFlags1::e flags) { this->Flags1 = flags; }
    sQuestFlags2::e  getFlags2(void) const { return this->Flags2; }
    void            setFlags2(const sQuestFlags2::e flags) { this->Flags2 = flags; }

    u32     getPostingFee(void) const { return this->PostingFee; }
    void    setPostingFee(const u32 value) { this->PostingFee = value; }
    u32     getRewardZenny(void) const { return this->RewardZenny; }
    void    setRewardZenny(const u32 value) { this->RewardZenny = value; }
    u32     getPenaltyZenny(void) const { return this->PenaltyZenny; }
    void    setPenaltyZenny(const u32 value) { this->PenaltyZenny = value; }
    u32     getSubRewardZenny(void) const { return this->SubRewardZenny; }
    void    setSubRewardZenny(const u32 value) { this->SubRewardZenny = value; }

    u32     getQuestTimeMinutes(void) const { return this->QuestTime; }
    void    setQuestTimeMinutes(const u32 value) { this->QuestTime = value; }

    u32     getIntruderProbability(void) const { return this->IntruderProbabilityPercent; }
    void    setIntruderProbability(const u32 value) { this->IntruderProbabilityPercent = value; }

    u32     getQuestId(void) const { return this->QuestID; }
    void    setQuestId(const u32 id) { this->QuestID = id; }

    auto getQuestStars(void) const -> sQuestStars::e { return QuestStars; }
    auto setQuestStars(const sQuestStars::e stars) -> void { QuestStars = stars; }
};

struct MonsterStats {
    u16	size_percentage;            // 0 ... 300
    u8  size_variation_percentage;  // 0 ... 40
    u8  health_percentage;          // 0 ... 95
    u8  attack_percentage;          // 0 ... 119
    u8  defence_percentage;         // 0 ... 140
    u8  stamina;                    // 0 1 2 3 4 table/type?
    u8  unk;                        // 0 1 2
};

struct SmallMonsterUnk  {
    u8  unk[8];
    /*
     * [0] 0 1 2 4 5
     * [1] 0
     * [2] 0
     * [3] 0
     * [4] 0 44 46 ... 73 198
     * [5] 0 3 6
     * [6] 0 1 2 3 ... 58
     * [7] 1 2
    */
};

union HRPoints {
    u32	u;
    s32 s;
};

struct sQuest {
public:
    ////////////////////////////////////////////////////////////
    u32                 p_sFlags;
    char                version[4];

    u32                 p_supply_box;
    u8                  refill_box_settings[4];
    u32                 p_refill_box_1;
    u32                 p_refill_box_2;
    u32                 p_refill_box_3;
    u32                 p_main_reward_box_a;		// 0x1C
    u32                 p_main_reward_box_b;
    u32                 p_sub_reward_box;

    u32                 p_large_monster_waves;	// 0x28
    u32                 p_small_monster_waves;
    u32                 p_IntruderMonster;

    MonsterStats        boss_stats[5];
    MonsterStats        small_mons_stats;
    SmallMonsterUnk     small_mons_unk[2];

    HRPoints            reward_points;
    HRPoints            penalty_points;
    HRPoints            sub_reward_points;

    u8                  unk3[8];
    /*
     * [0] 0 1 2 3 4 5 6 8 10
     * [1] 0
     * [2] 0 10 20 30 40 50 60 70 80 90 100
     * [3] 0
     * [4] 0 2 4 5 15 99
     * [5] 0
     * [6] 0
     * [7] 0
    */
    u8                  unk4; // 0 1 2 3 5 7 bitfield?

    sGatheringLv::e      gathering_level;
    sCarvingLv::e        carving_level;
    sMonsterAILv::e      monsters_ai_level;
    sPlayerSpawnType::e  player_spawn_type;

    ArenaFence          arena_fence;

    u8  padding1[15]; // 16 byte alignment
    ////////////////////////////////////////////////////////////


    enum QuestType {
        NOT_A_QUEST = 0,
        DECODED,
        ENCODED
    };

    static QuestType isQuestFile(const fs::path &path);

    void print(const QuestLanguage language = QuestLanguage::ENGLISH) const;

    bool checkVersion(void) const;

    sFlags* get_sFlags(void);
    const sFlags* get_sFlags(void) const;

    sItemBox* get_item_box(const ItemBoxID id);
    const sItemBox* get_item_box(const ItemBoxID id) const;

    std::vector<sSupplyBoxItem> get_supply_box_items(const ItemBoxID id);

    // Header::sFlags::sTextLanguages::sText
    std::u16string          get_text(const QuestLanguage language, const Text choice);
    const std::u16string    get_text(const QuestLanguage language, const Text choice) const;

    std::vector<std::vector<sEnemy_s>>                      getLargeEnemyVector(void);
    const std::vector<std::vector<sEnemy_s>>                getLargeEnemyVector(void) const;
    std::vector<std::vector<std::vector<sEnemy_s>>>         getSmallEnemyVector(void);
    const std::vector<std::vector<std::vector<sEnemy_s>>>   getSmallEnemyVector(void) const;

private:
    u32 get_item_box_by_id(const ItemBoxID id) const;

    // Header::sFlags
    sTextLanguages*         get_sTextLanguages(void);
    const sTextLanguages*   get_sTextLanguages(void) const;

    // Header::sFlags::sTextLanguages
    sText*                  get_sText(const QuestLanguage language);
    const sText*            get_sText(const QuestLanguage language) const;

    std::vector<sEnemyWave*>               get_enemy_waves(const EnemyWaveType type);
    const std::vector<const sEnemyWave*>   get_enemy_waves(const EnemyWaveType type) const;

    std::vector<std::vector<sEnemyWave*>>               get_small_enemy_groups(void);
    const std::vector<std::vector<const sEnemyWave*>>   get_small_enemy_groups(void) const;
};

}; /// namespace mh4u
}; /// namespace mh
