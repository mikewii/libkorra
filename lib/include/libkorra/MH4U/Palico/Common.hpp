#pragma once
#include <libkorra/types.h>
#include <libkorra/MH4U/Equipment/Hunter/Equipment.hpp>

namespace mh {
namespace mh4u {

enum Coat {
    Coat1,
    Coat2,
    Coat3,
    Coat4,
    Coat5,
    Coat6
};

enum Clothing {
    Clothing1,
    Clothing2
};

enum EyeShape {
    Shape1,
    Shape2,
    Shape3,
    Shape4,
    Shape5,
    Shape6
};

enum Ears {
    Ears1,
    Ears2,
    Ears3
};

enum Tail {
    Tail1,
    Tail2,
    Tail3
};

enum Voice {
    Voice1,
    Voice2,
    Voice3,
    Voice4,
    Voice5
};

}; /// namespace mh4u
}; /// namespace mh
