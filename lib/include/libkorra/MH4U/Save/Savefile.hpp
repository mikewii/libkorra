#pragma once
#include <libkorra/tools/ccontainer_fwd.hpp>
#include <libkorra/MH4U/Save/Common.hpp>
#include <libkorra/MH4U/Save/Options.hpp>
#include <libkorra/MH4U/Equipment/Palico/Equipment.hpp>
#include <libkorra/MH4U/Quest/GuildQuest.hpp>
#include <libkorra/MH4U/GuildCard.hpp>

#include <string>

namespace mh {
namespace mh4u {

struct sMonsterLog_Crowns {
    u16 small;
    u16 big;
};

struct sSavefile {
    utf16       player_name[NAME_LENGTH_MAX + 2];
    Gender      gender;
    Face        face;
    Hairstyle   hairstyle;
    Clothing    clothing;
    Voice       voice;
    EyeColor    eye_color;
    Features    face_features;
    u8          FaceFeatureColor[3];
    u8          HairColor[3];
    u8          ClothingColor[3];
    u8          SkinTone[3];
    u8          padding; // ?
    u32         HunterRank;
    u32         HunterRankPoints;
    u32         Zenny;
    u32         Playtime; // seconds
    u32         padding1;

    uEquipment  Weapon;
    uEquipment  Head;
    uEquipment  Chest;
    uEquipment  Arms;
    uEquipment  Waiste;
    uEquipment  Legs;
    uEquipment  Talisman;

    // 0xFF if none equipped
    u16         WeaponEquipedBoxSlot;
    u16         ChestEquipedBoxSlot;
    u16         ArmsEquipedBoxSlot;
    u16         WaistEquipedBoxSlot;
    u16         LegsEquipedBoxSlot;
    u16         HeadEquipedBoxSlot;
    u16         TalismanEquipedBoxSlot;

    // 0x11A flags?

    u8 unk[76];

    sBoxItem        BoxItems[ITEM_BOX_CELLS_MAX];
    sBoxEquip       BoxEquipment[EQUIPMENT_BOX_CELLS_MAX];
    sBoxPalicoEquip BoxPalicoEquip[PALICO_EQUIPMENT_BOX_CELLS_MAX];

    u16         padding4;
    sPalico     MainPalico;

    /*
     * quest flags
     * story flags
     * world map flags
     * card list
     * hunters for hire
     * current city
     * wycoon
     * village requests
    */

    // Guild card:
    // 0xC598
    u16         GuildCard_MonsterLog_Hunted[static_cast<u16>(GuildCardMonsters::Total)];
    u8          padding5[10];

    u16         GuildCard_MonsterLog_Captured[static_cast<u16>(GuildCardMonsters::Total)];
    u8          padding6[10];

    u8          padding7[4];

    // 0xC79C
    sMonsterLog_Crowns GuildCard_MonsterLog_Size[static_cast<u16>(GuildCardMonsters::Total)];
    u8          padding8[16];

    // 0xC998
    Poogie      poogie;

    u8          flags[320];

    sBoxItem    GunnerPouch[8];
    sBoxItem    ItemPouch[24];

    options::Options    options;

    u8          unk1[4192];

    u8          questFlags[64]; // 08377440 | loads at 00C22054

    u8          unk2[116];

    sGuildQuest GuildQuestRegistered[10];
    u8          unk3[4];
    u32         fillers[3];
    u32         padding9[2];
    u32         CaravanPoints;

    u32         unk4[677];

    // 0xE8A4
    // 0xECB0

    sPalico     FirstStringersPalico[5]; // 0xF338
    sPalico     ReservedPalico[50];
    u32         unk5[60]; // padding?
    sGuildCard  GuildCard; // 0x12600







    const std::u16string    get_player_name(void) const;
    bool                    set_player_name(const std::u16string& name);

    Gender                  get_gender(void) const;
    void                    set_gender(const Gender gender);

    Face                    get_face(void) const;
    void                    set_face(const Face face);

    Hairstyle               get_hairstyle(void) const;
    void                    set_hairstyle(const Hairstyle hairstyle);

    Clothing                get_clothing(void) const;
    void                    set_clothing(const Clothing clothing);

    Voice                   get_voice(void) const;
    void                    set_voice(const Voice voice);

    EyeColor                get_eye_color(void) const;
    void                    set_eye_color(const EyeColor eye_color);

    Features                get_face_features(void) const;
    void                    set_face_features(const Features features);

    // TODO: finish
} PACKED; // remove once finished

class cSavefile
{
public:
    cSavefile();
    cSavefile(CContainer* cc);
    ~cSavefile();

    void assign_header(void* header) { m_header = reinterpret_cast<sSavefile*>(header); }

//private:
    sSavefile* m_header;
};

}; /// namespace mh4u
}; /// namespace mh
