#pragma once
#include <libkorra/types.h>
#include <array>

namespace mh {
namespace mh4u {

struct QuestFlags {
    const char* title;
    const s32   id;
};

constexpr const std::array<const QuestFlags, 488> aQuestFlags =
{{
     /* 00 */   {"Training: Insect Glaive", 11},
     /* 01 */   {"Training: Great Sword", 12},
     /* 02 */   {"Training: Long Sword", 13},
     /* 03 */   {"Training: Sword & Shield", 14},
     /* 04 */   {"Training: Dual Blades", 15},
     /* 05 */   {"Training: Hammer", 16},
     /* 06 */   {"Training: Hunting Horn", 17},
     /* 07 */   {"Training: Lance", 18},

     /* 00 */   {"Training: Gunlance", 19},
     /* 01 */   {"Training: Switch Axe", 20},
     /* 02 */   {"Training: Charge Blade", 21},
     /* 03 */   {"Training: Bow", 22},
     /* 04 */   {"Training: Light Bowgun", 23},
     /* 05 */   {"Training: Heavy Bowgun", 24},
     /* 06 */   {"[Quest from start of the game]", 100},
     /* 07 */   {"Steak Your Ground", 101},

     /* 00 */   {"A Winning Combination", 102},
     /* 01 */   {"Ancestral Steppe Fungus Run", 201},
     /* 02 */   {"Den Mothers", 202},
     /* 03 */   {"Kelbi Cure-All", 203},
     /* 04 */   {"Chip Off the Old Blockade", 204},
     /* 05 */   {"Lair Scare", 205},
     /* 06 */   {"Egg-straction: Gargwa", 206},
     /* 07 */   {"Sushifish SOS", 207},

     /* 00 */   {"Jaggi Alliance", 208},
     /* 01 */   {"The Stinking Seltas", 209},
     /* 02 */   {"Crystal Clearing", 210},
     /* 03 */   {"Bug Appétit", 211},
     /* 04 */   {"", -1},
     /* 05 */   {"", -1},
     /* 06 */   {"Swing into Action", 301},
     /* 07 */   {"Harvest Tour: Anc. Steppe", 302},

     /////////////////////////////////////

     /* 00 */   {"Boulder-bash", 303},
     /* 01 */   {"Rhenoplos Slayer", 304},
     /* 02 */   {"Kecha Wacha Wallop", 305},
     /* 03 */   {"Shipshape Skin", 306},
     /* 04 */   {"Genprey Payback", 307},
     /* 05 */   {"Crystal Bone Collector", 308},
     /* 06 */   {"Road Work", 309},
     /* 07 */   {"Conga Bongo", 310},

     /* 00 */   {"Fish Wish", 311},
     /* 01 */   {"Harvest Tour: Sunken Hollow", 312},
     /* 02 */   {"Egg-straction: Herbivores", 313},
     /* 03 */   {"", 351},
     /* 04 */   {"", -1},
     /* 05 */   {"Nerscylla Thrilla", 401},
     /* 06 */   {"Wild Palico Panic", 402}, // dissapears, need another flag
     /* 07 */   {"Pink Stink", 403},

     /* 00 */   {"Oil Toil", 404},
     /* 01 */   {"Zamite Fight", 405},
     /* 02 */   {"Meownster Hunter Havoc", 406}, // dissapears, need another flag
     /* 03 */   {"Snow with Occasional Lagombi", 407},
     /* 04 */   {"The Smell of Fear", 408},
     /* 05 */   {"Hanging by a Thread", 409},
     /* 06 */   {"Poison Patrol", 410},
     /* 07 */   {"Najarala Hunt", 411},

     /* 00 */   {"The Walls Have Eyes", 412},
     /* 01 */   {"Goldenfish Gold Mine", 413},
     /* 02 */   {"Egg-straction: Wyvern", 414},
     /* 03 */   {"Harvest Tour: Primal Forest", 415},
     /* 04 */   {"Harvest Tour: Frozen Seaway", 416},
     /* 05 */   {"Troverian Trouble", 417},
     /* 06 */   {"An Ice Surprise", 418}, // multiflag?
     /* 07 */   {"Tipping the Scales", 501},

     /////////////////////////////////////

     /* 00 */   {"Pick Your Poison", 502},
     /* 01 */   {"Energy Crisis", 503},
     /* 02 */   {"Learning Experience", 504},
     /* 03 */   {"Major Miner Problem", 505},
     /* 04 */   {"Advanced: Odd Ape Out", 506}, // missing
     /* 05 */   {"Harvest Tour: Volcanic Hollow", 507},
     /* 06 */   {"Uncorner the Market", 508},
     /* 07 */   {"Zinogre Zone", 509},

     /* 00 */   {"The Royal Guard", 510},
     /* 01 */   {"King of the Skies", 511},
     /* 02 */   {"Remobra Remedy", 512},
     /* 03 */   {"Advanced: Khezu Chaos", 513},
     /* 04 */   {"Advanced: En Trade Route", 514},
     /* 05 */   {"Rathalos Renovation", 515},
     /* 06 */   {"Harvest Tour: Heaven's Mount", 516},
     /* 07 */   {"Advanced: Tigrex Terror", 517},

     /* 00 */   {"Ruler of the Frozen Seaway", 518},
     /* 01 */   {"Speartuna Fishing", 519},
     /* 02 */   {"", -1},
     /* 03 */   {"", -1},
     /* 04 */   {"Advanced: Heaven's Wheel", 601}, // Whole Lava Love
     /* 05 */   {"Advanced: Dragon's Din", 603},
     /* 06 */   {"Two Live Crew", 604},
     /* 07 */   {"Advanced: Order of Magnitude", 605}, // missing

     /* 00 */   {"Advanced: Hell Hunters", 606}, // missing
     /* 01 */   {"", -1}, // Advanced: Fleet Action
     /* 02 */   {"Advanced: Shipping Out", 608}, // missing
     /* 03 */   {"Advanced: Miner Issues", 609}, // missing
     /* 04 */   {"Advanced: Predator into Prey", 610}, // missing
     /* 05 */   {"Advanced: Tigrex Challenge", 611},
     /* 06 */   {"Hammer It Out", 612},
     /* 07 */   {"Hammer It Home", 613},

     /////////////////////////////////////

     /* 00 */   {"", -1}, // missing | Advanced: Deep Trouble
     /* 01 */   {"Advanced: Amphibiphobia", 615}, // missing
     /* 02 */   {"Advanced: Afraid of the Dark", 616}, // missing
     /* 03 */   {"Arachnophobia", 617},
     /* 04 */   {"Fear Factor", 618},
     /* 05 */   {"", -1},  // Advanced: Stand Tall
     /* 06 */   {"Egg-straction: Final Mission", 620},
     /* 07 */   {"Mistentoe Delivery", 621},

     /* 00 */   {"Advanced: Howling for Blood", 622},
     /* 01 */   {"Catch and Release", 623},
     /* 02 */   {"See the Lightning", 624},
     /* 03 */   {"", -1},
     /* 04 */   {"Crazy Stupid Love", 626},
     /* 05 */   {"", -1},
     /* 06 */   {"The Caravaneer's Challenge", 2628},
     /* 07 */   {"", -1}, // Research: Kirin - dissapears

     // Gathering Hall:
     /* 00 */   {"Harvest Tour: Anc. Steppe", 10101},
     /* 01 */   {"Harvest Tour: Sunken Hollow", 10102},
     /* 02 */   {"Local Yolk", 10103},
     /* 03 */   {"Fishing's Lure", 10104},
     /* 04 */   {"Ancestral Steppe Fungus Run", 10105},
     /* 05 */   {"Konchu Collector", 10106},
     /* 06 */   {"Bnahabra Bloodbath", 10107},
     /* 07 */   {"Genprey Payback", 10108},

     /* 00 */   {"Rhenoplos Headache", 10109},
     /* 01 */   {"Field Trip", 10110},
     /* 02 */   {"Bug Burger", 10111},
     /* 03 */   {"Kecha Konundrum", 10112},
     /* 04 */   {"Tackling a Tetsucabra", 10113},
     /* 05 */   {"The Gypceros Project", 10114},
     /* 06 */   {"Operation Catch-a-Kecha", 10115},
     /* 07 */   {"Robbed Blind", 10116},

     /////////////////////////////////////

     /* 00 */   {"Web Sighting", 10201}, // missing | hgurgent
     /* 01 */   {"Harvest Tour: Primal Forest", 10202},
     /* 02 */   {"Harvest Tour: Frozen Seaway", 10203},
     /* 03 */   {"Sootstone Search", 10204},
     /* 04 */   {"Bite Your Tongues", 10205},
     /* 05 */   {"Stone-cold Stones", 10206},
     /* 06 */   {"Zamite Fight", 10207},
     /* 07 */   {"Conga Bongo", 10208},

     /* 00 */   {"Gendrome Roadblock", 10209},
     /* 01 */   {"Snow with Occasional Lagombi", 10210},
     /* 02 */   {"Eau de Congalala", 10211},
     /* 03 */   {"Royal Assassination", 10212},
     /* 04 */   {"Nix the Najarala", 10213},
     /* 05 */   {"Zamtrios of the Deep", 10214},
     /* 06 */   {"A Ghastly Gift", 10215},
     /* 07 */   {"Subterranean Serenade", 10216},

     /* 00 */   {"Tails from the Sunken Hollow", 10217},
     /* 01 */   {"Snowed In", 10218},
     /* 02 */   {"Field Study: Primal Forest", 10219},
     /* 03 */   {"Dark Wings, Dark Work", 10301}, // missing | urgent
     /* 04 */   {"Heat Exhaustion", 10302},
     /* 05 */   {"Energy Crisis", 10303},
     /* 06 */   {"Ioprey In the Way", 10304},
     /* 07 */   {"Remobra Remedy", 10305},

     /* 00 */   {"Pick Your Poison", 10306},
     /* 01 */   {"Temper Tantrum", 10307},
     /* 02 */   {"Tigrex Tough Love", 10308},
     /* 03 */   {"Royal Audience", 10309},
     /* 04 */   {"Tuff Turf", 10310},
     /* 05 */   {"The Royal Guard", 10311},
     /* 06 */   {"Queen and Rook", 10312},
     /* 07 */   {"Advanced: Rock and Rolled", 10313},

     /////////////////////////////////////

     /* 00 */   {"Harvest Tour: Heaven's Mount", 10314},
     /* 01 */   {"Harvest Tour: Volcanic Hollow", 10315},
     /* 02 */   {"Rathalos Run", 10316},
     /* 03 */   {"The Angry Couple", 10317},
     /* 04 */   {"Hunters' Heaven", 10318},
     /* 05 */   {"Advanced: Purge and Binge", 10319},
     /* 06 */   {"Advanced: One-man Army", 10320},
     /* 07 */   {"Advanced: Family Business", 10321},

     /* 00 */   {"Advanced: Fearsome Rumblings", 10322},
     /* 01 */   {"Advanced: Approaching Storm", 10323},
     /* 02 */   {"Advanced: Bug Buffet", 10324},
     /* 03 */   {"Advanced: Sand Sailor", 10325}, // missing | urgent
     /* 04 */   {"Harvest Tour: Anc. Steppe", 10402},
     /* 05 */   {"Harvest Tour: Sunken Hollow", 10403},
     /* 06 */   {"Harvest Tour: Primal Forest", 10404},
     /* 07 */   {"Rhenoplos Headache", 10405},

     /* 00 */   {"Bnahabra Bloodbath", 10406},
     /* 01 */   {"Face Two Face", 10407},
     /* 02 */   {"Purple People Eater", 10408},
     /* 03 */   {"Buffoonish Baboon", 10409},
     /* 04 */   {"Dastardly Duo", 10410},
     /* 05 */   {"Tetsucabra Bounty", 10411},
     /* 06 */   {"Operation Catch-a-Kecha", 10412},
     /* 07 */   {"Arachnophilia", 10413},

     /* 00 */   {"Seltas Romance", 10414},
     /* 01 */   {"Pack Attack", 10415},
     /* 02 */   {"Gold in Them There Fish", 10416},
     /* 03 */   {"Splendid Sootstone", 10417},
     /* 04 */   {"Apes of Wrath", 10418},
     /* 05 */   {"Material Effect", 10419},
     /* 06 */   {"Sunken Hollow Showdown", 10420},
     /* 07 */   {"Gypceros Overload", 10421},

     /////////////////////////////////////

     /* 00 */   {"Pink Stink", 10422},
     /* 01 */   {"Distress Signal", 10423},
     /* 02 */   {"Rathian's Wrath", 10501}, // missing | urgent
     /* 03 */   {"Heat Exhaustion", 10502},
     /* 04 */   {"Mistentoe Mission", 10503},
     /* 05 */   {"Zamite Fight", 10504},
     /* 06 */   {"Party Pants", 10505},
     /* 07 */   {"Poisonous Pair", 10506},

     /* 00 */   {"Lagombi Lair", 10507},
     /* 01 */   {"Fin Finder", 10508},
     /* 02 */   {"Cold-blooded Combo", 10509},
     /* 03 */   {"Writer's Block", 10510},
     /* 04 */   {"The Red Menace", 10511},
     /* 05 */   {"Trader in Trouble", 10512},
     /* 06 */   {"Harvest Tour: Frozen Seaway", 10513},
     /* 07 */   {"Harvest Tour: Volcanic Hollow", 10514},

     /* 00 */   {"Harvest Tour: Heaven's Mount", 10515},
     /* 01 */   {"Electrifying Encounter", 10516},
     /* 02 */   {"Royal Rumble", 10517},
     /* 03 */   {"Newlywed Game Hunter", 10518},
     /* 04 */   {"Dread Locked", 10519},
     /* 05 */   {"Topple the Monarch", 10520},
     /* 06 */   {"Queen's Academy", 10521},
     /* 07 */   {"Advanced: Frenzied Foe", 10522},

     /* 00 */   {"Pyres Beware", 10523},
     /* 01 */   {"Temper Tantrum", 10524},
     /* 02 */   {"Dragons' Tale", 10525},
     /* 03 */   {"Gathering of Dragons", 10526},
     /* 04 */   {"Primal Forest M.I.A.", 10527},
     /* 05 */   {"Advanced: Approaching Storm", 10528},
     /* 06 */   {"Advanced: Eerie Aerie", 10529},
     /* 07 */   {"Advanced: Bug Bite", 10530},

     /////////////////////////////////////

     /* 00 */   {"Tigrex by the Tail", 10531},
     /* 01 */   {"Ore D'oeuvre", 10601}, // missing | urgent
     /* 02 */   {"Bring Down the Sky King", 10602},
     /* 03 */   {"Infernal Overlord", 10603},
     /* 04 */   {"Ear Ache", 10604},
     /* 05 */   {"Brimstone and Brachydios", 10605},
     /* 06 */   {"Advanced: Dance with Dragons", 10606},
     /* 07 */   {"Advanced: Mega Hunt-a-thon", 10607},

     /* 00 */   {"Advanced: Dragon Attack", 10608},
     /* 01 */   {"Advanced: Heaven and Earth", 10609},
     /* 02 */   {"Advanced: Forest Gone Wild", 10610},
     /* 03 */   {"Advanced: Armor Amore", 10611},
     /* 04 */   {"Advanced: Arena Emergency", 10612},
     /* 05 */   {"Advanced: Pair of Kings", 10613},
     /* 06 */   {"Advanced: Wake-up Call", 10614},
     /* 07 */   {"Advanced: Gravios Fortunes", 10615},

     /* 00 */   {"Advanced: Insecticide", 10616},
     /* 01 */   {"Advanced: Azure Alert", 10617},
     /* 02 */   {"Advanced: Bag a Brachydios", 10618},
     /* 03 */   {"Advanced: Stop the Wheel", 10701}, // urgent | missing
     /* 04 */   {"Devil of a Problem", 10702},
     /* 05 */   {"Advanced: Grim Tidings", 10703},
     /* 06 */   {"Advanced: Teostra Tangle", 10704},
     /* 07 */   {"Advanced: Storm Front", 10705},

     /* 00 */   {"Advanced: The Raging Rajang", 10706},
     /* 01 */   {"Advanced: Fleet Action", 10707},
     /* 02 */   {"Advanced: Bet Your Life", 10708},
     /* 03 */   {"Advanced: Speartip Menace", 10709}, // urgent | missing
     /* 04 */   {"Ring of the Silver Sol", 10710},
     /* 05 */   {"Ring of the Golden Lune", 10711},
     /* 06 */   {"Sinister Astronomy", 10712},
     /* 07 */   {"Advanced: Seek the Hide", 10713},

     /////////////////////////////////////

     /* 00 */   {"Busted Hell Hunters", 10714},
     /* 01 */   {"Advanced: Stormy Knight", 10716},
     /* 02 */   {"Advanced: More Mohran", 10717},
     /* 03 */   {"Advanced: Enraged Rajang", 10718},
     /* 04 */   {"Advanced: Naked and Afraid", 10719},
     /* 05 */   {"Advanced: Akantor Coup", 10720},
     /* 06 */   {"Advanced: Here to Eternity", 10721},
     /* 07 */   {"Advanced: Fade to Black", 10722},

     /* 00 */   {"Advanced: Queen Slayer", 10723},
     /* 01 */   {"Advanced: King Slayer", 10724},
     /* 02 */   {"Advanced: Glacial Grinder", 10725}, // missing
     /* 03 */   {"", -1}, // a lot of missing
     /* 04 */   {"", -1},
     /* 05 */   {"", -1},
     /* 06 */   {"", -1},
     /* 07 */   {"", -1},

     /* 00 */   {"", -1},
     /* 01 */   {"", -1},
     /* 02 */   {"", -1},
     /* 03 */   {"", -1}, // An Ice Surprise
     /* 04 */   {"", -1},
     /* 05 */   {"", -1},
     /* 06 */   {"Hot-air Buffoon", 702},
     /* 07 */   {"Harvest Tour: Anc. Steppe", 703},

     /* 00 */   {"Harvest Tour: Sunken Hollow", 704},
     /* 01 */   {"Harvest Tour: Primal Forest", 705},
     /* 02 */   {"Harvest Tour: Dunes (Day)", 706},
     /* 03 */   {"Singin' the Blues", 707},
     /* 04 */   {"Purse Snatcher", 708},
     /* 05 */   {"Pungent Pairing", 709},
     /* 06 */   {"Hunt-a-thon: Kecha Wacha", 710},
     /* 07 */   {"Hunt-a-thon: Tetsucabra", 711},

     /////////////////////////////////////

     /* 00 */   {"Hermit Grab", 712},
     /* 01 */   {"Dinner Guests", 713},
     /* 02 */   {"The Hero & the Captain's Trap", 714},
     /* 03 */   {"Facility Facilitation", 715},
     /* 04 */   {"Hunt-a-thon: Nerscylla", 716},
     /* 05 */   {"", -1},
     /* 06 */   {"Hide-and-Freak", 802},
     /* 07 */   {"Take a Powderstone", 803},

     /* 00 */   {"Harvest Tour: Frozen Seaway", 804},
     /* 01 */   {"Harvest Tour: Volcanic Hollow", 805},
     /* 02 */   {"Harvest Tour: Heaven's Mount", 806},
     /* 03 */   {"Fin Finder", 807},
     /* 04 */   {"Primal Pests", 808},
     /* 05 */   {"Bnahabra Buffet", 717},
     /* 06 */   {"Snowbound Showdown", 810},
     /* 07 */   {"Killer Queens", 811},

     /* 00 */   {"In the Pink", 812},
     /* 01 */   {"Harvest Tour: Dunes (Night)", 813},
     /* 02 */   {"Hunt-a-thon: Najarala", 814},
     /* 03 */   {"Desert Delinquents", 815},
     /* 04 */   {"Hi Ho Silver", 816},
     /* 05 */   {"Meteoric Prize", 817},
     /* 06 */   {"The Hero and the Sandy Spear", 818},
     /* 07 */   {"Ladykiller", 819},

     /* 00 */   {"Midnight Madness", 820},
     /* 01 */   {"Nocturnal Commission", 821},
     /* 02 */   {"Hunt-a-thon: Gravios", 822},
     /* 03 */   {"Hunt-a-thon: Zinogre", 823},
     /* 04 */   {"The Hero and the Fiery King", 824},
     /* 05 */   {"Dance of a Thousand Blades", 901}, // missing | urgent
     /* 06 */   {"Black Rock Down", 902},
     /* 07 */   {"Thunder and Stone", 903},

     /////////////////////////////////////

     /* 00 */   {"An Omen in the Skies", 904}, // missing | 2 quests same name
     /* 01 */   {"The Echoing Roar", 905},
     /* 02 */   {"Hunting Vicariously", 906},
     /* 03 */   {"Forge Ahead", 907},
     /* 04 */   {"Advanced: Glacier's Bane", 908}, // missing
     /* 05 */   {"Advanced: Tyrant's Maw", 909}, // missing
     /* 06 */   {"Advanced: Risky Research", 910},
     /* 07 */   {"Advanced: Whale of a Hammer", 911},

     /* 00 */   {"", -1},
     /* 01 */   {"Kushala Kushowdown", 1002}, // missing
     /* 02 */   {"Terrible Tyrant", 1003},
     /* 03 */   {"Devil at Heaven's Gate", 1004},
     /* 04 */   {"Bad Hair Day: Brachydios", 1005},
     /* 05 */   {"Striking Gold", 1006},
     /* 06 */   {"Silver Sovereign", 1007},
     /* 07 */   {"Egg-straction: The Reckoning", 1008},

     /* 00 */   {"Advanced: Red Dragon's Dawn", 1009},
     /* 01 */   {"Advanced: A Hero's Icy Test", 1010},
     /* 02 */   {"1000 Shimmering Swords", 1011},
     /* 03 */   {"Advanced: Moving Mountains", 1012},
     /* 04 */   {"Icy Shadow", 1013},
     /* 05 */   {"Shrouded in Mystery", 1014},
     /* 06 */   {"Into the Mist", 1015},
     /* 07 */   {"Advanced: Elder Dragon of Mist", 1016},

     /* 00 */   {"Bad Hair Day: Iodrome", 1017},
     /* 01 */   {"Bad Hair Day: Monoblos", 1018},
     /* 02 */   {"Advanced: Bad Hair Rajang", 1019},
     /* 03 */   {"Advanced: Beyond the Pale", 1020},
     /* 04 */   {"The Silvery Spear", 1021},
     /* 05 */   {"Advanced: All in Its Place", 1022},
     /* 06 */   {"Haughty Hell Hunters", 1023},
     /* 07 */   {"Harebrained Hell Hunters", 1024},

     /////////////////////////////////////

     /* 00 */   {"Duel With the Devil", 1025},
     /* 01 */   {"Advanced: Hell Hunter Hoax", 1026},
     /* 02 */   {"Puffer Fish", 1027},
     /* 03 */   {"Advanced: The Master's Test", 1028},
     // Elder Hall:
     /* 04 */   {"Fashion Victim", 10801},
     /* 05 */   {"Line in the Sand", 10802},
     /* 06 */   {"Berserker Rage", 10803},
     /* 07 */   {"Seeing Spots", 10804},

     /* 00 */   {"Sculptural Seltas", 10805},
     /* 01 */   {"Harvest Tour: Anc. Steppe", 10807},
     /* 02 */   {"Harvest Tour: Sunken Hollow", 10808},
     /* 03 */   {"Harvest Tour: Primal Forest", 10809},
     /* 04 */   {"Harvest Tour: Dunes (Day)", 10810},
     /* 05 */   {"Primate Plunder", 10811},
     /* 06 */   {"Purple Pros", 10812},
     /* 07 */   {"Chasing Tail", 10813},

     /* 00 */   {"Lumber Support", 10814},
     /* 01 */   {"Netting Neopterons", 10815},
     /* 02 */   {"Birthday Berries", 10816},
     /* 03 */   {"Advanced: Terrible Twins", 10817},
     /* 04 */   {"Advanced: Bug-Be-Gone", 10818},
     /* 05 */   {"Hunger Games", 10819},
     /* 06 */   {"Hit List: Plum Daimyo Hermitaur", 10820},
     /* 07 */   {"Grand Finals Confrontation", 10821},

     /* 00 */   {"Serpent Serenade", 10822},
     /* 01 */   {"Hunter In Green and Red", 10823},
     /* 02 */   {"Harvest Tour: Frozen Seaway", 10824},
     /* 03 */   {"Death and Taxidermy", 10825},
     /* 04 */   {"Lagombi Lair", 10826},
     /* 05 */   {"Advanced: Chumming the Waters", 10827},
     /* 06 */   {"All-You-Can-Hunt Zamite", 10828},
     /* 07 */   {"Advanced: Claw Clash", 10829},

     /////////////////////////////////////

     /* 00 */   {"Advanced: Tiger-sharq", 10830},
     /* 01 */   {"Serpentine Samba", 10831},
     /* 02 */   {"Advanced: Frenzied Trading", 10832},
     /* 03 */   {"Pink Problems", 10833},
     /* 04 */   {"Royal Assassination", 10834},
     /* 05 */   {"Hunt-a-thon: Khezu", 10835},
     /* 06 */   {"La Vie en Rose", 10836},
     /* 07 */   {"Advanced: Hollow Hysterics", 10837},

     /* 00 */   {"Seer of Swords", 10901},
     /* 01 */   {"Course Correction", 10902},
     /* 02 */   {"How to Zap Your Zinogre", 10903},
     /* 03 */   {"Temper Tantrum", 10904},
     /* 04 */   {"The Azure King and the Tyrant", 10905},
     /* 05 */   {"Harvest Tour: Volcanic Hollow", 10906},
     /* 06 */   {"Harvest Tour: Heaven's Mount", 10907},
     /* 07 */   {"Harvest Tour: Dunes (Night)", 10908},

     /* 00 */   {"All-You-Can-Hunt Remobra", 10909},
     /* 01 */   {"Sovereign of the Sky", 10910},
     /* 02 */   {"Strutting the Royal Runway", 10911},
     /* 03 */   {"Out for the Count", 10912},
     /* 04 */   {"Rival Clash", 10913},
     /* 05 */   {"Advanced: Spider Study", 10914},
     /* 06 */   {"Advanced: Bolt of Pink", 10915},
     /* 07 */   {"Ring Hollow", 10916},

     /* 00 */   {"Azure Arrival", 10917},
     /* 01 */   {"Beast Not Quite Busted", 10918},
     /* 02 */   {"Advanced: Mean and Green", 10919},
     /* 03 */   {"Showdown at High Noon", 10920},
     /* 04 */   {"Showdown at Midnight", 10921},
     /* 05 */   {"Brute Force", 10922},
     /* 06 */   {"Hollow Promise", 10923},
     /* 07 */   {"Pestering Pest", 10924},

     /////////////////////////////////////

     /* 00 */   {"Fire Drill", 10925},
     /* 01 */   {"Locked Horns", 10926},
     /* 02 */   {"Advanced: A Black Day", 10927},
     /* 03 */   {"Advanced: Escaped Convicts", 10928},
     /* 04 */   {"Fire and Ice", 10929},
     /* 05 */   {"Mountain Rescue", 10930},
     /* 06 */   {"Advanced: A Plague Awoken", 11001},
     /* 07 */   {"Advanced: A Formless Fiend", 11002},

     /* 00 */   {"Operation Tongue Twister", 11003},
     /* 01 */   {"Advanced: Emperor of Embers", 11004},
     /* 02 */   {"Operation Fire Extinguisher", 11005}, // missing | urgent
     /* 03 */   {"Advanced: A Storm of Steel", 11006},
     /* 04 */   {"Operation Windbreaker", 11007}, // missing | urgent
     /* 05 */   {"Advanced: Icy Investigation", 11008},
     /* 06 */   {"Advanced: Fur Fixation", 11009},
     /* 07 */   {"Into the Heavens", 11010},

     /* 00 */   {"Advanced: The Scathing Shore", 11011},
     /* 01 */   {"Advanced: Lab Partners", 11012},
     /* 02 */   {"Advanced: Misty Challenge", 11013},
     /* 03 */   {"Advanced: Quagmire Quarrel", 11014},
     /* 04 */   {"Advanced: Wheel of Time", 11015},
     /* 05 */   {"Grim Quartet", 11016},
     /* 06 */   {"Advanced: Fury on the Mount", 11017},
     /* 07 */   {"Advanced: Tigrex In Effect", 11018},

     /* 00 */   {"Operation Tigrex Tamer", 11019}, // missing | urgent
     /* 01 */   {"Advanced: The Wailing Devil", 11020},
     /* 02 */   {"Advanced: Gravios Situation", 11021},
     /* 03 */   {"Advanced: Rhapsody in Rime", 11022},
     /* 04 */   {"Advanced: Winds of Discord", 11023},
     /* 05 */   {"Operation Rust Remover", 11024}, // missing | urgent
     /* 06 */   {"The Sky Is Falling", 11025},
     /* 07 */   {"Advanced: Dark Domination", 11026},

     /////////////////////////////////////

     /* 00 */   {"Advanced: 1,001 Shards", 11027},
     /* 01 */   {"Operation Swordbreaker", 11028}, // missing | urgent
     /* 02 */   {"Advanced: Wings of Woe", 11029},
     /* 03 */   {"Advanced: Eternal Emperor", 11030},
     /* 04 */   {"Advanced: Beyond Brawn", 11031},
     /* 05 */   {"Operation Lionheart", 11032}, // missing | urgent
     /* 06 */   {"Advanced: Achy Brachy Heart", 11033},
     /* 07 */   {"Advanced: Silver Cataclysm", 11034},

     /* 00 */   {"Advanced: Act of Gog", 11035},
     /* 01 */   {"Advanced: A Final Battle Cry", 11036},
     /* 02 */   {"Advanced: Monster Hunter!", 11037},
     // Episode: Down to Business
     /* 03 */   {"Bonus: Stash Grab", 62101},
     /* 04 */   {"Bonus: Net Profits", 62102},
     /* 05 */   {"Bonus: A Bigger Boat", 62103},
     // Episode: Code 16010
     /* 06 */   {"Bonus: Code Purple", 62104},
     /* 07 */   {"Bonus: Code Black", 62105},

     /* 00 */   {"Bonus: Code White", 62106},
     // Episode: Of Masks & Monsters
     /* 01 */   {"Bonus: Miracle Mask", 62201},
     /* 02 */   {"Bonus: Mystical Mask", 62202},
     /* 03 */   {"Bonus: Running With the Devil", 62203},
     // Episode: Inimitable Instructor
     /* 04 */   {"Bonus: Awaken!", 62204},
     /* 05 */   {"Bonus: Take Flight!", 62205},
     /* 06 */   {"Bonus: Into Eternity!", 62206},
     // Episode: Sweetheart Square Off
     /* 07 */   {"Bonus: A Lady's Lament", 62207},

     /* 00 */   {"Bonus: A Maiden's Mission", 62208},
     /* 01 */   {"Bonus: A Girl's Gumption", 62209},
     // Episode: Lay of the Land
     /* 02 */   {"Bonus: A Spreading Scourge", 62210},
     /* 03 */   {"Bonus: Mist Amid the Ruins", 62211},
     /* 04 */   {"Bonus: Total Eclipse", 62212},
     /* 05 */   {"", -1}, // unknown
     /* 06 */   {"", -1}, // unknown
     /* 07 */   {"", -1}, // unknown

     /////////////////////////////////////

     /* 00 */   {"", -1}, // unknown
     /* 01 */   {"", -1}, // unknown
     /* 02 */   {"", -1}, // unknown
     /* 03 */   {"", -1}, // unknown
     /* 04 */   {"", -1}, // unknown
     /* 05 */   {"", -1}, // unknown
     /* 06 */   {"", -1}, // unknown
     /* 07 */   {"", -1}, // unknown


     /* the end */
}};

// not found:
//{"Advanced: Winds of Discord", 11038}, // missing
//{"Grudge Match: Yian Kut-Ku", 20001},
//{"Grudge Match: Kecha Wacha", 20002},
//{"Grudge Match: Bug Out", 20003},
//{"Grudge Match: Zamtrios", 21001},
//{"Grudge Match: Brute Tigrex", 21002},
//{"Grudge Match: S. Zinogre", 21003},
//{"Grudge Match: Deviljho", 21004},
//{"Grudge Match: Color Code", 21005},
//{"Grudge Match: Dos Gravi", 21006},
//{"Grudge Match: Shell Game", 21007},
//{"Grudge Match: Fish Fry", 22001},
//{"Grudge Match: Tidal Najarala", 22002},
//{"Grudge Match: S. Nerscylla", 22003},
//{"Grudge Match: Dual Devils", 22004},
//{"Grudge Match: Apex Deviljho", 22005},
//{"Grudge Match: Triplets", 22006},
//{"Expedition, Ho!", 45000},
//{"High-rank Expedition, Ho!", 45001},
//{"G-rank Expedition, Ho!", 45002},
//{"Wasteland Warrior", 1030},
//{"Research: Velocidrome", 1250},
//{"Research: Yian Kut-Ku", 1251},
//{"Research: Basarios", 1350},
//{"An Omen in the Skies", 912},
//{"Death of a Thousand Cuts", 1001},
//{"Skiff Scuttler", 701},
//{"Gore Magala Drama", 1450},
//{"Research: Yian Garuga", 1550},
//{"Research: Kirin", 1650},
//{"Whole Lava Love", 2602},
//{"Advanced: Fleet Action", 2607},
//{"Advanced: Deep Trouble", 2614},
//{"Advanced: Stand Tall", 2619},
//{"Advanced: Wicked Wings", 2625},
//{"Advanced: Tough Love", 2627},
//{"Skiff Competition", 801},
//{"Advanced: Prickly Pair", 1029},

//// events:
//{"", 45100},
//{"", 45101},
//{"", 45102},
//{"", 45103},
//{"", 45104},
//{"", 45105},
//{"", 45106},
//{"", 45107},
//{"", 45108},
//{"", 45109},
//{"", 45200},
//{"", 45201},
//{"", 45202},
//{"", 45203},
//{"", 45204},
//{"", 45205},
//{"", 45206},
//{"", 45207},
//{"", 45208},
//{"", 45209},
//{"", 45210},
//{"", 45211},
//{"", 45212},
//{"", 45213},
//{"", 45214},
//{"", 45215},
//{"USJ: High-fire Act", 60001},
//{"The Poogie King", 60003},
//{"Fan Club: Remobra Removal", 60009},
//{"Uniqlo: Hunt for Inspiration", 60010},
//{"Sand Blasted", 60011},
//{"Rollin' Rollin' Rollin'", 60012},
//{"USJ: Hot Ticket", 60101},
//{"USJ: Zamtrios 3D", 60102},
//{"Gravios May Cry", 60104},
//{"The Steel Vanguard", 60109},
//{"Uniqlo: Material Needs", 60113},
//{"A Lost Civilization", 60120},
//{"Fan Club: Two-Toned Titans", 60121},
//{"Three Virtues", 60122},
//{"Fire Fight", 60123},
//{"Mario: Oh, Brothers!", 60124},
//{"The Devil's Due", 60125},
//{"Enter the Red Dragon", 60126},
//{"Return of the Dragon", 60127},
//{"Royal Restoration", 60128},
//{"Tower of Trouble", 60129},
//{"Eye of the Tigrex", 60130},
//{"Pillar of Strength", 60131},
//{"All the Rage", 60132},
//{"Out of Time", 60133},
//{"Might and Melody", 60134},
//{"Kirin Acquisition", 60135},
//{"Leaping Terror", 60136},
//{"Heavy Metal", 60137},
//{"Plain & Carefree", 60138},
//{"Today's Special: Meat", 60140},
//{"Animal Crossing: Fisher King", 60141},
//{"Super Sonic Seregios", 60142},
//{"The Brave Warrior", 60201},
//{"Bewitching Blossoms", 60202},
//{"Only the Strong Survive", 60203},
//{"Foreboding Lightning", 60204},
//{"A Song of Extremes", 60205},
//{"Desert Strike", 60206},
//{"The Silver King's Slumber", 60207},
//{"Ruby of my Eye", 60208},
//{"The Speartip's King", 60209},
//{"Bedeviled Deviljho", 60210},
//{"Take the Black", 60211},
//{"To Victory and Beyond", 60212},
//{"The Crimson Shah", 60213},
//{"An Unstoppable Rage", 60214},
//{"A Feast for the Departed", 60215},
//{"Bombs over Gogmazios", 60216},
//{"Sacred Wind", 60217},
//{"Super Smash Monsters", 60218},
//{"Poison Pinch", 60219},
//{"Crouching Tiger, Hidden Shark", 60220},
//{"Hunter's Log: Yian Kut-Ku", 60221},
//{"Hear No Evil, See No Evil", 60222},
//{"A Small Mountain to Scale", 60223},
//{"Bug Hunters Inc.", 60224},
//{"The Ultimate Gravios", 60225},
//{"Big Game Hunting", 60226},
//{"Horn To Be Wild", 60227},
//{"Little Big Hermitaur", 60228},
//{"USJ: Tigrex 3D", 60229},
//{"USJ: A Colorful Feast", 60230},
//{"The Candle of Darkness", 60235},
//{"Fan Club: Desert Training", 60237},
//{"Fan Club: Ashes to Ashes", 60238},
//{"Protector of Peace", 60239},
//{"Hunter's Log: Rath", 60240},
//{"Hunter's Log: Small Fry", 60241},
//{"Metroid: Special Mission", 60242},
//{"Metroid: Looming Shadows", 60243},
//{"Primeval Slugfest", 60244},
//{"Taiko: Big Bellied Bruisers", 60245},
//{"Twilight of the Gods", 60246},
//{"The Apex Predator", 60247},
//{"Red, White, and You", 60248},
//{"Beyond the Crimson Veil", 60249},
//{"Fight for the Future", 60251},
//{"Challenge Quest 1", 61001},
//{"Challenge Quest 4", 61002},
//{"Party Challenge 1", 61003},
//{"Challenge Quest 2", 61101},
//{"Challenge Quest 3", 61102},
//{"Challenge Quest 5", 61103},
//{"Challenge Quest 6", 61104},
//{"Challenge Quest 7", 61105},
//{"Challenge Quest 8", 61106},
//{"Challenge Quest 9", 61107},
//{"Challenge Quest 10", 61108},
//{"Challenge Quest 11", 61109},
//{"Party Challenge 2", 61110},
//{"Monster Fest 1", 61111},
//{"Monster Fest 7", 61112},
//{"Monster Fest 5", 61113},
//{"Challenge Quest 12", 61201},
//{"Challenge Quest 13", 61202},
//{"Party Challenge 3", 61203},
//{"Monster Fest 2", 61204},
//{"Monster Fest 3", 61205},
//{"Monster Fest 4", 61206},
//{"Monster Fest 8", 61207},
//{"Monster Fest 6", 61208},

}; /// namespace mh4u
}; /// namespace mh
