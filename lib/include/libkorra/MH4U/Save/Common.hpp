#pragma once
#include <libkorra/types.h>
#include <libkorra/MH4U/Equipment/Hunter/Equipment.hpp>

namespace mh {
namespace mh4u {

#define SAVEFILE_SIZE_BYTES 0x13E00
#define NAME_LENGTH_MAX 10
#define PLAYER_NAME_SIZE_BYTES_MAX ((NAME_LENGTH_MAX + 2) * sizeof(utf16))
#define ITEM_BOX_CELLS_MAX 100 * 14
#define EQUIPMENT_BOX_CELLS_MAX 100 * 15
#define PALICO_EQUIPMENT_BOX_CELLS_MAX 100 * 6

struct Poogie {
    u8      unk[2];
    utf16   name[NAME_LENGTH_MAX + 2];
    u16     costumeID;
};

enum Gender : u8 {
    MALE = 0,
    FEMALE
};

enum Face : u8 {
    Face1 = 0,
    Face2,
    Face3,
    Face4,
    Face5,
    Face6,
    Face7,
    Face8,
    Face9,
    Face10,
    Face11,
    Face12,
    Face13,
    Face14,
    Face15,
    Face16
};

enum Hairstyle : u8 {
    Hair1 = 0,
    Hair2,
    Hair3,
    Hair4,
    Hair5,
    Hair6,
    Hair7,
    Hair8,
    Hair9,
    Hair10,
    Hair11,
    Hair12,
    Hair13,
    Hair14,
    Hair15,
    Hair16,
    Hair17,
    Hair18,
    Hair19,
    Hair20,
    Hair21,
    Hair22,
    Hair23,
    Hair24,
    Hair25,
    Hair26,
    Hair27,
    Hair28
};

enum Clothing : u8 {
    Clothing1 = 0,
    Clothing2,
    Clothing3,
    Clothing4,
    Clothing5,
    Clothing6
};

enum Voice : u8 {
    Voice1 = 0,
    Voice2,
    Voice3,
    Voice4,
    Voice5,
    Voice6,
    Voice7,
    Voice8,
    Voice9,
    Voice10,
    Voice11,
    Voice12,
    Voice13,
    Voice14,
    Voice15,
    Voice16,
    Voice17,
    Voice18,
    Voice19,
    Voice20
};

enum EyeColor : u8 {
    EyeColor1 = 0,
    EyeColor2,
    EyeColor3,
    EyeColor4,
    EyeColor5,
    EyeColor6,
    EyeColor7,
    EyeColor8,
    EyeColor9,
    EyeColor10
};

enum Features : u8 {
    FeaturesNone = 0,
    Features1,
    Features2,
    Features3,
    Features4,
    Features5,
    Features6,
    Features7,
    Features8,
    Features9,
    Features10,
    Features11,
    Features12
};

enum class GuildCardMonsters : u16 {
    None = 0,
    Rathian,
    Rathalos,
    PinkRathian,
    AzureRathalos,
    GoldRathian,
    SilverRathalos,
    YianKutKu,
    BlueYianKutKu,
    Gypceros,
    PurpleGypceros,
    Tigrex,
    BruteTigrex,
    Gendrome,
    Iodrome,
    GreatJaggi,
    Velocidrome,
    Congalala,
    EmeraldCongalala,
    Rajang,
    KechaWacha,
    Tetsucabra,
    Zamtrios,
    Najarala,
    DalamadurHead,
    Seltas,
    SeltasQueen,
    Nerscylla,
    GoreMagala,
    ShagaruMagala,
    YianGaruga,
    KushalaDaora,
    Teostra,
    Akantor,
    Kirin,
    OroshiKirin,
    Khezu,
    RedKhezu,
    Basarios,
    RubyBasarios,
    Gravios,
    BlackGravios,
    Deviljho,
    SavageDeviljho,
    Brachydios,
    FuriousRajang,
    DahrenMohran,
    Lagombi,
    Zinogre,
    StygianZinogre,
    Gargwa,
    Rhenoplos,
    Aptonoth,
    Popo,
    SlagtothGreen,
    SlagtothRed,
    Jaggi,
    Jaggia,
    Velociprey,
    Genprey,
    Ioprey,
    Remobra,
    Delex,
    Conga,
    Kelbi,
    Felyne,
    Melynx,
    Altaroth,
    BnahabraBlue,
    BnahabraYellow,
    BnahabraGreen,
    BnahabraRed,
    Zamite,
    KonchuYellow,
    KonchuGreen,
    KonchuBlue,
    KonchuRed,
    Fatalis,
    CrimsonFatalis,
    WhiteFatalis,
    MoltenTigrex,
    Rock0,
    RustedKushalaDaora,
    DalamadurTail,
    Rock1,
    Rock2,
    Rock3,
    Rock4,
    Seregios,
    Gogmazios,
    AshKechaWacha,
    BerserkTetsucabra,
    TigerstripeZamtrios,
    TidalNajarala,
    DesertSeltas,
    DesertSeltasQueen,
    ShroudedNerscylla,
    ChaoticGoreMagala,
    RagingBrachydios,
    Diablos,
    BlackDiablos,
    Monoblos,
    WhiteMonoblos,
    Chameleos,
    Rock5,
    Cephadrome,
    Cephalos,
    DaimyoHermitaur,
    PlumDHermitaur,
    Hermitaur,
    ShahDalamadurHead,
    ShahDalamadurTail,
    ApexRajang,
    ApexDeviljho,
    ApexZinogre,
    ApexGravios,
    Ukanlos,
    FlameFatalis,
    Apceros,
    ApexDiablos,
    ApexTidalNajarala,
    ApexTigrex,
    ApexSeregios,

    Total
};

enum class GuildCard_MonsterLog : u16 {
    Seltas = 0,
    DesertSeltas,
    Lagombi,
    GreatJaggi,
    Velocidrome,
    Gendrome,
    Iodrome,
    Cephadrome,
    KechaWacha,

    AshKechaWacha,
    Tetsucabra,
    BerserkTetsucabra,
    Zamtrios,
    TigerstripeZamtrios,
    Najarala,
    TidalNajarala,
    SeltasQueen,
    DesertSeltasQueen,

    Nerscylla,
    ShroudedNerscylla,
    DaimyoHermitaur,
    PlumDHermitaur,
    GoreMagala,
    ShagaruMagala,
    Seregios,
    Rathian,
    PinkRathian,

    GoldRathian,
    Rathalos,
    AzureRathalos,
    SilverRathalos,
    Zinogre,
    StygianZinogre,
    Diablos,
    BlackDiablos,
    Monoblos,

    WhiteMonoblos,
    Brachydios,
    Deviljho,
    Tigrex,
    BruteTigrex,
    MoltenTigrex,
    YianKutKu,
    BlueYianKutKu,
    YianGaruga,

    Gypceros,
    PurpleGypceros,
    Basarios,
    RubyBasarios,
    Gravios,
    BlackGravios,
    Khezu,
    RedKhezu,
    Congalala,

    EmeraldCongalala,
    Rajang,
    Kirin,
    OroshiKirin,
    KushalaDaora,
    Teostra,
    Chameleos,
    Akantor,
    Ukanlos,

    DahrenMohran,
    Dalamadur,
    ShahDalamadur,
    Gogmazios,
    Fatalis
};

struct sBoxItem {
    u16 ID;
    u16 Ammount;
};

union sBoxEquip {
    uEquipment Equipment;
};

}; /// namespace mh4u
}; /// namespace mh
