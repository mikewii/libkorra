#pragma once
#include <libkorra/types.h>

namespace mh {
namespace mh4u {
namespace options {

enum class HUD : u8 {
    On = 0,
    Off
};

enum class Map : u8 {
    On = 0,
    Off
};

enum class SoundSettings : u8 {
    Mono = 0,
    Stereo,
    Surround
};

enum class MusicVolume : u8 {
    VolumeOff = 0,
    Volume1,
    Volume2,
    Volume3,
    Volume4,
    Volume5,
    Volume6,
    VolumeMax
};

enum class SFXVolume : u8 {
    VolumeOff = 0,
    Volume1,
    Volume2,
    Volume3,
    Volume4,
    Volume5,
    Volume6,
    VolumeMax
};

enum class EffectSettings : u8 {
    Normal = 0,
    Reduced
};

enum class CameraControls : u8 {
    Normal = 0,
    FlipY,
    FlipX,
    FlipXY
};

enum class CameraAngle : u8 {
    Type1 = 0,
    Type2,
    Type3,
    Type4,
    Type5
};

enum class ScopeControls : u8 {
    Normal = 0,
    FlipY,
    FlipX,
    FlipXY
};

enum class QuickAimCamera : u8 {
    Type1 = 0,
    Type2,
    Type3
};

enum class QuickAimControls : u8 {
    Normal = 0,
    FlipY,
    FlipX,
    FlipXY
};

enum class StartBtn : u8 {
    Menu = 0,
    Kick
};

enum class TargetCamControls : u8 {
    Type1 = 0,
    Type2
};

enum class Orientation : u8 {
    Type1 = 0,
    Type2
};

enum class BowControls : u8 {
    Type1 = 0,
    Type2
};

enum class TerrainSavvyCamera : u8 {
    Manual = 0,
    SemiAuto
};

enum class GunnerReticle : u8 {
    Type1 = 0,
    Type2
};

enum class ReticleSpeed : u8 {
    Slow = 0,
    Default,
    Fast
};

enum class TargetCamBehavior : u8 {
    Type1 = 0,
    Type2
};

enum class CirclePadPro : u8 {
    Off = 0,
    On
};

enum class CirclePadProButtons : u8 {
    Type1 = 0,
    Type2,
    Type3,
    Type4
};

enum class BowAimModeCancel : u8 {
    Auto = 0,
    Manual
};

enum class OnlinePalicoMessages : u8 {
    Off = 0,
    On
};

enum class DialogueMessages : u8 {
    Normal = 0,
    Instant
};

struct Options {
    SoundSettings           soundSettings;
    MusicVolume             musicVolume;
    SFXVolume               sfxVolume;

    u8  missing3;

    EffectSettings          effectSettings;

    u8  missing5;
    u8  missing6;

    CameraControls          cameraControls;
    CameraAngle             cameraAngle;
    ScopeControls           scopeControls;
    QuickAimCamera          quickAimCamera;
    QuickAimControls        quickAimControls;
    HUD                     hud;
    Map                     map;

    u8  missing14;
    u8  missing15;
    u8  missing16;

    StartBtn                startButton;
    TargetCamControls       targetCamControls;
    Orientation             orientation;
    BowControls             bowControls;
    TerrainSavvyCamera      terrainSavvyCamera;
    GunnerReticle           gunnersReticle;
    ReticleSpeed            reticleSpeed;
    TargetCamBehavior       targetCamBehavior;
    CirclePadPro            circlePadPro;
    CirclePadProButtons     circlePadProButtons;
    BowAimModeCancel        bowAimModeCancel;
    OnlinePalicoMessages    onlinePalicoMessages;
    DialogueMessages        dialogueMessages;

    u8  missing30;
    u8  missing31;
};

}; /// namespace options
}; /// namespace mh4u
}; /// namespace mh
