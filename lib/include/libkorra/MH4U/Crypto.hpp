#pragma once
#include <libkorra/types.h>
#include <libkorra/tools/ccontainer_fwd.hpp>
#include <libkorra/tools/blowfish_fwd.hpp>

class BlowFish;

namespace mh {
namespace mh4u {

class Crypto
{
public:
    enum Key {
        EXT_DATA = 0,
        DLC_EUR_NA,
        DLC_JPN,
        DLC_KOR,
        DLC_TW,

        LENGTH
    };

    Crypto();
    ~Crypto();


    bool decodeSave(const CContainer& in, CContainer& out);
    bool encodeSave(const CContainer& in, CContainer& out);

    bool decodeExtData(const CContainer& in, CContainer& out) { return decodeSave(in, out); }
    bool encodeExtData(const CContainer& in, CContainer& out) { return encodeSave(in, out); }

    bool decodeQuest(const CContainer& in, CContainer& out) { return decodeDlc(in, out); }
    bool encodeQuest(const CContainer& in, CContainer& out, const Key key = Key::DLC_EUR_NA) { return encodeDlc(in, out, key); }

    bool decodeDlc(const CContainer& in, CContainer& out);
    bool encodeDlc(const CContainer& in, CContainer& out, const Key key = Key::DLC_EUR_NA);

    bool blowfishDecode(const CContainer& in, CContainer& out, const Key key);
    bool blowfishEncode(const CContainer& in, CContainer& out, const Key key);

    bool xorDecode(CContainer& cc);
    void xorEncode(CContainer& cc);

private:
    BlowFish *m_blowfish;
    void xorMH4U(CContainer& data, u32 seed);

    const uint8_t* getKey(const Key key) const;
    void insertValue(CContainer& cc, u32 value);
};

}; /// namespace mh4u
}; /// namespace mh

