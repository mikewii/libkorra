#pragma once
#include <libkorra/tools/ccontainer.hpp>
#include <libkorra/MH4U/Crypto.hpp>
#include <libkorra/MH4U/Quest/Quest.hpp>

#include <boost/filesystem.hpp>
#include <array>

namespace mh {
namespace mh4u {
namespace extdata {

#define QUEST_SIZE 0x2010
#define XOR_MH4U_SIZE 8
#define EXT_QUEST_FILE_SIZE 0x50400
#define EXT_QUEST_FILES_AMMOUNT 5
#define MH4_EXT_QUEST_FILES_AMMOUNT 4
#define EXT_QUEST_DATA_AMMOUNT 40
#define MH4_EXT_QUEST_DATA_AMMOUNT 20
#define EXT_QUEST_DATA_PADDING 0xf0
#define EXT_QUEST_DATA_SIZE (QUEST_SIZE * EXT_QUEST_DATA_AMMOUNT) + XOR_MH4U_SIZE + 0x80
#define MH4_EXT_QUEST_DATA_SIZE (QUEST_SIZE * MH4_EXT_QUEST_DATA_AMMOUNT) + XOR_MH4U_SIZE + 0x80

// input is container with encoded quest data
class DLCExtractor : virtual protected Crypto
{
public:
    DLCExtractor();
    DLCExtractor(CContainer* ext_quest_data);
    ~DLCExtractor();

    void set_ext_quest_data(CContainer* data) { m_ext_quest_data = data; }

    void get_decoded(CContainer* output, const bool mh4 = false);
    void get_all_mib(std::vector<CContainer>& output, const bool mh4 = false);

private:
    CContainer* m_ext_quest_data;
};

#ifndef N3DS
class DLCExtractor_Helper : protected DLCExtractor
{
public:
    DLCExtractor_Helper();
    DLCExtractor_Helper(const fs::path& path);

    bool extract(const bool dump_decoded = false,
                 const bool mh4 = false);

    void set_data_path(const fs::path& path);
    const fs::path& get_data_path(void) const;

    void set_out_folder(const fs::path& path);
    const fs::path& get_out_folder(void) const;

private:
    fs::path   m_data_path;
    fs::path   m_out_folder;
    std::vector<CContainer> m_dlc_vector;

    bool is_quest_ext_file(const fs::path &path) const;

    void write(void) const;
};
#endif

////////////////////////////////////////////////////////////////////////////////

class DLCRepacker : virtual protected Crypto
{
public:
    DLCRepacker();
    DLCRepacker(std::vector<CContainer>* dlc_vector);
    ~DLCRepacker();

    void perform(std::array<CContainer, EXT_QUEST_FILES_AMMOUNT>* output
                 , const bool mh4 = false);

    void set_dlc_vector(std::vector<CContainer>* vector);

private:
    std::vector<CContainer>* m_dlc_vector;

    void quest_file_populate(CContainer& in,
                             const size_t vector_begin_index,
                             const size_t vector_end_index,
                             const bool mh4 = false);
    void quest_file_encode(CContainer& in,
                           CContainer& out,
                           const bool mh4 = false);

    const std::array<u8, EXT_QUEST_FILES_AMMOUNT> split_by_id(const bool mh4 = false) const;
};

class DLCRepacker_Helper : protected DLCRepacker
{
public:
    // [quest_id, selected text]
    using dlc_info = std::pair<const u16, const std::u16string>;

    DLCRepacker_Helper();
    DLCRepacker_Helper(const fs::path& path);

    bool prepare(const bool mh4 = false);
    void repack(const bool mh4 = false);
    void write(const bool mh4 = false) const;

    void set_data_path(const fs::path& path);
    const fs::path& get_data_path(void) const;

    void set_out_folder(const fs::path& path);
    const fs::path& get_out_folder(void) const;

    const std::vector<size_t>& get_duplicates_vector(void) const;
    const dlc_info get_dlc_info(const size_t& id,
                                const QuestLanguage& language = QuestLanguage::ENGLISH,
                                const Text& text = Text::TITLE) const;

private:
    fs::path m_data_path;
    fs::path m_out_folder;
    std::vector<CContainer> m_dlc_vector;
    std::vector<size_t> m_dlc_duplicates;

    std::array<CContainer, EXT_QUEST_FILES_AMMOUNT> m_encoded_ext_quests;


    void read_files(const bool mh4 = false);
    void dlc_vector_sort(void);
    void find_duplicates(void);
};

}; /// namespace extdata
}; /// namespace mh4u
}; /// namespace mh
