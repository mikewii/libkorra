#pragma once
#include <libkorra/types.h>
#include <array>

namespace mh {
namespace mh4u {

struct sArms {
    using size = u16;
    static constexpr const u32 ITEMS_MAX = 956;

    enum e : size {

    };

    static const std::array<const char*, ITEMS_MAX> str;
    static const char* getStr(const e id);
};

}; /// namespace mh4u
}; /// namespace mh
