#pragma once
#include <libkorra/types.h>
#include <libkorra/MH4U/Equipment/Hunter/Skills.hpp>

namespace mh {
namespace mh4u {

struct sArmorLevel {
    using size = u8;

    enum e:size {
        Lv1 = 0,
        Lv2,
        Lv3,
        Lv4,
        Lv5,
        Lv6,
        Lv7,
        Lv8,
        Lv9,
        Lv10,
        Lv11,
        Lv12,
        Lv13,
        Lv14,
        Lv16,
        Lv17,
        Lv18
    };
};

struct sArmorDefence {
    using size = u8;

    enum e:size {
        Def10 = 0,
        Def12,
        Def16,
        Def18,
        Def20_0,
        Def22,
        Def33,
        Def36,
        Def39,
        Def20_1,
        Def24,
        Def30,
        Def34,
        Def40,
        Def45,
        Def65,
        Def72_0,
        Def78,
        Def46,
        Def50,
        Def54,
        Def64,
        Def72_1,
        Def86,
        Def92,
        Def98,
        Def112,
        Def120
    };
};

struct sTalismanType {
    using size = u8;

    enum e:size {
        PAWN = 0,
        BISHOP,
        KNIGHT,
        ROOK,
        QUEEN,
        KING,
        DRAGON,
        UNKNOWABLE,
        MYSTIC,
        HERO,
        LEGEND,
        CREATOR,
        SAGE,
        MIRACLE,
        AMBER,
        JADE,
        EMERY
    };
};

struct sBonusAtk {
    using size = u8;

    enum e:size {
        ATK00 = 0,
        ATK10,
        ATK20,
        ATK__
    };
};

struct sBonusAff {
    using size = u8;

    enum e:size {
        AFF00 = 0,
        AFF10,
        AFF20,
        AFF__
    };
};

enum sBonusDef : u8 {
    DEF00 = 0,
    DEF10,
    DEF20_1,
    DEF20_2,
    DEF35
};

struct sRelicLevel {
    using size = u8;

    enum e:size {
        Lv1 = 0,
        Lv2,
        Lv3,
        Lv4
    };
};

struct sRarity {
    using size = u8;

    enum e:size {
        Rare1 = 0,
        Rare2,
        Rare3,
        Rare4,
        Rare5,
        Rare6,
        Rare7,
        Rare8,
        Rare9,
        Rare10
    };
};

struct sSAPhialType {
    using size = u8;

    enum e:size {
        POWER = 0,
        ELEMENT,
        PARALYSIS,
        DRAGON,
        EXHAUST,
        POISON
    };
};

struct sCBPhialType {
    using size = u8;

    enum e:size {
        IMPACT = 0,
        ELEMENT
    };
};

struct sKinsectLevel {
    using size = u8;

    enum e:size {
        Lv1 = 0,
        Lv2,
        Lv3,
        Lv4,
        Lv5,
        Lv6,
        Lv7,
        Lv8,
        Lv9,
        Lv10,
        Lv11,
        Lv12
    };
};

struct sExcavatedType {
    using size = u8;

    enum e:size {
        BATTERED = 0,
        SEASONED,
        VENERABLE,
        CHAMPION,
        BESHACKLED
    };
};

struct sElementType {
    using size = u8;

    enum e:size {
        NONE = 0,
        FIRE,
        WATER,
        THUNDER,
        DRAGON,
        ICE,
        POISON,
        PARALYSIS,
        SLEEP,
        BLAST
    };
};

struct sEquipmentType {
    using size = u8;

    enum e:size {
        NONE = 0,

        CHEST,
        ARMS,
        WAIST,
        LEGS,
        HEAD,

        TALISMAN,

        GS,
        SNS,
        HAMMER,
        LANCE,
        LBG,
        HBG,
        LS,
        SA,
        GL,
        BOW,
        DB,
        HH,
        IG,
        CB
    };
};

struct sPolishedType {
    using size = u8;

    enum e:size {
        POLISHED = 0,
        RUSTED
    };
};

struct sSlot {
    u16 Decoration : 15;
    u16 IsFixed : 1;
};

struct sRelicInfo {
    sPolishedType::e    PolishedType : 1;
    u8                  Glow : 1;
    u8                  Slots : 2;
    u8                  Padding : 4;
};

struct sHoneType {
    using size = u8;

    enum e:size {
        NONE = 0,
        ATTACK,
        DEFENCE,
        HEALTH
    };
};

struct sHone {
    u8              Padding : 6;
    sHoneType::e    Type : 2;
};

union uEquipment {
    u8 Raw[28];

    struct sRelic {
        sEquipmentType::e   Type;
        u8                  Padding0;
        u16                 ID;
        u8                  Padding1[2];
        sSlot               Slot[3];
        u8                  Padding2;
        u8                  AtkAffDef;
        sRelicLevel::e      RelicLevel;
        u8                  Padding3;
        sRelicInfo          Info;
        sRarity::e          Rarity;
        sExcavatedType::e   ExcavatedType;
        sHone               Honing;
        u8                  Padding4[8];
    };

    struct sBlademaster {
        sEquipmentType::e   Type;
        u8                  Padding0;
        u16                 ID;
        u8                  ElementAmount;
        sElementType::e     ElementType;
        sSlot               Slot[3];
        u8                  Sharpness;
        u8                  AtkAffDef;
        sRelicLevel::e      RelicLevel;
        u8                  Bonus;
        sRelicInfo          Info;
        sRarity::e          Rarity;
        sExcavatedType::e   ExcavatedType;
        sHone               Honing;
        u8                  Padding2[8];
    };

    struct sArmor {
        sEquipmentType::e   Type;
        sArmorLevel::e      ArmorLevel;
        u16                 ID;
        u16                 Color;
        sSlot               Slot[3];
        u8                  Resistance;
        sArmorDefence::e    Defence;
        u8                  Padding0[2];
        sRelicInfo          Info;
        sRarity::e          Rarity;
        sExcavatedType::e   ExcavatedType;
        u8                  ColorMode;
        u32                 Padding1[2];
    };

    struct sTalisman {
        sEquipmentType::e   Type;
        u8                  Slots;
        sTalismanType::e    TalismanType;
        u8                  Padding0[3];
        sSlot               Slot[3];
        sSkills::e          Skill1;
        sSkillPoints::e     Skill1Points;
        sSkills::e          Skill2;
        sSkillPoints::e     Skill2Points;
        u8                  Padding1[8];
    };

    struct sGS {
        sEquipmentType::e   Type;
        u8                  Level;
        u16                 ID;
        u8                  ElementAmount;
        sElementType::e     ElementType;
        sSlot               Slot[3];
        u8                  Sharpness;
        u8                  AtkAffDef;
        sRelicLevel::e      RelicLevel;
        sBonusAtk::e        BonusAtk;
        sRelicInfo          Info;
        sRarity::e          Rarity;
        sExcavatedType::e   ExcavatedType;
        sHone               Honing;
        u8                  Padding[8];
    };

    struct sSnS {
        sEquipmentType::e   Type;
        u8                  Level;
        u16                 ID;
        u8                  ElementAmount;
        sElementType::e     ElementType;
        sSlot               Slot[3];
        u8                  Sharpness;
        u8                  AtkAffDef;
        sRelicLevel::e      RelicLevel;
        sBonusDef           BonusDef;
        sRelicInfo          Info;
        sRarity::e          Rarity;
        sExcavatedType::e   ExcavatedType;
        sHone               Honing;
        u32                 Padding[2];
    };

    struct sHammer {
        sEquipmentType::e   Type;
        u8                  Level;
        u16                 ID;
        u8                  ElementAmount;
        sElementType::e     ElementType;
        sSlot               Slot[3];
        u8                  Sharpness;
        u8                  AtkAffDef;
        sRelicLevel::e      RelicLevel;
        sBonusAff::e        BonusAff;
        sRelicInfo          Info;
        sRarity::e          Rarity;
        sExcavatedType::e   ExcavatedType;
        sHone               Honing;
        u32                 Padding[2];
    };

    struct sLance {
        sEquipmentType::e   Type;
        u8                  Level;
        u16                 ID;
        u8                  ElementAmount;
        sElementType::e     ElementType;
        sSlot               Slot[3];
        u8                  Sharpness;
        u8                  AtkAffDef;
        sRelicLevel::e      RelicLevel;
        sBonusDef           BonusDef;
        sRelicInfo          Info;
        sRarity::e          Rarity;
        sExcavatedType::e   ExcavatedType;
        sHone               Honing;
        u32                 Padding[2];
    };

    struct sLBG {
        sEquipmentType::e   Type;
        u8                  UpgradeType;
        u16                 ID;
        u8                  Unk1;
        u8                  Unk2;
        sSlot               Slot[3];
        u8                  ClipSizeType;
        u8                  AtkAffDef;
        sRelicLevel::e      RelicLevel;
        u8                  CrouchFireType;
        sRelicInfo          Info;
        sRarity::e          Rarity;
        sExcavatedType::e   ExcavatedType;
        sHone               Honing;
        u32                 Padding[2];
    };

    struct sHBG {
        sEquipmentType::e   Type;
        u8                  UpgradeType;
        u16                 ID;
        u8                  Unk1;
        u8                  Unk2;
        sSlot               Slot[3];
        u8                  ClipSizeType;
        u8                  AtkAffDef;
        sRelicLevel::e      RelicLevel;
        u8                  RapidFireType;
        sRelicInfo          Info;
        sRarity::e          Rarity;
        sExcavatedType::e   ExcavatedType;
        sHone               Honing;
        u32                 Padding[2];
    };

    struct sLS {
        sEquipmentType::e   Type;
        u8                  Level;
        u16                 ID;
        u8                  ElementAmount;
        sElementType::e     ElementType;
        sSlot               Slot[3];
        u8                  Sharpness;
        u8                  AtkAffDef;
        sRelicLevel::e      RelicLevel;
        sBonusAff::e        BonusAff;
        sRelicInfo          Info;
        sRarity::e          Rarity;
        sExcavatedType::e   ExcavatedType;
        sHone               Honing;
        u32                 Padding[2];
    };

    struct sSA {
        sEquipmentType::e   Type;
        u8                  LevelCrafted;
        u16                 ID;
        u8                  ElementAmount;
        sElementType::e     ElementType;
        sSlot               Slot[3];
        u8                  Sharpness;
        u8                  AtkAffDef;
        sRelicLevel::e      LevelRelic;
        sSAPhialType::e     PhialType;
        sRelicInfo          Info;
        sRarity::e          Rarity;
        sExcavatedType::e   ExcavatedType;
        sHone               Honing;
        u8                  Padding[8];
    };

    struct sGL {
        sEquipmentType::e   Type;
        u8                  Padding0;
        u16                 ID;
        u8                  ElementAmount;
        sElementType::e     ElementType;
        sSlot               Slot[3];
        u8                  Sharpness;
        u8                  AtkAffDef;
        sRelicLevel::e      RelicLevel;
        u8                  ShotTypeLevel;
        sRelicInfo          Info;
        sRarity::e          Rarity;
        sExcavatedType::e   ExcavatedType;
        sHone               Honing;
        u32                 Padding[2];
    };

    struct sBow {
        sEquipmentType::e   Type;
        u8                  Padding0;
        u16                 ID;
        u8                  ElementAmount;
        sElementType::e     ElementType;
        sSlot               Slot[3];
        u8                  ShotType;
        u8                  AtkAffDef;
        sRelicLevel::e      RelicLevel;
        u8                  Unk;
        sRelicInfo          Info;
        sRarity::e          Rarity;
        sExcavatedType::e   ExcavatedType;
        sHone               Honing;
        u32                 Padding[2];
    };

    struct sDB {
        sEquipmentType::e   Type;
        u8                  Padding0;
        u16                 ID;
        u8                  ElementAmount;
        sElementType::e     ElementType;
        sSlot               Slot[3];
        u8                  Sharpness;
        u8                  AtkAffDef;
        sRelicLevel::e      RelicLevel;
        sBonusAff::e        BonusAff;
        sRelicInfo          Info;
        sRarity::e          Rarity;
        sExcavatedType::e   ExcavatedType;
        sHone               Honing;
        u32                 Padding[2];
    };

    struct sHH {
        sEquipmentType::e   Type;
        u8                  Padding0;
        u16                 ID;
        u8                  ElementAmount;
        sElementType::e     ElementType;
        sSlot               Slot[3];
        u8                  Sharpness;
        u8                  AtkAffDef;
        sRelicLevel::e      RelicLevel;
        u8                  NotesColor;
        sRelicInfo          Info;
        sRarity::e          Rarity;
        sExcavatedType::e   ExcavatedType;
        sHone               Honing;
        u32                 Padding[2];
    };

    struct sIG {
        sEquipmentType::e   Type;
        sKinsectLevel::e    KinsectLevel;
        u16                 ID;
        union {
            u8              ElementAmount_Relics;
            u8              KinsectType_Crafted;
        };
        sElementType::e     ElementType;
        sSlot               Slot[3];
        u8                  Sharpness;
        u8                  AtkAffDef;
        sRelicLevel::e      RelicLevel;
        u8                  KinsectType_Relics;
        sRelicInfo          Info;
        sRarity::e          Rarity;
        sExcavatedType::e   ExcavatedType;
        sHone               Honing;
        u8                  KinsectPower;
        u8                  KinsectStamina;
        u8                  KinsectSpeed;
        u8                  KinsectFireAtk;
        u8                  KinsectWaterAtk;
        u8                  KinsectThunderAtk;
        u8                  KinsectIceAtk;
        u8                  KinsectDragonAtk;
    };

    struct sCB {
        sEquipmentType::e   Type;
        u8                  Padding0;
        u16                 ID;
        u8                  ElementAmount;
        sElementType::e     ElementType;
        sSlot               Slot[3];
        u8                  Sharpness;
        u8                  AtkAffDef;
        sRelicLevel::e      RelicLevel;
        sCBPhialType::e     PhialType;
        sRelicInfo          Info;
        sRarity::e          Rarity;
        sExcavatedType::e   ExcavatedType;
        sHone               Honing;
        u32                 Padding1[2];
    };
};

}; /// namespace mh4u
}; /// namespace mh
