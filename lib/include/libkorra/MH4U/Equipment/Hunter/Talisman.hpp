#pragma once

namespace mh {
namespace mh4u {

constexpr const char* Talisman_s[] = {
    "(None)",
    "Pawn Talisman",
    "Bishop Talisman",
    "Knight Talisman",
    "Rook Talisman",
    "Queen Talisman",
    "King Talisman",
    "Dragon Talisman",
    "Unknowable Talisman",
    "Mystic Talisman",
    "Hero Talisman",
    "Legend Talisman",
    "Creator Talisman",
    "Sage Talisman",
    "Miracle Talisman",
    "Amber Tali.",
    "Jade Tali.",
    "Emery Tali."
};

}; /// namespace mh4u
}; /// namespace mh
