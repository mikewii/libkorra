#pragma once

namespace mh {
namespace mh4u {

constexpr const char* IG_Kinsect_s[] = {
    "Culldrone",
    "Reavedrone",
    "Alucanid",
    "Monarch Alucanid",
    "Empresswing",
    "Rigiprayne",
    "Cancadaman",
    "Fiddlebrix",
    "Windchopper",
    "Grancathar",
    "Pseudocath",
    "Elscarad",
    "Mauldrone",
    "Pummeldrone",
    "Foebeetle",
    "Carnage Beetle",
    "Bonnetfille",
    "Ladytarge",
    "Ladypavise",
    "Arkmaiden",
    "Gullshad",
    "Bullshroud",
    "Whispervesp",
    "Arginesse",
    "Thunderball",
    "Bilbobrix",
    "Foliacath",
    "Exalted Alucanid",
    "Great Elscarad",
    "Ladytower",
    "Fleetflammer",
    "Gleambeetle",
    "Great Arginesse",
    "Clockmaster",
    "Barrett Hawk"
};

}; /// namespace mh4u
}; /// namespace mh
