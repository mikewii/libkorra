#pragma once
#include <libkorra/types.h>

namespace mh {
namespace mh4u {

struct sBoxPalicoEquip {
    u16 Type;
    u16 ID;
};

struct sPalico { // size 0xE8
    utf16   PalicoName[10+2];
    u32     padding1[2];
    u8      CloatColor[4]; // rgba
    u8      ClothingColor[4];
    u8      Coat;
    u8      Clothes;
    u8      Eyes;
    u8      Ears;
    u8      Tail;
    u8      Voice;
    u16     unk[3];

    sBoxPalicoEquip Weapon;
    sBoxPalicoEquip Head;
    sBoxPalicoEquip Body;

    u16     unk1[5];
    utf16   PalicoComment[14+1];
    u32     unk2[6];
    utf16   FormerMaster[10 + 2];
    u32     unk3[8];
    utf16   Namegiver[10 + 2];
    u32     unk4[6];
};

}; /// namespace mh4u
}; /// namespace mh
