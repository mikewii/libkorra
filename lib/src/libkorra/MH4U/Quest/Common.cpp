#include "libkorra/MH4U/Quest/Common.hpp"
#include "libkorra/function_name.h"

namespace mh {
namespace mh4u {

const char *sEnemy::GetStr(const sEnemy::e id) { return EnemyNames_s.at(id).at(CurrentLanguage); }
const char *sIcons::GetStr(const sIcons::e id) { return IconNames_s.at(id).at(CurrentLanguage); }

constexpr const std::array<const char*, 6> sQuestType::str = {
    "Slay",
    "Capture",
    "Hunting",
    "Gathering",
    "Hunt multiple",
    "Special",
};

const std::vector<const char *> sQuestType::GetStrVec(const e bit)
{
    std::vector<const char*> out;

    if (bit == sQuestType::NONE) {
        out.push_back("(None)");
        return out;
    }
    if (bit & sQuestType::SLAY)
        out.push_back(sQuestType::str.at(0));
    if (bit & sQuestType::CAPTURE)
        out.push_back(sQuestFlags0::str.at(1));
    if (bit & sQuestType::HUNTING)
        out.push_back(sQuestFlags0::str.at(2));
    if (bit & sQuestType::GATHERING)
        out.push_back(sQuestFlags0::str.at(3));
    if (bit & sQuestType::HUNT_MULTIPLE)
        out.push_back(sQuestFlags0::str.at(4));
    if (bit & sQuestType::SPECIAL)
        out.push_back(sQuestFlags0::str.at(5));

    return out;
}

constexpr const std::array<const char*, 7> sQuestFlags0::str = {
    "Hunt a ton",
    "Intruder",
    "Repel",
    "Tutorial delivery",
    "Urgent quest",
    "Key quest",
    "Unknown"
};

const std::vector<const char*> sQuestFlags0::GetStrVec(const sQuestFlags0::e bit)
{
    std::vector<const char*> out;

    if (bit == sQuestFlags0::NONE) {
        out.push_back("(None)");
        return out;
    }

    if (bit & sQuestFlags0::HUNT_A_TON)
        out.push_back(sQuestFlags0::str.at(0));
    if (bit & sQuestFlags0::INTRUDER)
        out.push_back(sQuestFlags0::str.at(1));
    if (bit & sQuestFlags0::REPEL)
        out.push_back(sQuestFlags0::str.at(2));
    if (bit & sQuestFlags0::TUTORIAL_DELIVERY)
        out.push_back(sQuestFlags0::str.at(3));
    if (bit & sQuestFlags0::URGENT_QUEST)
        out.push_back(sQuestFlags0::str.at(4));
    if (bit & sQuestFlags0::KEY_QUEST)
        out.push_back(sQuestFlags0::str.at(5));
    if (bit & sQuestFlags0::UNK)
        out.push_back(sQuestFlags0::str.at(6));

    return out;
}

constexpr const std::array<const std::array<sEnemyParts::PartName, 7>, 124> sEnemyParts::parts_table = {{
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // none
    {NONE, NONE, HEAD, NONE, BACK, NONE, TAIL},             // Rathian
    {NONE, NONE, HEAD, WINGS, BACK, NONE, TAIL},            // Rathalos
    {NONE, NONE, HEAD, WING, BACK, NONE, TAIL},             // Pink Rathian
    {NONE, NONE, HEAD, WING, NONE, NONE, TAIL},             // Azure Rathalos
    {NONE, HEAD, WING, BACK, NONE, NONE, TAIL},             // Gold Rathian
    {NONE, HEAD, NONE, BACK, NONE, NONE, TAIL},             // Silver Rathalos
    {NONE, NONE, HEAD, NONE, NONE, NONE, NONE},             // Yian Kut-Ku
    {NONE, NONE, HEAD, NONE, NONE, NONE, NONE},             // B.Yian Kut-Ku
    {NONE, NONE, CREST, NONE, NONE, NONE, NONE},            // Gypceros
    {NONE, NONE, CREST, NONE, NONE, NONE, NONE},            // P.Gypceros
    {NONE, NONE, HEAD, CLAW, NONE, NONE, TAIL},             // Tigerex
    {NONE, NONE, CLAW, HEAD, NONE, NONE, TAIL},             // B. Tigrex
    {NONE, NONE, HEAD, NONE, NONE, NONE, NONE},             // Gendrome
    {NONE, NONE, HEAD, NONE, NONE, NONE, NONE},             // Iodrome
    {NONE, NONE, HEAD, NONE, NONE, NONE, NONE},             // Great Jaggi
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Velocidrome
    {NONE, NONE, COMB, CLAW, NONE, NONE, NONE},             // Congalala
    {NONE, NONE, COMB, CLAW, NONE, NONE, NONE},             // E.Congalala
    {NONE, NONE, NONE, TAIL, NONE, NONE, NONE},             // Rajang
    {NONE, NONE, EARS, CLAW, TAIL, NONE, NONE},             // Kecha Wacha
    {NONE, NONE, JAW, HIND_LEG, BACK, NONE, NONE},          // Tetsucabra
    {NONE, NONE, HEAD, TOP_FIN, FRONT_LEG, TAIL, NONE},     // Zamtrios
    {NONE, NONE, HEAD, NONE, BACK, TAIL, NONE},             // Najarala
    {HEAD, NONE, BACK, FRONT_LEGS, NONE, NONE, NONE},       // Dalamadur
    {NONE, NONE, HORN, NONE, NONE, NONE, NONE},             // Seltas
    {NONE, NONE, HEAD, NONE, NONE, TAIL, NONE},             // Seltas Queen
    {NONE, NONE, CLAW, POISON_SPIKES, HIDE, NONE, NONE},    // Nerscylla
    {NONE, NONE, FEELERS, NONE, WINGARM, WING, TAIL},       // Gore Magala
    {HORNS, NONE, WING, NONE, NONE, NONE, NONE},            // Shagaru Magala
    {NONE, NONE, NONE, EARS, NONE, BACK, NONE},             // Yian Garuga
    {HEAD, WING, NONE, NONE, NONE, NONE, TAIL},             // Kushala Daora
    {HEAD, WINGTALON, NONE, NONE, NONE, NONE, TAIL},        // Teostra
    {HEAD, BACK, BELLY, NONE, NONE, NONE, TAIL},            // Akantor
    {HORN, NONE, NONE, NONE, NONE, NONE, NONE},             // Kirin
    {HORN, NONE, NONE, NONE, NONE, NONE, NONE},             // Oroshi Kirin
    {NONE, NONE, HEAD, BODY, NONE, NONE, NONE},             // Khezu
    {NONE, NONE, HEAD, NONE, NONE, NONE, NONE},             // Red Khezu
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Basarios
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Ruby Basarios
    {NONE, NONE, HEAD, BACK, CHEST, WING, TAIL},            // Gravios
    {NONE, NONE, HEAD, BACK, CHEST, NONE, TAIL},            // Black Gravios
    {NONE, NONE, HEAD, NONE, NONE, NONE, TAIL},             // Deviljho
    {NONE, HEAD, NONE, NONE, NONE, NONE, TAIL},             // Savage Deviljho (7 not used anywhere)
    {NONE, NONE, HEAD, FRONT_LEG, NONE, NONE, TAIL},        // Brachydios
    {NONE, HORNS, NONE, NONE, NONE, NONE, NONE},            // Furious Rajang
    {HORN, NONE, BLOWHOLE, NONE, NONE, NONE, NONE},         // Dah'ren Mohran
    {NONE, NONE, HEAD, NONE, NONE, NONE, NONE},             // Lagombi
    {NONE, NONE, HEAD, FRONT_LEG, BACK, NONE, TAIL},        // Zinogre
    {NONE, NONE, HEAD, FRONT_LEG, BACK, NONE, TAIL},        // Stygian Zinogre
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Gargwa
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Rhenoplos
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Aptonoth
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Popo
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Slagtoth (Green)
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Slagtoth (Red)
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Jaggi
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Jaggia
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Velociprey
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Genprey
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Ioprey
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Remobra
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Delex
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Conga
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Kelbi
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Felyne
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Melynx
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Altaroth
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Bnahabra (Blue)
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Bnahabra (Yellow)
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Bnahabra (Green)
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Bnahabra (Red)
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Zamite
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Konchu (Yellow)
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Konchu (Green)
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Konchu (Blue)
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Konchu (Red)
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Fatalis
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Crimson Fatalis
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // White Fatalis
    {NONE, HEAD, CLAW, NONE, NONE, NONE, TAIL},             // Molten Tigrex
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Rock (Grey/Green)
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Rusted Kushala Daora
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Dalamadur (Tail) ?
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Rock (Dark/Dirty)
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Rock (Black)
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Rock (Icy 1)
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Rock (Icy 2)
    {NONE, NONE, HORN, WING, NONE, NONE, TAIL},             // Seregios
    {NONE, WING, NONE, BACK, NONE, NONE, TAIL},             // Gogmazios
    {NONE, NONE, EARS, NONE, TAIL, NONE, NONE},             // Ash Kecha Wacha
    {NONE, NONE, JAW, NONE, NONE, NONE, NONE},              // Berserk Tetsucabra
    {NONE, NONE, NONE, DORSAL_FIN, NONE, NONE, NONE},       // Tigerstripe Zamtrios
    {NONE, NONE, HEAD, NONE, NONE, NONE, NONE},             // Tidal Najarala
    {NONE, NONE, HORN, NONE, NONE, NONE, NONE},             // Desert Seltas
    {NONE, NONE, NONE, NONE, NONE, TAIL, NONE},             // Desert Seltas Queen ?
    {NONE, NONE, NONE, NONE, OUTER_HIDE, NONE, NONE},       // Shrouded Nerscylla ?
    {NONE, NONE, HEAD, WING, R_WINGARM, L_WINGARM, NONE},   // Chaotic Gore Magala ?
    {NONE, HEAD, FRONT_LEGS, NONE, NONE, NONE, NONE},       // Raging Brachydios ?
    {NONE, NONE, HORNS, BACK, NONE, NONE, TAIL},            // Diablos
    {NONE, NONE, HORNS, BACK, NONE, NONE, NONE},            // Black Diablos ?
    {NONE, NONE, HORN, NONE, NONE, NONE, NONE},             // Monoblos ?
    {NONE, NONE, HORN, NONE, NONE, NONE, NONE},             // White Monoblos ?
    {HORN, WING, NONE, NONE, NONE, NONE, TAIL},             // Chameleos
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Rock (Brown)
    {NONE, NONE, TOP_FIN, NONE, TAIL, NONE, NONE},          // Cephadrome
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Cephalos
    {NONE, NONE, SHELL, CLAW, NONE, NONE, NONE},            // Daimyo Hermitaur
    {NONE, NONE, NONE, CLAW, NONE, NONE, NONE},             // Plum D.Hermitaur
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Hermitaur
    {NONE, NONE, BACK, NONE, NONE, NONE, NONE},             // Shah Dalamadur (Head) ?
    {NONE, NONE, NONE, NONE, NONE, NONE, TAIL},             // Shah Dalamadur (Tail) ?
    {NONE, NONE, HORNS, TAIL, NONE, NONE, NONE},            // Apex Rajang
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Apex Deviljho ?
    {NONE, NONE, NONE, FRONT_LEG, NONE, NONE, NONE},        // Apex Zinogre ?
    {NONE, NONE, HEAD, NONE, CHEST, NONE, NONE},            // Apex Gravios ?
    {CHIN, NONE, NONE, FRONT_LEG, NONE, NONE, NONE},        // Ukanlos ?
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Flame Fatalis
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE},             // Apceros
    {NONE, NONE, NONE, NONE, NONE, NONE, TAIL},             // Apex Diablos ?
    {NONE, NONE, NONE, NONE, NONE, TAIL, NONE},             // Apex Tidal Najarala ?
    {NONE, NONE, HEAD, NONE, NONE, NONE, TAIL},             // Apex Tigrex ?
    {NONE, NONE, HORN, NONE, LEG_CLAW, NONE, NONE},         // Apex Seregios ?
    {NONE, NONE, NONE, NONE, NONE, NONE, NONE}              // (Reinforcement)
}};
const char* sEnemyParts::GetStr(const sEnemy::e enemy_id, const sEnemyParts::e part_id)
{
    if (part_id == 7)
        return sEnemyParts::GetStr(sEnemyParts::GetAllParts(enemy_id).at(6));
    else
        return sEnemyParts::GetStr(sEnemyParts::GetAllParts(enemy_id).at(part_id));
}
const char *sEnemyParts::GetStr(const PartName id) { return EnemyPartNames_s.at(id).at(CurrentLanguage); }
const std::array<sEnemyParts::PartName, 7> sEnemyParts::GetAllParts(const sEnemy::e id)
{
    if (id == 7)
        return parts_table.at(6);
    else
        return parts_table.at(id);
}

const std::array<const char*, 7> sEnemyParts::GetAllPartsStr(const sEnemy::e id)
{
    std::array<const char*, 7> out;
    size_t i = 0;

    for (const auto& part : sEnemyParts::GetAllParts(id)) {
        if (i == 5)
            i = 6;

        out.at(i) = sEnemyParts::GetStr(part);
        ++i;
    }

    return out;
}


constexpr const std::array<const char*, 10> sQuestStars::str = {
    "1*",
    "2*",
    "3*",
    "4*",
    "5*",
    "6*",
    "7*",
    "8* (G1)",
    "9* (G2)",
    "10* (G3)"
};

const char *sQuestStars::getStr(const sQuestStars::e id)
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id];

    return err.c_str();
}

constexpr const std::array<const char*, 22> sMap::str = {
    "Test Area",
    "Ancestal Steppe",
    "Sunken Hollow",
    "Primal Forest",
    "Frozen Seaway",
    "Heaven's Mount",
    "Great Desert",
    "Tower Summit",
    "Speartip Crag",
    "Ingle Isle",
    "Castle Schrade",
    "Arena",
    "Slayground",
    "Everwood",
    "Great Sea",
    "Volcanic Hollow",
    "Sanctuary",
    "Dunes (Day)",
    "Dunes (Night)",
    "Battlequarters",
    "Polar Field",
    "Great Sea(Storm)"
};

const char *sMap::getStr(const sMap::e id)
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id];

    return err.c_str();
}

constexpr const std::array<const char*, 37> sRequirements::str = {
    "None",
    "HR 2 or higher",
    "HR 3 or higher",
    "HR 4 or higher",
    "HR 5 or higher",
    "HR 6 or higher",
    "HR 7 or higher",
    "HR 8 or higher",
    "HR 10 or higher",
    "HR 20 or higher",
    "HR 50 or higher",
    "Only Greatswords",
    "Only Lances",
    "Only Hammers",
    "Only SnS",
    "Only LBG or HBG",
    "Only Dual Blades",
    "Only Longsword",
    "Only Gunlance",
    "Only Hunting Horns",
    "Only Bows",
    "Only SwitchAxes",
    "Only Insect Glaives",
    "Only Charge Blades",
    "Only Rare 1 Weapons",
    "No Armor/Charm",
    "No Items",
    "Only 1 Player",
    "Only 2 Players",
    "Only 3 Players",
    "Palico needed",
    "No Palico",
    "G1-Permit",
    "G2-Permit",
    "G3-Permit",
    "G-Special-Permit",
    "G1/Rare 1/only Ammo"
};

const char *sRequirements::getStr(const sRequirements::e id)
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id];

    return err.c_str();
}

constexpr const std::array<const char*, 3> sPlayerSpawnType::str = {
    "Base Camp",
    "Random",
    "Elder Dragon Fight"
};

const char* sPlayerSpawnType::getStr(const sPlayerSpawnType::e id)
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id];

    return err.c_str();
}

constexpr const std::array<const char*, 5> sGatheringLv::str = {
    "Training",
    "Low Rank",
    "High Rank",
    "G Rank",
    "Arena"
};

const char* sGatheringLv::getStr(const sGatheringLv::e id)
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id];

    return err.c_str();
}

constexpr const std::array<const char*, 4> sCarvingLv::str = {
    "Arena",
    "Low Rank",
    "High Rank",
    "G Rank"
};

const char* sCarvingLv::getStr(const sCarvingLv::e id)
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id];

    return err.c_str();
}

constexpr const std::array<const char*, 7> sMonsterAILv::str = {
    "Dumb",
    "Low Rank",
    "Low Rank+",
    "High Rank",
    "High Rank+",
    "G Rank",
    "GQ lvl 140"
};

const char* sMonsterAILv::getStr(const sMonsterAILv::e id)
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id];

    return err.c_str();
}


constexpr const std::array<const char*, 2> sArenaFenceEnabled::str = {
    "Off",
    "On"
};

const char* sArenaFenceEnabled::getStr(const sArenaFenceEnabled::e id)
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id];

    return err.c_str();
}


constexpr const std::array<const char*, 2> sArenaFenceStatus::str = {
    "Down",
    "Up"
};

const char* sArenaFenceStatus::getStr(const sArenaFenceStatus::e id)
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id];

    return err.c_str();
}

}; /// namespace mh4u
}; /// namespace mh
