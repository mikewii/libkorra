#include "libkorra/MH4U/Quest/Editor.hpp"

namespace mh {
namespace mh4u {

QuestEditor::QuestEditor()
{
}

QuestEditor::QuestEditor(const fs::path &path)
{

}

void QuestEditor::initialize(void)
{
    this->m_p_header = reinterpret_cast<sQuest*>(this->m_data.data());
}

void QuestEditor::read_header(void)
{
    this->m_header = *this->m_p_header;

}

}; /// namespace mh4u
}; /// namespace mh
