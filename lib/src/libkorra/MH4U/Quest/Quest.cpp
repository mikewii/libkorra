#include "libkorra/MH4U/Quest/Quest.hpp"
#include "libkorra/MH4U/Crypto.hpp"
#include "libkorra/tools/file.hpp"
#include <codecvt>

namespace mh {
namespace mh4u {

u32 sText::get_by_id(const size_t id) const
{
    switch (id) {
    default:
    case Text::TITLE: return this->p_Title;
    case Text::MAIN_GOAL: return this->p_MainGoal;
    case Text::FAILURE: return this->p_Failure;
    case Text::SUMMARY: return this->p_Summary;
    case Text::MAIN_MONSTER: return this->p_MainMonster;
    case Text::CLIENT: return this->p_Client;
    case Text::SUB_QUEST: return this->p_SubQuest;
    }
}

u32 sTextLanguages::get_by_id(const size_t id) const
{
    switch (id) {
    default:
    case QuestLanguage::ENGLISH: return this->p_sText_English;
    case QuestLanguage::FRENCH: return this->p_sText_French;
    case QuestLanguage::SPANISH: return this->p_sText_Spanish;
    case QuestLanguage::GERMAN: return this->p_sText_German;
    case QuestLanguage::ITALIAN: return this->p_sText_Italian;
    }
}

bool sQuest::checkVersion(void) const
{
    static const char VERSION[5] = "v005";

    if (std::equal(VERSION, VERSION + 4, version))
        return true;
    else return false;
}

sFlags* sQuest::get_sFlags(void)
{
    return reinterpret_cast<sFlags*>(reinterpret_cast<char*>(this) + this->p_sFlags);
}

const sFlags* sQuest::get_sFlags(void) const
{
    return reinterpret_cast<const sFlags*>(reinterpret_cast<const char*>(this) + this->p_sFlags);
}

sItemBox *sQuest::get_item_box(const ItemBoxID id)
{
    auto offset = this->get_item_box_by_id(id);

    if (offset == 0)
        return nullptr;

    return reinterpret_cast<sItemBox*>(reinterpret_cast<char*>(this) + offset);
}

const sItemBox *sQuest::get_item_box(const ItemBoxID id) const
{
    auto offset = this->get_item_box_by_id(id);

    if (offset == 0)
        return nullptr;

    return reinterpret_cast<const sItemBox*>(reinterpret_cast<const char*>(this) + offset);
}

std::vector<sSupplyBoxItem> sQuest::get_supply_box_items(const ItemBoxID id)
{
    std::vector<sSupplyBoxItem> supply_box_items;
    bool proceed = false;

    switch (id) {
    default:break;
    case ItemBoxID::SUPPLY_BOX: proceed = true; break;
    case ItemBoxID::REFILL_SUPPLIES_1: proceed = true; break;
    case ItemBoxID::REFILL_SUPPLIES_2: proceed = true; break;
    case ItemBoxID::REFILL_SUPPLIES_3: proceed = true; break;
    }

    if (proceed) {
        auto offset = this->get_item_box(id)->pItemsBox;
        sSupplyBoxItem* item_array = reinterpret_cast<sSupplyBoxItem*>(reinterpret_cast<char*>(this) + offset);

        supply_box_items.reserve(SUPPLY_BOX_MAX_ITEMS);

        for (size_t i = 0; i < SUPPLY_BOX_MAX_ITEMS; i++)
            supply_box_items.push_back(item_array[i]);
    }

    return supply_box_items;
}

sTextLanguages *sQuest::get_sTextLanguages(void)
{
    auto offset = this->get_sFlags()->p_sTextLanguages;

    return reinterpret_cast<sTextLanguages*>(reinterpret_cast<char*>(this) + offset);
}

const sTextLanguages *sQuest::get_sTextLanguages(void) const
{
    auto offset = this->get_sFlags()->p_sTextLanguages;

    return reinterpret_cast<const sTextLanguages*>(reinterpret_cast<const char*>(this) + offset);
}

sText *sQuest::get_sText(const QuestLanguage language)
{
    auto offset = this->get_sTextLanguages()->get_by_id(language);

    return reinterpret_cast<sText*>(reinterpret_cast<char*>(this) + offset);
}

const sText *sQuest::get_sText(const QuestLanguage language) const
{
    auto offset = this->get_sTextLanguages()->get_by_id(language);

    return reinterpret_cast<const sText*>(reinterpret_cast<const char*>(this) + offset);
}

std::u16string sQuest::get_text(const QuestLanguage language, const Text choice)
{
    auto offset = this->get_sText(language)->get_by_id(choice);

    return std::u16string(reinterpret_cast<std::u16string::value_type*>(reinterpret_cast<char*>(this) + offset));
}

const std::u16string sQuest::get_text(const QuestLanguage language, const Text choice) const
{
    auto offset = this->get_sText(language)->get_by_id(choice);

    return std::u16string(reinterpret_cast<const std::u16string::value_type*>(reinterpret_cast<const char*>(this) + offset));
}

std::vector<std::vector<sEnemy_s>> sQuest::getLargeEnemyVector(void)
{
    constexpr const u32 terminator = 0xFFFFFFFF;
    std::vector<std::vector<sEnemy_s>> out;
    const auto groups = this->get_enemy_waves(EnemyWaveType::LargeEnemy);

    for (const auto& group : groups) {
        size_t i = 0;

        out.push_back({});
        auto& back = out.back();

        while (*reinterpret_cast<u32*>(reinterpret_cast<char*>(group) + (sizeof(sEnemy_s) * i)) != terminator) {
            const auto* enemy = reinterpret_cast<sEnemy_s*>(reinterpret_cast<char*>(group) + (sizeof(sEnemy_s) * i));
            back.push_back(*enemy);
            ++i;
        }

        if (back.empty())
            out.pop_back();
    }

    return out;
}

const std::vector<std::vector<sEnemy_s>> sQuest::getLargeEnemyVector(void) const
{
    constexpr const u32 terminator = 0xFFFFFFFF;
    std::vector<std::vector<sEnemy_s>> out;
    const auto groups = this->get_enemy_waves(EnemyWaveType::LargeEnemy);

    for (const auto& group : groups) {
        size_t i = 0;

        out.push_back({});
        auto& back = out.back();

        while (*reinterpret_cast<const u32*>(reinterpret_cast<const char*>(group) + (sizeof(sEnemy_s) * i)) != terminator) {
            const auto* enemy = reinterpret_cast<const sEnemy_s*>(reinterpret_cast<const char*>(group) + (sizeof(sEnemy_s) * i));
            back.push_back(*enemy);
            ++i;
        }

        if (back.empty())
            out.pop_back();
    }

    return out;
}

std::vector<std::vector<std::vector<sEnemy_s>>> sQuest::getSmallEnemyVector(void)
{
    constexpr const u32 terminator = 0xFFFFFFFF;
    std::vector<std::vector<std::vector<sEnemy_s>>> out;
    const auto waves = this->get_small_enemy_groups();

    for (const auto& wave : waves) {
        out.push_back({});
        auto& back0 = out.back();

        for (const auto& group : wave) {
            size_t i = 0;

            back0.push_back({});
            auto& back1 = back0.back();

            while (*reinterpret_cast<u32*>(reinterpret_cast<char*>(group) + (sizeof(sEnemy_s) * i)) != terminator) {
                auto* enemy = reinterpret_cast<sEnemy_s*>(reinterpret_cast<char*>(group) + (sizeof(sEnemy_s) * i));
                back1.push_back(*enemy);
                ++i;
            }

            if (back1.empty())
                back0.pop_back();
        }

        if (back0.empty())
            out.pop_back();
    }

    return out;
}

const std::vector<std::vector<std::vector<sEnemy_s>>> sQuest::getSmallEnemyVector(void) const
{
    constexpr const u32 terminator = 0xFFFFFFFF;
    std::vector<std::vector<std::vector<sEnemy_s>>> out;
    const auto waves = this->get_small_enemy_groups();

    for (const auto& wave : waves) {
        out.push_back({});
        auto& back0 = out.back();

        for (const auto& group : wave) {
            size_t i = 0;

            back0.push_back({});
            auto& back1 = back0.back();

            while (*reinterpret_cast<const u32*>(reinterpret_cast<const char*>(group) + (sizeof(sEnemy_s) * i)) != terminator) {
                const auto* enemy = reinterpret_cast<const sEnemy_s*>(reinterpret_cast<const char*>(group) + (sizeof(sEnemy_s) * i));
                back1.push_back(*enemy);
                ++i;
            }

            if (back1.empty())
                back0.pop_back();
        }

        if (back0.empty())
            out.pop_back();
    }

    return out;
}

std::vector<sEnemyWave *> sQuest::get_enemy_waves(const EnemyWaveType type)
{
    std::vector<sEnemyWave *> out;
    size_t i = 0;
    u32 offset = 0;


    if (type == EnemyWaveType::LargeEnemy)
        offset = this->p_large_monster_waves;
    else if (type == EnemyWaveType::SmallEnemy)
        offset = this->p_small_monster_waves;

    if (offset == 0)
        return out;

    auto* waves = reinterpret_cast<u32*>(reinterpret_cast<char*>(this) + offset);

    while (waves[i]) {
        auto offset = waves[i];
        out.push_back(reinterpret_cast<sEnemyWave*>(reinterpret_cast<char*>(this) + offset));
        ++i;
    }

    return out;
}

const std::vector<const sEnemyWave *> sQuest::get_enemy_waves(const EnemyWaveType type) const
{
    std::vector<const sEnemyWave *> out;
    size_t i = 0;
    u32 offset = 0;


    if (type == EnemyWaveType::LargeEnemy)
        offset = this->p_large_monster_waves;
    else if (type == EnemyWaveType::SmallEnemy)
        offset = this->p_small_monster_waves;

    if (offset == 0)
        return out;

    const auto* waves = reinterpret_cast<const u32*>(reinterpret_cast<const char*>(this) + offset);

    while (waves[i]) {
        auto offset = waves[i];
        out.push_back(reinterpret_cast<const sEnemyWave*>(reinterpret_cast<const char*>(this) + offset));
        ++i;
    }

    return out;
}

std::vector<std::vector<sEnemyWave *> > sQuest::get_small_enemy_groups(void)
{
    std::vector<std::vector<sEnemyWave *>> out;
    const auto waves = this->get_enemy_waves(EnemyWaveType::SmallEnemy);

    for (const auto& wave : waves) {
        size_t i = 0;
        auto* groups  = reinterpret_cast<u32*>(wave);

        out.push_back({});
        auto& back = out.back();

        while (groups[i]) {
            auto offset = groups[i];
            back.push_back(reinterpret_cast<sEnemyWave*>(reinterpret_cast<char*>(this) + offset));
            ++i;
        }

        if (back.empty())
            out.pop_back();
    }

    return out;
}

const std::vector<std::vector<const sEnemyWave *>> sQuest::get_small_enemy_groups(void) const
{
    std::vector<std::vector<const sEnemyWave *>> out;
    const auto waves = this->get_enemy_waves(EnemyWaveType::SmallEnemy);

    for (const auto& wave : waves) {
        size_t i = 0;
        const auto* groups  = reinterpret_cast<const u32*>(wave);

        out.push_back({});
        auto& back = out.back();

        while (groups[i]) {
            auto offset = groups[i];
            back.push_back(reinterpret_cast<const sEnemyWave*>(reinterpret_cast<const char*>(this) + offset));
            ++i;
        }

        if (back.empty())
            out.pop_back();
    }

    return out;
}

u32 sQuest::get_item_box_by_id(const ItemBoxID id) const
{
    switch (id) {
    default:
    case ItemBoxID::SUPPLY_BOX: return this->p_supply_box;
    case ItemBoxID::REFILL_SUPPLIES_1: return this->p_refill_box_1;
    case ItemBoxID::REFILL_SUPPLIES_2: return this->p_refill_box_2;
    case ItemBoxID::REFILL_SUPPLIES_3: return this->p_refill_box_3;
    case ItemBoxID::MAIN_REWARD_BOX_A: return this->p_main_reward_box_a;
    case ItemBoxID::MAIN_REWARD_BOX_B: return this->p_main_reward_box_b;
    case ItemBoxID::SUB_REWARD_BOX: return this->p_sub_reward_box;
    }
}

sQuest::QuestType sQuest::isQuestFile(const fs::path &path)
{
#define PROBE_SIZE sizeof(u32) * 4
    CContainer  input(PROBE_SIZE); // 4 values by 4 bytes: [seed, checksum, data, version]
    CContainer  input_decoded;
    sQuest*     quest = nullptr;

    if (!tools::File::file_to_cc_size(path, input, PROBE_SIZE))
        return sQuest::QuestType::NOT_A_QUEST;

    quest = reinterpret_cast<sQuest*>(input.data());

    if (quest->checkVersion())
        return sQuest::QuestType::DECODED;
    else if (Crypto().decodeQuest(input, input_decoded))
        return sQuest::QuestType::ENCODED;
    else
        return sQuest::QuestType::NOT_A_QUEST;
}

void sQuest::print(const QuestLanguage language) const
{
    std::wstring_convert<std::codecvt_utf8<std::u16string::value_type>, std::u16string::value_type> converter;
    std::vector<std::u16string> strings;

    // text:
    strings.push_back(this->get_text(language, Text::TITLE));
    strings.push_back(this->get_text(language, Text::MAIN_GOAL));
    strings.push_back(this->get_text(language, Text::FAILURE));
    strings.push_back(this->get_text(language, Text::SUMMARY));
    strings.push_back(this->get_text(language, Text::MAIN_MONSTER));
    strings.push_back(this->get_text(language, Text::CLIENT));
    strings.push_back(this->get_text(language, Text::SUB_QUEST));

    for (const auto& string : strings)
        printf("%s\n", converter.to_bytes(string).c_str());
}

}; /// namespace mh4u
}; /// namespace mh
