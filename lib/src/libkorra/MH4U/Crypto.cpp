#include "libkorra/MH4U/Crypto.hpp"
#include "libkorra/tools/ccontainer.hpp"
#include "libkorra/tools/Blowfish.hpp"
#include "libkorra/tools/tools.hpp"

#include "libkorra/MH4U/Quest/Quest.hpp"

#include <random>
#include <cstring>

namespace mh {
namespace mh4u {

#define RNG_SEED 0x484D // 0x484D == MH
static const uint8_t KeyEXTData[]       = "blowfish key iorajegqmrna4itjeangmb agmwgtobjteowhv9mope";
static const uint8_t KeyDLC_EUR_NA[]    = "AgK2DYheaCjyHGPB";
static const uint8_t KeyDLC_JPN[]       = "AgK2DYheaCjyHGP8";
static const uint8_t KeyDLC_KOR[]       = "AgK2DYheaOjyHGP8";
static const uint8_t KeyDLC_TW[]        = "Capcom123 ";

static bool seed_rng_once = false;
static std::mt19937 rng;

Crypto::Crypto()
    : m_blowfish(new BlowFish)
{
    // guard in case virtual inheritance fails
    if (!seed_rng_once) {
        seed_rng_once = true;
        rng.seed(RNG_SEED);
    }
}

Crypto::~Crypto()
{
    delete m_blowfish;
}

bool Crypto::decodeSave(const CContainer &in,
                         CContainer &out)
{
    bool res = true;

    res &= blowfishDecode(in, out, Crypto::Key::EXT_DATA);
    res &= xorDecode(out);

    return res;
}

bool Crypto::encodeSave(const CContainer &in,
                         CContainer &out)
{
    CContainer temp = in;

    xorEncode(temp);

    return blowfishEncode(temp, out, Crypto::Key::EXT_DATA);;
}

bool Crypto::decodeDlc(const CContainer &in,
                        CContainer &out)
{
    for (size_t i = Key::DLC_EUR_NA; i < Key::LENGTH; i++) {
        blowfishDecode(in, out, static_cast<Key>(i));

        sQuest* quest = reinterpret_cast<sQuest*>(out.data());

        if (quest->checkVersion())
            return true;
    }

    return false;
}

bool Crypto::encodeDlc(const CContainer &in,
                        CContainer &out,
                        const Key key)
{
    return blowfishEncode(in, out, key);
}

bool Crypto::blowfishDecode(const CContainer &in,
                             CContainer &out,
                             const Key key)
{
    const uint8_t* selected_key = nullptr;
    u32 out_size;

    out_size = m_blowfish->get_output_length(in.size());

    out.clear();
    out.resize(out_size);

    selected_key = getKey(key);

    if (selected_key) {
        auto size = std::strlen((char*)selected_key);

        m_blowfish->initialize((char*)selected_key, size);
        m_blowfish->decode(in.data(), out.data(), out_size);

        return true;
    } else return false;
}

bool Crypto::blowfishEncode(const CContainer &in,
                             CContainer &out,
                             const Key key)
{
    const uint8_t* selected_key = nullptr;
    u32 out_size;

    out_size = m_blowfish->get_output_length(in.size());

    out.clear();
    out.resize(out_size);

    selected_key = getKey(key);

    if (selected_key) {
        auto size = std::strlen((char*)selected_key);

        m_blowfish->initialize((char*)selected_key, size);
        m_blowfish->encode(in.data(), out.data(), out_size);

        return true;
    } else return false;
}

bool Crypto::xorDecode(CContainer &cc)
{
    u32 seed;
    u32 checksum, checksum2;

    seed = cc.castAs<u32>(0) >> 16;
    cc.subBefore(sizeof(u32)); // remove seed

    xorMH4U(cc, seed);

    checksum = cc.castAs<u32>(0);
    cc.subBefore(sizeof(u32)); // remove checksum

    checksum2 = tools::calculate_checksum(cc);

    if (checksum == checksum2)
        return true;
    return false;
}

void Crypto::xorEncode(CContainer &cc)
{
    u16 random; // must be u16
    u32 seed, seed_header;
    u32 checksum;

    random      = rng();
    seed        = seed_header = random;
    seed_header = (seed_header << 16) + 0x10;
    checksum    = tools::calculate_checksum(cc);

    insertValue(cc, checksum);

    xorMH4U(cc, seed);

    insertValue(cc, seed_header);
}

void Crypto::xorMH4U(CContainer &data,
                      u32 seed)
{
    u32 i = 0;

    // /2 because reading as u16
    while(i < data.size() / 2) {
        if (seed == 0)
            seed = 1;

        seed = seed * 0xB0 % 0xFF53;
        data.castAs<u16>(i) ^= seed;

        i++;
    };
}

const uint8_t *Crypto::getKey(const Key key) const
{
    switch (key) {
    case Key::EXT_DATA: return mh4u::KeyEXTData;
    case Key::DLC_EUR_NA: return mh4u::KeyDLC_EUR_NA;
    case Key::DLC_JPN: return mh4u::KeyDLC_JPN;
    case Key::DLC_KOR: return mh4u::KeyDLC_KOR;
    case Key::DLC_TW: return mh4u::KeyDLC_TW;
    default:
    case Key::LENGTH: return nullptr;
    }
}

void Crypto::insertValue(CContainer& data,
                          u32 value)
{
    data.addBefore(sizeof(u32));
    data.castAs<u32>(0) = value;

//    size_t i = 0;
//    value = Utils::swap_endianness<u32>(value);


//    while (i < 4){
//        if (i != 0) {value >>= 8;}
//        data.add_before(1);
//        data.as<u8>(0) = value;
//        i++;
//    }
}

}; /// namespace mh4u
}; /// namespace mh
