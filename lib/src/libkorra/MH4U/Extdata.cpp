#include "libkorra/MH4U/Extdata.hpp"
#include "libkorra/MH4U/Quest/Quest.hpp"

#include "libkorra/tools/tools.hpp"
#include "libkorra/tools/file.hpp"
#include "libkorra/tools/folder.hpp"
#include "libkorra/tools/sort.hpp"

#include <iostream>

namespace mh {
namespace mh4u {
namespace extdata {

#define BUFFER_SIZE 16
#define PROBE_SIZE sizeof(u32) * 4

DLCExtractor::DLCExtractor()
    : m_ext_quest_data(nullptr)
{
}

DLCExtractor::DLCExtractor(CContainer *ext_quest_data)
    : m_ext_quest_data(ext_quest_data)
{
}

DLCExtractor::~DLCExtractor()
{
    m_ext_quest_data = nullptr;
}

void DLCExtractor::get_decoded(CContainer* output, const bool mh4)
{
    if (mh4) {
        output = m_ext_quest_data;
        Crypto::xorDecode(*output);
    } else {
        Crypto::decodeExtData(*m_ext_quest_data, *output);
    }
}

void DLCExtractor::get_all_mib(std::vector<CContainer>& output, const bool mh4)
{
    CContainer temp;

    if (mh4) {
        temp = *m_ext_quest_data;
        Crypto::xorDecode(temp);
    } else {
        Crypto::decodeExtData(*m_ext_quest_data, temp);
    }

    if (temp.size() / QUEST_SIZE > EXT_QUEST_DATA_AMMOUNT)
        return;

    auto limit = mh4 ? MH4_EXT_QUEST_DATA_AMMOUNT
                     : EXT_QUEST_DATA_AMMOUNT;

    for (auto i = 0; i < limit; i++) {
        const sQuest* quest = reinterpret_cast<sQuest*>(temp.data() + i * QUEST_SIZE);

        if (quest->checkVersion()) {
            CContainer container(quest, QUEST_SIZE);

            output.push_back(std::move(container));
        }
    }
}

#ifndef N3DS
DLCExtractor_Helper::DLCExtractor_Helper()
{
}

DLCExtractor_Helper::DLCExtractor_Helper(const fs::path &path)
    : m_data_path(path)
    , m_out_folder(path)
{
}

bool DLCExtractor_Helper::extract(const bool dump_decoded,
                                  const bool mh4)
{
    auto files = tools::Folder(m_data_path).Get_ListOfFiles();

    if (files.empty())
        return false;

    for (const auto& filepath : files) {
        if (is_quest_ext_file(filepath)) {
            CContainer encrypted_ext_quest;

            m_dlc_vector.clear();

            if (fs::file_size(filepath) != EXT_QUEST_FILE_SIZE)
                ;
//                NotifyError("DLCExtractor_Helper:: quest file corrupt!");

            encrypted_ext_quest.read(filepath.string());

            DLCExtractor::set_ext_quest_data(&encrypted_ext_quest);

            if (dump_decoded) {
                CContainer decoded;

                DLCExtractor::get_decoded(&decoded, mh4);

                fs::path path = m_out_folder;

                path.append(filepath.filename().string() + ".dec");

                decoded.write(path.string());
            }

            DLCExtractor::get_all_mib(m_dlc_vector, mh4);

            write();
        }
    }

    return true;
}

void DLCExtractor_Helper::set_data_path(const boost::filesystem::path &path)
{
    m_data_path = path;
}

const boost::filesystem::path &DLCExtractor_Helper::get_data_path(void) const
{
    return m_data_path;
}

void DLCExtractor_Helper::set_out_folder(const boost::filesystem::path &path)
{
    m_out_folder = path;
    m_data_path.make_preferred();

    if (!fs::exists(m_out_folder))
        fs::create_directories(m_out_folder);
}

const boost::filesystem::path &DLCExtractor_Helper::get_out_folder() const
{
    return m_out_folder;
}

void DLCExtractor_Helper::write(void) const
{
    for (const auto& cc : m_dlc_vector) {
        const sQuest* quest = reinterpret_cast<const sQuest*>(cc.data());

        // no need to check, but we do it anyway
        if (quest->checkVersion()) {
            char    buffer[BUFFER_SIZE];
            auto*   flags = quest->get_sFlags();
            auto    id = flags->QuestID;

            snprintf(buffer, BUFFER_SIZE, "q%05d.mib", id);

            auto outpath = m_out_folder;
            outpath.append(buffer);

            tools::File::Data_To_File(outpath, quest, QUEST_SIZE);
        }
    }
}

bool DLCExtractor_Helper::is_quest_ext_file(const fs::path &path) const
{
    static const std::string names[] = {
        "quest1",
        "quest2",
        "quest3",
        "quest4",
        "quest5"
    };

    for (const auto& name : names) {
        if (name.compare(path.filename().string()) == 0)
            return true;
    }
    return false;
}
#endif

DLCRepacker::DLCRepacker()
    : m_dlc_vector(nullptr)
{
}

DLCRepacker::DLCRepacker(std::vector<CContainer> *dlc_vector)
    : m_dlc_vector(dlc_vector)
{
}

DLCRepacker::~DLCRepacker()
{
    m_dlc_vector = nullptr;
}

void DLCRepacker::perform(std::array<CContainer, EXT_QUEST_FILES_AMMOUNT>* output,
                          const bool mh4)
{
    auto split = split_by_id(mh4);
    size_t begin = 0, end = 0;

    for (auto i = 0
         ; i < (mh4 ? MH4_EXT_QUEST_FILES_AMMOUNT : EXT_QUEST_FILES_AMMOUNT)
         ; i++) {
        CContainer  in;
        CContainer& out = output->at(i);
        const auto& value = split.at(i);

        end += value;

        quest_file_populate(in, begin, end, mh4);

        std::cout << in.size() << std::endl;

        quest_file_encode(in, out, mh4);

        begin += value;
    }
}

void DLCRepacker::set_dlc_vector(std::vector<CContainer> *vector)
{
    m_dlc_vector = vector;
}

void DLCRepacker::quest_file_populate(CContainer &in,
                                      const size_t vector_begin_index,
                                      const size_t vector_end_index,
                                      const bool mh4)
{
    char    buffer[BUFFER_SIZE]{0};
    size_t  offset = 0;

    in.resize(mh4 ? MH4_EXT_QUEST_DATA_SIZE : EXT_QUEST_DATA_SIZE);

    if (vector_begin_index > vector_end_index
        || vector_begin_index == vector_end_index)
        return;

    for (size_t i = vector_begin_index; i < vector_end_index; i++) {
        const auto& quest_data = m_dlc_vector->at(i);

        const auto* quest = reinterpret_cast<const sQuest*>(quest_data.data());

        tools::copyBytes(in.data() + offset, quest_data.data(), quest_data.size());

        offset += QUEST_SIZE;

        // write quest name
        snprintf(buffer, BUFFER_SIZE, "m%5d.mib", quest->get_sFlags()->QuestID);
        tools::copyBytes(in.data() + offset - 0x10, buffer, BUFFER_SIZE);
    }
}

void DLCRepacker::quest_file_encode(CContainer &in,
                                    CContainer &out,
                                    const bool mh4)
{
    if (mh4) {
        out = in;

        Crypto::xorEncode(out);
    } else {
        Crypto::encodeExtData(in, out);
    }

    out.addAfter(EXT_QUEST_DATA_PADDING);
}



/* ranges:
 * 60000 to 61000 - normal quest
 * 61000 to 62000 - arena challenge quests
 * 62000 -> ~ - gq?
 *
 * 61200 - 62000
 *
 * from ext quests:
 * quest1: 60001 - 60207
 * quest2: 60208 - 60251
 * quest3: empty in mh4u, 60229 - 60259 in mh4g
 * quest4: 61001 - 61208
 * quest5: 62101 - 62212
 *
 * types:
 * LR | HR | G
 * LR/HR Arena | G Arena
 * Episode events (possibly count as lr|hr|g)
*/
const std::array<u8, 5> DLCRepacker::split_by_id(const bool mh4) const
{
    std::array<u8, EXT_QUEST_FILES_AMMOUNT> arr{0, 0, 0, 0, 0};

    for (const auto& cc : *m_dlc_vector) {
        const auto* quest = reinterpret_cast<const sQuest*>(cc.data());
        const auto& id = quest->get_sFlags()->QuestID;

        if (id >= 60000 && id < 61000) { // quest1 quest2 quest3
            for (size_t i = 0; i < 3; i++) {
                if (arr[i] < (mh4 ? MH4_EXT_QUEST_DATA_AMMOUNT : EXT_QUEST_DATA_AMMOUNT)) {
                    arr[i]++;
                    break;
                } else continue;
            }
        } else if (id >= 61000 && id < 62000) { // quest4
            if (arr[3] < (mh4 ? MH4_EXT_QUEST_DATA_AMMOUNT : EXT_QUEST_DATA_AMMOUNT))
                arr[3]++;
        } else if (id >= 62000) { // quest5
            if (arr[4] < (mh4 ? MH4_EXT_QUEST_DATA_AMMOUNT : EXT_QUEST_DATA_AMMOUNT))
                arr[4]++;
        }
    }

    return arr;
}

#ifndef N3DS
DLCRepacker_Helper::DLCRepacker_Helper(const fs::path &path)
    : m_data_path(path), m_out_folder(path)
{
    m_out_folder.append("quest");

    if (!fs::exists(m_out_folder)
        && !fs::is_directory(m_out_folder))
        fs::create_directories(m_out_folder);
}

bool DLCRepacker_Helper::prepare(const bool mh4)
{
    read_files(mh4);

    dlc_vector_sort();
    find_duplicates();

    return m_dlc_duplicates.size() == 0;
}

void DLCRepacker_Helper::repack(const bool mh4)
{
    DLCRepacker::set_dlc_vector(&m_dlc_vector);
    DLCRepacker::perform(&m_encoded_ext_quests, mh4);
}

void DLCRepacker_Helper::read_files(const bool mh4)
{
    auto files = tools::Folder(m_data_path).Get_ListOfFiles();

    m_dlc_vector.reserve(EXT_QUEST_FILES_AMMOUNT * (mh4 ? MH4_EXT_QUEST_DATA_AMMOUNT
                                                        : EXT_QUEST_DATA_AMMOUNT));

    for (const auto& path : files) {
        const auto& size = fs::file_size(path);
        sQuest::QuestType res;

        if (size > QUEST_SIZE + XOR_MH4U_SIZE)
            continue;
        else
            res = sQuest::isQuestFile(path);

        if (res == sQuest::QuestType::DECODED) {
            m_dlc_vector.push_back({});
            m_dlc_vector.back().read(path.string());
        } else if (res == sQuest::QuestType::ENCODED) {
            CContainer encoded;

            encoded.read(path.string());

            m_dlc_vector.push_back({});

            Crypto::decodeQuest(encoded, m_encoded_ext_quests.back());
        }
    }
}

void DLCRepacker_Helper::write(const bool mh4) const
{
    for (auto i = 0
         ; i < (mh4 ? MH4_EXT_QUEST_FILES_AMMOUNT : EXT_QUEST_FILES_AMMOUNT)
         ; i++) {
        char buffer[BUFFER_SIZE]{0};
        auto path = m_out_folder;

        snprintf(buffer, BUFFER_SIZE, "quest%u", i + 1);

        path.append(buffer);

        this->m_encoded_ext_quests.at(i).write(path.string());
    }
}

void DLCRepacker_Helper::set_data_path(const boost::filesystem::path &path)
{
    m_data_path = path;
}

const boost::filesystem::path &DLCRepacker_Helper::get_data_path(void) const
{
    return m_data_path;
}

void DLCRepacker_Helper::set_out_folder(const boost::filesystem::path &path)
{
    m_out_folder = path;
}

const boost::filesystem::path &DLCRepacker_Helper::get_out_folder(void) const
{
    return m_out_folder;
}

const std::vector<size_t> &DLCRepacker_Helper::get_duplicates_vector(void) const
{
    return m_dlc_duplicates;
}

const DLCRepacker_Helper::dlc_info DLCRepacker_Helper::get_dlc_info(const size_t& id,
                                                                    const QuestLanguage& language,
                                                                    const Text& text) const
{
    const sQuest* quest = reinterpret_cast<const sQuest*>(this->m_dlc_vector.at(id).data());

    return {quest->get_sFlags()->QuestID, quest->get_text(language, text)};
}

void DLCRepacker_Helper::dlc_vector_sort(void)
{
//#define SORT_TIME
    auto filter = [&](const CContainer& left, const CContainer& right) -> bool
    {
        sQuest* lquest = reinterpret_cast<sQuest*>(left.data());
        sQuest* rquest = reinterpret_cast<sQuest*>(right.data());

        return lquest->get_sFlags()->QuestID > rquest->get_sFlags()->QuestID;
    };

#ifdef SORT_TIME
    auto start = std::chrono::system_clock::now();
#endif

    sort_swap_only(m_dlc_vector.begin(), m_dlc_vector.end(), filter);

#ifdef SORT_TIME
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = end - start;

    printf("sort time: %f\n", diff.count());
#endif

//   for (const auto& cc : this->m_dlc_vector) {
//       const auto* quest = reinterpret_cast<const sQuest*>(cc.data());

//       printf("%d\n", quest->get_sFlags()->quest_id);
//   }
}

void DLCRepacker_Helper::find_duplicates(void)
{
    for (size_t i = 0; i < m_dlc_vector.size(); i++) {
        const auto* quest0 = reinterpret_cast<const sQuest*>(m_dlc_vector.at(i).data());
        const auto& quest_id0 = quest0->get_sFlags()->QuestID;
        bool        duplicate = false;

        for (size_t j = i + 1; j < m_dlc_vector.size(); j++) {
            const auto* quest1 = reinterpret_cast<const sQuest*>(m_dlc_vector.at(j).data());
            const auto& quest_id1 = quest1->get_sFlags()->QuestID;

            if (quest_id0 == quest_id1) {
                duplicate = true;

                m_dlc_duplicates.push_back(j);
            }
        }

        if (duplicate)
            m_dlc_duplicates.push_back(i);
    }
}
#endif

}; /// namespace extdata
}; /// namespace mh4u
}; /// namespace mh
