#include "libkorra/MH4U/Save/Savefile.hpp"
#include <cstring>

namespace mh {
namespace mh4u {

cSavefile::cSavefile()
    : m_header(nullptr)
{

}

cSavefile::cSavefile(CContainer *cc)
{
//    if (cc->size() != SAVEFILE_SIZE_BYTES)
//        return;

//    this->assign_header(cc->data());
}

cSavefile::~cSavefile()
{
    m_header = nullptr;
}

const std::u16string sSavefile::get_player_name(void) const
{
    std::u16string out;

    for (const auto& ch : player_name)
        if (ch != 0)
            out += ch;

    return out;
}

bool sSavefile::set_player_name(const std::u16string &name)
{
    if (name.size() * sizeof(utf16) > PLAYER_NAME_SIZE_BYTES_MAX
        || name.length() > NAME_LENGTH_MAX)
        return false;

    std::memset(player_name,
                0,
                sizeof(player_name));

    for (size_t i = 0; i < name.size(); i++)
        player_name[i] = name.at(i);

    return true;
}

Gender sSavefile::get_gender(void) const { return gender; }
void sSavefile::set_gender(const Gender gender) { this->gender = gender; }
Face sSavefile::get_face(void) const { return face; }
void sSavefile::set_face(const Face face) { this->face = face; }
Hairstyle sSavefile::get_hairstyle(void) const { return hairstyle; }
void sSavefile::set_hairstyle(const Hairstyle hairstyle) { this->hairstyle = hairstyle; }
Clothing sSavefile::get_clothing(void) const { return clothing; }
void sSavefile::set_clothing(const Clothing clothing) { this->clothing = clothing; }
Voice sSavefile::get_voice(void) const { return voice; }
void sSavefile::set_voice(const Voice voice) { this->voice = voice; }
EyeColor sSavefile::get_eye_color(void) const { return eye_color; }
void sSavefile::set_eye_color(const EyeColor eye_color) { this->eye_color = eye_color; }
Features sSavefile::get_face_features(void) const { return face_features; }
void sSavefile::set_face_features(const Features features) { this->face_features = features; }

}; /// namespace mh4u
}; /// namespace mh
