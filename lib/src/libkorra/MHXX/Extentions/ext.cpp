#include <libkorra/MHXX/Extentions/ext.hpp>
#include "libkorra/tools/ccontainer.hpp"
#include "libkorra/tools/magicVersion.h"
#include "libkorra/function_name.h"

#include <stdexcept>

namespace mh {
namespace mhxx {
namespace ext {

auto parse(const CContainer &container, cEXT &obj) noexcept -> bool
{
    if (container.size() < sizeof(Header::MAGIC) + sizeof(Header::VERSION))
        return false;

    if (!tools::MagicVersion(Header::MAGIC, Header::VERSION)(container.data()))
        return false;

    if (   !obj.readHeader(container)
        || !obj.readLinks(container)
        || !obj.readHeader1(container))
        return false;

    obj.m_valid = true;

    return true;
}

auto cEXT::isValid(void) const noexcept -> bool { return m_valid; }

auto cEXT::dump(CContainer &container) noexcept -> void
{
    fixGMDLinkAmmount();

    auto totalSize =   sizeof(Header)
                     + m_gmdLinks.size() * sizeof(sGMDLink)
                     + sizeof(Header1);

    container.resize(totalSize, true);

    container.castAt<Header>() = m_header0;

    for (auto i = 0u; i < m_gmdLinks.size(); i++) {
        auto offset =   sizeof(Header)
                      + sizeof(sGMDLink) * i;

        container.castAt<sGMDLink>(offset) = m_gmdLinks[i];
    }

    auto offset =   sizeof(Header)
                  + m_gmdLinks.size() * sizeof(sGMDLink);

    container.castAt<Header1>(offset) = m_header1;
}

auto cEXT::getIndex(void) const noexcept -> u32 { return m_header0.Index; }
auto cEXT::setIndex(const u32 val) noexcept -> void { m_header0.Index = val; }

auto cEXT::getQuestID(void) const noexcept -> u32 { return m_header0.QuestID; }
auto cEXT::setQuestID(const u32 val) noexcept -> void { m_header0.QuestID = val; }

auto cEXT::getQuestType0(void) const noexcept -> sQuestType0::e { return m_header0.QuestType0; }
auto cEXT::setQuestType0(const sQuestType0::e val) noexcept -> void { m_header0.QuestType0 = val; }

auto cEXT::getQuestType1(void) const noexcept -> sQuestType1::e { return m_header0.QuestType1; }
auto cEXT::setQuestType1(const sQuestType1::e val) noexcept -> void { m_header0.QuestType1 = val; }

auto cEXT::getQuestLv(void) const noexcept -> sQuestLv::e { return m_header0.QuestLv; }
auto cEXT::setQuestLv(const sQuestLv::e val) -> void { m_header0.QuestLv = val; }

auto cEXT::getBossLv(void) const noexcept -> sEnemyLv::e { return m_header0.BossLv; }
auto cEXT::setBossLv(const sEnemyLv::e val) -> void { m_header0.BossLv = val; }

auto cEXT::getMapNo(void) const noexcept -> sMaps::e { return m_header0.MapNo; }
auto cEXT::setMapNo(const sMaps::e val) noexcept -> void { m_header0.MapNo = val; }

auto cEXT::getStartType(void) const noexcept -> sStartType::e { return m_header0.StartType; }
auto cEXT::setStartType(const sStartType::e val) noexcept -> void { m_header0.StartType = val; }

auto cEXT::getQuestTime(void) const noexcept -> u8 { return m_header0.QuestTime; }
auto cEXT::setQuestTime(const u8 val) noexcept -> void { m_header0.QuestTime = val; }

auto cEXT::getQuestLives(void) const noexcept -> u8 { return m_header0.QuestLives; }
auto cEXT::setQuestLives(const u8 val) noexcept -> void { m_header0.QuestLives = val; }

auto cEXT::getAcEquipSetNo(void) const noexcept -> u8 { return m_header0.AcEquipSetNo; }
auto cEXT::setAcEquipSetNo(const u8 val) noexcept -> void { m_header0.AcEquipSetNo = val; }

auto cEXT::getBGMType(void) const noexcept -> sBGMType::e { return m_header0.BGMType; }
auto cEXT::setBGMType(const sBGMType::e val) noexcept -> void { m_header0.BGMType = val; }

auto cEXT::getEntryType(const std::size_t id) const noexcept(false) -> sEntryType::e
{
    if (id < 2)
        return m_header0.EntryType[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cEXT::getEntryType0(void) const noexcept -> sEntryType::e { return getEntryType(0); }
auto cEXT::getEntryType1(void) const noexcept -> sEntryType::e { return getEntryType(1); }

auto cEXT::setEntryType(const std::size_t id, const sEntryType::e val) noexcept -> bool
{
    if (id < 2) {
        m_header0.EntryType[id] = val;

        return true;
    }

    return false;
}

auto cEXT::setEntryType0(const sEntryType::e val) noexcept -> void { (void)setEntryType(0, val); }
auto cEXT::setEntryType1(const sEntryType::e val) noexcept -> void { (void)setEntryType(1, val); }

auto cEXT::getEntryTypeCombo(void) const noexcept -> u8 { return m_header0.EntryTypeCombo; }
auto cEXT::setEntryTypeCombo(const u8 val) noexcept -> void { m_header0.EntryTypeCombo = val; }

auto cEXT::getClearType(void) const noexcept -> sClearType::e { return m_header0.ClearType; }
auto cEXT::setClearType(const sClearType::e val) noexcept -> void { m_header0.ClearType = val; }

auto cEXT::getGekitaiHP(void) const noexcept -> u8 { return m_header0.GekitaiHP; }
auto cEXT::setGekitaiHP(const u8 val) noexcept -> void { m_header0.GekitaiHP = val; }

auto cEXT::getTargetMain(const std::size_t id) const noexcept(false) -> sTarget
{
    if (id < 2)
        return m_header0.TargetMain[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cEXT::getTargetMain0(void) const noexcept -> sTarget { return getTargetMain(0); }
auto cEXT::getTargetMain1(void) const noexcept -> sTarget { return getTargetMain(1); }

auto cEXT::setTargetMain(const std::size_t id, const sTarget val) noexcept -> bool
{
    if (id < 2) {
        m_header0.TargetMain[id] = val;

        return true;
    }

    return false;
}

auto cEXT::setTargetMain0(const sTarget val) noexcept -> void { (void)setTargetMain(0, val); }
auto cEXT::setTargetMain1(const sTarget val) noexcept -> void { (void)setTargetMain(1, val); }

auto cEXT::getTargetSub(void) const noexcept -> sTarget { return m_header0.TargetSub; }
auto cEXT::setTargetSub(const sTarget val) noexcept -> void { m_header0.TargetSub = val; }

auto cEXT::getCarvingLv(void) const noexcept -> sCarvingLv::e { return m_header0.CarvingLv; }
auto cEXT::setCarvingLv(const sCarvingLv::e val) noexcept -> void { m_header0.CarvingLv = val; }

auto cEXT::getGatheringLv(void) const noexcept -> sGatheringLv::e { return m_header0.GatheringLv; }
auto cEXT::setGatheringLv(const sGatheringLv::e val) noexcept -> void { m_header0.GatheringLv = val; }

auto cEXT::getFishingLv(void) const noexcept -> sFishingLv::e { return m_header0.FishingLv; }
auto cEXT::setFishingLv(const sFishingLv::e val) noexcept -> void { m_header0.FishingLv = val; }

auto cEXT::getEntryFee(void) const noexcept -> u32 { return m_header0.EntryFee; }
auto cEXT::setEntryFee(const u32 val) noexcept -> void { m_header0.EntryFee = val; }

auto cEXT::getVillagePoints(void) const noexcept -> u32 { return m_header0.VillagePoints; }
auto cEXT::setVillagePoints(const u32 val) noexcept -> void { m_header0.VillagePoints = val; }

auto cEXT::getMainRewardMoney(void) const noexcept -> u32 { return m_header0.MainRewardMoney; }
auto cEXT::setMainRewardMoney(const u32 val) noexcept -> void { m_header0.MainRewardMoney = val; }

auto cEXT::getSubRewardMoney(void) const noexcept -> u32 { return m_header0.SubRewardMoney; }
auto cEXT::setSubRewardMoney(const u32 val) noexcept -> void { m_header0.SubRewardMoney = val; }

auto cEXT::getClearRemVillagePoint(void) const noexcept -> u32 { return m_header0.ClearRemVillagePoint; }
auto cEXT::setClearRemVillagePoint(const u32 val) noexcept -> void { m_header0.ClearRemVillagePoint = val; }

auto cEXT::getFailedRemVillagePoint(void) const noexcept -> u32 { return m_header0.FailedRemVillagePoint; }
auto cEXT::setFailedRemVillagePoint(const u32 val) noexcept -> void { m_header0.FailedRemVillagePoint = val; }

auto cEXT::getSubRemVillagePoint(void) const noexcept -> u32 { return m_header0.SubRemVillagePoint; }
auto cEXT::setSubRemVillagePoint(const u32 val) noexcept -> void { m_header0.SubRemVillagePoint = val; }

auto cEXT::getClearRemHunterPoint(void) const noexcept -> u32 { return m_header0.ClearRemHunterPoint; }
auto cEXT::setClearRemHunterPoint(const u32 val) noexcept -> void { m_header0.ClearRemHunterPoint = val; }

auto cEXT::getSubRemHunterPoint(void) const noexcept -> u32 { return m_header0.SubRemHunterPoint; }
auto cEXT::setSubRemHunterPoint(const u32 val) noexcept -> void { m_header0.SubRemHunterPoint = val; }

auto cEXT::getRemAddFrame(const std::size_t id) const noexcept(false) -> u8
{
    if (id < 2)
        return m_header0.RemAddFrame[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cEXT::getRemAddFrame0(void) const noexcept -> u8 { return getRemAddFrame(0); }
auto cEXT::getRemAddFrame1(void) const noexcept -> u8 { return getRemAddFrame(1); }

auto cEXT::setRemAddFrame(const std::size_t id, const u8 val) noexcept -> bool
{
    if (id < 2) {
        m_header0.RemAddFrame[id] = val;

        return true;
    }

    return false;
}

auto cEXT::setRemAddFrame0(const u8 val) noexcept -> void { (void)setRemAddFrame(0, val); }
auto cEXT::setRemAddFrame1(const u8 val) noexcept -> void { (void)setRemAddFrame(1, val); }

auto cEXT::getSupply(const std::size_t id) const noexcept(false) -> sSupply
{
    if (id < 2)
        return m_header0.Supply[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cEXT::getSupply0(void) const noexcept -> sSupply { return getSupply(0); }
auto cEXT::getSupply1(void) const noexcept -> sSupply { return getSupply(1); }

auto cEXT::setSupply(const std::size_t id, const sSupply val) noexcept -> bool
{
    if (id < 2) {
        m_header0.Supply[id] = val;

        return true;
    }

    return false;
}

auto cEXT::setSupply0(const sSupply val) noexcept -> void { (void)setSupply(0, val); }
auto cEXT::setSupply1(const sSupply val) noexcept -> void { (void)setSupply(1, val); }

auto cEXT::getBoss(const std::size_t id) const noexcept(false) -> sBoss
{
    if (id < 5)
        return m_header0.Boss[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cEXT::getBoss0(void) const noexcept -> sBoss { return getBoss(0); }
auto cEXT::getBoss1(void) const noexcept -> sBoss { return getBoss(1); }
auto cEXT::getBoss2(void) const noexcept -> sBoss { return getBoss(2); }
auto cEXT::getBoss3(void) const noexcept -> sBoss { return getBoss(3); }
auto cEXT::getBoss4(void) const noexcept -> sBoss { return getBoss(4); }

auto cEXT::setBoss(const std::size_t id, const sBoss val) noexcept -> bool
{
    if (id < 5) {
        m_header0.Boss[id] = val;

        return true;
    }

    return false;
}

auto cEXT::setBoss0(const sBoss val) noexcept -> void { (void)setBoss(0, val); }
auto cEXT::setBoss1(const sBoss val) noexcept -> void { (void)setBoss(1, val); }
auto cEXT::setBoss2(const sBoss val) noexcept -> void { (void)setBoss(2, val); }
auto cEXT::setBoss3(const sBoss val) noexcept -> void { (void)setBoss(3, val); }
auto cEXT::setBoss4(const sBoss val) noexcept -> void { (void)setBoss(4, val); }

auto cEXT::getSmallEmHPTbl(void) const noexcept -> u8 { return m_header0.SmallEmHPTbl; }
auto cEXT::setSmallEmHPTbl(const u8 val) noexcept -> void { m_header0.SmallEmHPTbl = val; }

auto cEXT::getSmallEmAttackTbl(void) const noexcept -> u8 { return m_header0.SmallEmAttackTbl; }
auto cEXT::setSmallEmAttackTbl(const u8 val) noexcept -> void { m_header0.SmallEmAttackTbl = val; }

auto cEXT::getSmallEmOtherTbl(void) const noexcept -> u8 { return m_header0.SmallEmOtherTbl; }
auto cEXT::setSmallEmOtherTbl(const u8 val) noexcept -> void { m_header0.SmallEmOtherTbl = val; }

auto cEXT::getAppear(const std::size_t id) const noexcept(false) -> sAppear
{
    if (id < 5)
        return m_header0.Appear[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cEXT::getAppear0(void) const noexcept -> sAppear { return getAppear(0); }
auto cEXT::getAppear1(void) const noexcept -> sAppear { return getAppear(1); }
auto cEXT::getAppear2(void) const noexcept -> sAppear { return getAppear(2); }
auto cEXT::getAppear3(void) const noexcept -> sAppear { return getAppear(3); }
auto cEXT::getAppear4(void) const noexcept -> sAppear { return getAppear(4); }

auto cEXT::getInvaderAppearChance(void) const noexcept -> u8 { return m_header0.InvaderAppearChance; }
auto cEXT::setInvaderAppearChance(const u8 val) noexcept -> void { m_header0.InvaderAppearChance = val; }

auto cEXT::getInvaderStartTime(void) const noexcept -> u8 { return m_header0.InvaderStartTime; }
auto cEXT::setInvaderStartTime(const u8 val) noexcept -> void { m_header0.InvaderStartTime = val; }

auto cEXT::getInvaderStartRandChance(void) const noexcept -> u8 { return m_header0.InvaderStartRandChance; }
auto cEXT::setInvaderStartRandChance(const u8 val) noexcept -> void { m_header0.InvaderStartRandChance = val; }

auto cEXT::getStrayLimit(const std::size_t id) const noexcept(false) -> u8
{
    if (id < 3)
        return m_header0.StrayLimit[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cEXT::getStrayLimit0(void) const noexcept -> u8 { return getStrayLimit(0); }
auto cEXT::getStrayLimit1(void) const noexcept -> u8 { return getStrayLimit(1); }
auto cEXT::getStrayLimit2(void) const noexcept -> u8 { return getStrayLimit(2); }

auto cEXT::setStrayLimit(const std::size_t id, const u8 val) noexcept -> bool
{
    if (id < 3) {
        m_header0.StrayLimit[id] = val;

        return true;
    }

    return false;
}

auto cEXT::setStrayLimit0(const u8 val) noexcept -> void { (void)setStrayLimit(0, val); }
auto cEXT::setStrayLimit1(const u8 val) noexcept -> void { (void)setStrayLimit(1, val); }
auto cEXT::setStrayLimit2(const u8 val) noexcept -> void { (void)setStrayLimit(2, val); }

auto cEXT::getStrayRand2(const std::size_t id) const noexcept(false) -> u8
{
    if (id < 3)
        return m_header0.StrayRand2[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cEXT::getStrayRand2_0(void) const noexcept -> u8 { return getStrayRand2(0); }
auto cEXT::getStrayRand2_1(void) const noexcept -> u8 { return getStrayRand2(1); }
auto cEXT::getStrayRand2_2(void) const noexcept -> u8 { return getStrayRand2(2); }

auto cEXT::setStrayRand2(const std::size_t id, const u8 val) noexcept -> bool
{
    if (id < 3) {
        m_header0.StrayRand2[id] = val;

        return true;
    }

    return false;
}

auto cEXT::setStrayRand2_0(const u8 val) noexcept -> void { (void)setStrayRand2(0, val); }
auto cEXT::setStrayRand2_1(const u8 val) noexcept -> void { (void)setStrayRand2(1, val); }
auto cEXT::setStrayRand2_2(const u8 val) noexcept -> void { (void)setStrayRand2(2, val); }

auto cEXT::getSPExtraTicketNum(void) const noexcept -> u8 { return m_header0.SPExtraTicketNum; }
auto cEXT::setSPExtraTicketNum(const u8 val) noexcept -> void { m_header0.SPExtraTicketNum = val; }

auto cEXT::getIcon(const std::size_t id) const noexcept(false) -> sIcon::e
{
    if (id < 5)
        return m_header0.Icon[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cEXT::getIcon0(void) const noexcept -> sIcon::e { return getIcon(0); }
auto cEXT::getIcon1(void) const noexcept -> sIcon::e { return getIcon(1); }
auto cEXT::getIcon2(void) const noexcept -> sIcon::e { return getIcon(2); }
auto cEXT::getIcon3(void) const noexcept -> sIcon::e { return getIcon(3); }
auto cEXT::getIcon4(void) const noexcept -> sIcon::e { return getIcon(4); }

auto cEXT::setIcon(const std::size_t id, const sIcon::e val) noexcept -> bool
{
    if (id < 5) {
        m_header0.Icon[id] = val;

        return true;
    }

    return false;
}

auto cEXT::setIcon0(const sIcon::e val) noexcept -> void { (void)setIcon(0, val); }
auto cEXT::setIcon1(const sIcon::e val) noexcept -> void { (void)setIcon(1, val); }
auto cEXT::setIcon2(const sIcon::e val) noexcept -> void { (void)setIcon(2, val); }
auto cEXT::setIcon3(const sIcon::e val) noexcept -> void { (void)setIcon(3, val); }
auto cEXT::setIcon4(const sIcon::e val) noexcept -> void { (void)setIcon(4, val); }

auto cEXT::getsGMDLinkCount(void) const noexcept -> std::vector<sGMDLink>::size_type { return m_gmdLinks.size(); }

auto cEXT::getGMDLink(const std::size_t id) const noexcept(false) -> sGMDLink
{
    if (id < m_gmdLinks.size())
        return m_gmdLinks[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cEXT::setGMDLink(const std::size_t id, const sGMDLink val) -> bool
{
    if (id < 5) {
        if (m_gmdLinks.size() < id)
            m_gmdLinks.resize(id);

        m_gmdLinks[id] = val;

        return true;
    }

    return false;
}

auto cEXT::setGMDLink0(const sGMDLink val) -> void { (void)setGMDLink(0, val); }
auto cEXT::setGMDLink1(const sGMDLink val) -> void { (void)setGMDLink(1, val); }
auto cEXT::setGMDLink2(const sGMDLink val) -> void { (void)setGMDLink(2, val); }
auto cEXT::setGMDLink3(const sGMDLink val) -> void { (void)setGMDLink(3, val); }
auto cEXT::setGMDLink4(const sGMDLink val) -> void { (void)setGMDLink(4, val); }




auto cEXT::getHeader0(void) noexcept -> Header& { return m_header0; }
auto cEXT::getHeader0(void) const noexcept -> const Header& { return m_header0; }

auto cEXT::getGMDLinks(void) noexcept -> std::vector<sGMDLink>& { return m_gmdLinks; }
auto cEXT::getGMDLinks(void) const noexcept -> const std::vector<sGMDLink>& { return m_gmdLinks; }

auto cEXT::getHeader1(void) noexcept -> Header1& { return m_header1; }
auto cEXT::getHeader1(void) const noexcept -> const Header1& { return m_header1; }

auto cEXT::readHeader(const CContainer &container) noexcept -> bool
{
    if (container.size() < sizeof(Header))
        return false;

    m_header0 = container.castAs<Header>();

    return true;
}

auto cEXT::readLinks(const CContainer &container) noexcept -> bool
{
    auto linkAmmount = 0u;

    switch (container.size()) {
    default:return false;
    case MHXX_EXT_SIZE:
        linkAmmount = 1;
        break;
    case MHGU_EXT_SIZE:
        linkAmmount = 5;
        break;
    };

    if (container.size() < sizeof(Header) + sizeof(sGMDLink) * linkAmmount)
        return false;

    m_gmdLinks.reserve(linkAmmount);

    for (auto i = 0u; i < linkAmmount; i++) {
        sGMDLink link = container.castAt<sGMDLink>(  sizeof(Header)
                                                   + sizeof(sGMDLink) * i );

        m_gmdLinks.push_back(std::move(link));
    }

    return true;
}

auto cEXT::readHeader1(const CContainer &container) noexcept -> bool
{
    if (container.size() <   sizeof(Header)
                           + m_gmdLinks.size() * sizeof(sGMDLink)
                           + sizeof(Header1))
        return false;

    m_header1 = container.castAt<Header1>(  sizeof(Header)
                                          + m_gmdLinks.size() * sizeof(sGMDLink) );

    return true;
}

auto cEXT::fixGMDLinkAmmount(void) -> void
{
    if (   m_gmdLinks.size() > 1
        && m_gmdLinks.size() < 5) {
        m_gmdLinks.reserve(5);

        for (auto i = m_gmdLinks.size(); i < 5; i++)
            m_gmdLinks.push_back(m_gmdLinks[0]);
    }
}

}; /// namespase ext
}; /// namespase mhxx
}; /// namespace mh
