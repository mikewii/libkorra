#include "libkorra/MHXX/Extentions/rem.hpp"
#include "libkorra/tools/ccontainer.hpp"
#include "libkorra/tools/magicVersion.h"

namespace mh {
namespace mhxx {
namespace rem {

auto parse(const CContainer& container, cREM& obj) noexcept -> bool
{
    if (container.size() < sizeof(Header::MAGIC) + sizeof(Header::VERSION))
        return false;

    if (!tools::MagicVersion(Header::MAGIC, Header::VERSION)(container.data()))
        return false;

    if (!obj.readHeader(container))
        return false;

    obj.m_valid = true;

    return true;
}

auto cREM::isValid(void) const noexcept -> bool { return m_valid; }

auto cREM::getFlag(const u32 id, sRewardFlag& flag) const noexcept -> bool
{
    if (id < Header::FLAGS_MAX) {
        flag = m_header.Flags[id];

        return true;
    }

    return false;
}

auto cREM::getFlagNum(const u32 id, u8& flagNum) const noexcept -> bool
{
    if (id < Header::FLAGNUMS_MAX) {
        flagNum = m_header.FlagNums[id];

        return true;
    }

    return false;
}

auto cREM::getRewardItem(const u32 id, sRewardItem& rewardItem) const noexcept -> bool
{
    if (id < Header::ITEMS_MAX) {
        rewardItem = m_header.Items[id];

        return true;
    }

    return false;
}

auto cREM::setFlag(const u32 id, const sRewardFlag flag) noexcept -> bool
{
    if (id < Header::FLAGS_MAX) {
        m_header.Flags[id] = flag;

        return true;
    }

    return false;
}

auto cREM::setFlagNum(const u32 id, const s8 flagNum) noexcept -> bool
{
    if (id < Header::FLAGNUMS_MAX) {
        m_header.FlagNums[id] = flagNum;

        return true;
    }

    return false;
}

auto cREM::setRewardItem(const u32 id, const sRewardItem item) noexcept -> bool
{
    if (id < Header::ITEMS_MAX) {
        m_header.Items[id] = item;

        return true;
    }

    return false;
}

auto cREM::getRewardEm(void) noexcept -> Header& { return m_header; }
auto cREM::getRewardEm(void) const noexcept -> const Header& { return m_header; }

auto cREM::readHeader(const CContainer &container) noexcept -> bool
{
    if (container.size() != sizeof(Header))
        return false;

    m_header = container.castAs<Header>();

    return true;
}

}; /// namespase rem
}; /// namespase mhxx
}; /// namespace mh
