#include "libkorra/MHXX/Extentions/qdl.hpp"
#include "libkorra/tools/ccontainer.hpp"
#include "libkorra/tools/magicVersion.h"

namespace mh {
namespace mhxx {
namespace qdl {

auto parse(const CContainer& container, cQDL& obj) noexcept -> bool
{
    if (container.size() < sizeof(Header::MAGIC) + sizeof(Header::VERSION))
        return false;

    if (!tools::MagicVersion(Header::MAGIC, Header::VERSION)(container.data()))
        return false;

    if (!obj.readHeader(container))
        return false;

    obj.m_valid = true;

    return true;
}

auto cQDL::isValid(void) const noexcept -> bool { return m_valid; }

auto cQDL::getItem(const sQDLItemOrder id) const noexcept -> sQDLItem
{
    return m_header.Items[id];
}

std::string sQDLItem::getName(void) const noexcept
{
    return std::string(Name);
}

auto cQDL::setItem(const sQDLItemOrder id, const sQDLItem& item) noexcept -> void
{
    m_header.Items[id] = item;
}

auto cQDL::getQuestDataLink(void) noexcept -> Header& { return m_header; }
auto cQDL::getQuestDataLink(void) const noexcept -> const Header& { return m_header; }

auto cQDL::readHeader(const CContainer &container) noexcept -> bool
{
    if (container.size() != sizeof(Header))
        return false;

    m_header = container.castAs<Header>();

    return true;
}

bool sQDLItem::setName(const std::string& str) noexcept
{
    if (str.empty())
        return false;

    if (str.size() <= sQDLItem::QDL_NAME_MAX) {
        str.copy(Name, str.size());

        Name[str.size() + 1] = '\0';

        return true;
    }

    return false;
}

}; /// namespase qdl
}; /// namespase mhxx
}; /// namespace mh
