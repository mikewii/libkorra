#include "libkorra/MHXX/Extentions/sup.hpp"
#include "libkorra/tools/ccontainer.hpp"
#include "libkorra/tools/magicVersion.h"
#include "libkorra/function_name.h"

#include <stdexcept>

namespace mh {
namespace mhxx {
namespace sup {

auto parse(const CContainer& container, cSUP& obj) noexcept -> bool
{
    if (container.size() < sizeof(Header::MAGIC) + sizeof(Header::VERSION))
        return false;

    if (!tools::MagicVersion(Header::MAGIC, Header::VERSION)(container.data()))
        return false;

    if (!obj.readHeader(container))
        return false;

    obj.m_valid = true;

    return true;
}

auto cSUP::isValid(void) const noexcept -> bool { return m_valid; }

auto cSUP::getItem(const u32 id) noexcept(false) -> sSupplyItem&
{
    if (id < Header::ITEMS_MAX)
        return m_header.Items[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cSUP::getItem(const u32 id) const noexcept(false) -> const sSupplyItem&
{
    if (id < Header::ITEMS_MAX)
        return m_header.Items[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cSUP::getItem(const u32 id, sSupplyItem& item) const noexcept -> bool
{
    if (id < Header::ITEMS_MAX) {
        item = m_header.Items[id];

        return true;
    }

    return false;
}

auto cSUP::setItem(const u32 id, const sSupplyItem& item) noexcept -> bool
{
    if (id < Header::ITEMS_MAX) {
        m_header.Items[id] = item;

        return true;
    }

    return false;
}

auto cSUP::getSupply(void) noexcept -> Header&
{
    return m_header;
}

auto cSUP::getSupply(void) const noexcept -> const Header&
{
    return m_header;
}

auto cSUP::setSupply(const Header &supply) noexcept -> void
{
    m_header = supply;
}

auto cSUP::readHeader(const CContainer &container) noexcept -> bool
{
    if (container.size() != sizeof(Header))
        return false;

    m_header = container.castAs<Header>();

    return true;
}

}; /// namespase sup
}; /// namespase mhxx
}; /// namespace mh
