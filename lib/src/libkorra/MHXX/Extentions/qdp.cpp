#include "libkorra/MHXX/Extentions/qdp.hpp"
#include "libkorra/tools/ccontainer.hpp"
#include "libkorra/tools/magicVersion.h"

namespace mh {
namespace mhxx {
namespace qdp {

auto parse(const CContainer& container, cQDP& obj) noexcept -> bool
{
    if (container.size() < sizeof(Header::MAGIC) + sizeof(Header::VERSION))
        return false;

    if (!tools::MagicVersion(Header::MAGIC, Header::VERSION)(container.data()))
        return false;

    if (!obj.readHeader(container))
        return false;

    obj.m_valid = true;

    return true;
}

auto cQDP::isValid(void) const noexcept -> bool { return m_valid; }

auto cQDP::getIsFence(void) const noexcept -> bool { return m_header.isFence; }
auto cQDP::getIsFenceFromStart(void) const noexcept -> bool { return m_header.isFenceFromStart; }
auto cQDP::getFenceOpenTime(void) const noexcept -> u16 { return m_header.FenceOpenTime; }
auto cQDP::getFenceStartTime(void) const noexcept -> u16 { return m_header.FenceStartTime; }
auto cQDP::getFenceReuseTime(void) const noexcept -> u16 { return m_header.FenceReuseTime; }
auto cQDP::getIsDragonator(void) const noexcept -> bool { return m_header.isDragonator; }
auto cQDP::getDragonatorStartTime(void) const noexcept -> u16 { return m_header.DragonatorStartTime; }
auto cQDP::getDragonatorReuseTime(void) const noexcept -> u16 { return m_header.DragonatorReuseTime; }
auto cQDP::getFortHpS(void) const noexcept -> u16 { return m_header.FortHpS; }
auto cQDP::getFortHpL(void) const noexcept -> u16 { return m_header.FortHpL; }

void cQDP::setIsFence(const bool value) noexcept { m_header.isFence = value; }
void cQDP::setIsFenceFromStart(const bool value) noexcept { m_header.isFenceFromStart = value; }
void cQDP::setFenceOpenTime(const u16 num) noexcept { m_header.FenceOpenTime = num; }
void cQDP::setFenceStartTime(const u16 num) noexcept { m_header.FenceStartTime = num; }
void cQDP::setFenceReuseTime(const u16 num) noexcept { m_header.FenceReuseTime = num; }
void cQDP::setIsDragonator(const bool value) noexcept { m_header.isDragonator = value; }
void cQDP::setDragonatorStartTime(const u16 num) noexcept { m_header.DragonatorStartTime = num; }
void cQDP::setDragonatorReuseTime(const u16 num) noexcept { m_header.DragonatorReuseTime = num; }
void cQDP::setFortHpS(const u16 num) noexcept { m_header.FortHpS = num; }
void cQDP::setFortHpL(const u16 num) noexcept { m_header.FortHpL = num; }

auto cQDP::getQuestPlus(void) noexcept -> Header& { return m_header; }
auto cQDP::getQuestPlus(void) const noexcept -> const Header& { return m_header; }

auto cQDP::readHeader(const CContainer &container) noexcept -> bool
{
    if (container.size() != sizeof(Header))
        return false;

    m_header = container.castAs<Header>();

    return true;
}

}; /// namespase qdp
}; /// namespase mhxx
}; /// namespace mh
