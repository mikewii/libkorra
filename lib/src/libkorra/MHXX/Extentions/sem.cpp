#include "libkorra/MHXX/Extentions/sem.hpp"
#include "libkorra/tools/ccontainer.hpp"
#include "libkorra/tools/magicVersion.h"

namespace mh {
namespace mhxx {
namespace sem {

auto parse(const CContainer& container, cSEM& obj) noexcept -> bool
{
    if (container.size() < sizeof(Header::MAGIC) + sizeof(Header::VERSION))
        return false;

    if (!tools::MagicVersion(Header::MAGIC, Header::VERSION)(container.data()))
        return false;

    if (!obj.readHeader(container))
        return false;

    obj.m_valid = true;

    return true;
}

auto cSEM::isValid(void) const noexcept -> bool { return m_valid; }

auto cSEM::getWaveNo(void) const noexcept -> u32 { return m_header.WaveNo; }
auto cSEM::getAreaNo(void) const noexcept -> u32 { return m_header.AreaNo; }
auto cSEM::getPosition(void) const noexcept -> Geometry4<float> { return m_header.Position; }

void cSEM::setWaveNo(const u32 num) noexcept { m_header.WaveNo = num; }
void cSEM::setAreaNo(const u32 num) noexcept { m_header.AreaNo = num; }
void cSEM::setPosition(const Geometry4<float> &pos) noexcept { m_header.Position = pos; }

auto cSEM::getSetEmMain(void) noexcept -> Header& { return m_header; }
auto cSEM::getSetEmMain(void) const noexcept -> const Header& { return m_header; }

auto cSEM::readHeader(const CContainer &container) noexcept -> bool
{
    if (container.size() != sizeof(Header))
        return false;

    m_header = container.castAs<Header>();

    return true;
}

}; /// namespase sem
}; /// namespase mhxx
}; /// namespace mh
