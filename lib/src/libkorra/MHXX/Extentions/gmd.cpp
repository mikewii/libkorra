#include "libkorra/MHXX/Extentions/gmd.hpp"
#include "libkorra/tools/tools.hpp"
#include "libkorra/tools/ccontainer.hpp"
#include "libkorra/tools/magicVersion.h"
#include "libkorra/function_name.h"

#define NULL_TERMINATOR_SIZE 1

namespace mh {
namespace mhxx {
namespace gmd {

auto parse(const CContainer &container, cGMD &obj) noexcept -> bool
{
    if (container.size() <= sizeof(Header))
        return false;

    if (!tools::MagicVersion(Header::MAGIC, Header::VERSION)(container.data()))
        return false;

    obj.readHeader(container);

    if (container.size() != obj.getExpectedSize())
        return false;

    if (!obj.readFilename(container))
        return false;

    auto advanced = obj.getGMDHeader().LabelsNum > 0 ? true : false;

    if (advanced) {
        obj.readAdvanced1(container);
        obj.readAdvanced2(container);
    }

    obj.readLabels(container);
    obj.readItems(container);

    obj.m_valid = true;

    return true;
}

auto cGMD::isValid(void) const noexcept -> bool { return m_valid; }

auto cGMD::isAdvanced(void) const noexcept -> bool
{
    return m_header.LabelsNum > 0 ? true : false;
}

auto cGMD::dump(CContainer &container) -> void
{
    auto shift = sizeof(Header)
                 + m_filename.length()
                 + NULL_TERMINATOR_SIZE;
    auto totalSize = updateHeader();

    container.resize(totalSize, true);

    char* pStart = reinterpret_cast<char*>(container.data());

    /// Write header
    container.castAs<Header>() = m_header;

    /// Write filename
    tools::copyBytes(  pStart + sizeof(Header)
                     , m_filename.data()
                     , m_filename.length());

    /// Write advanced
    if (!m_advanced1.empty()) {
        for (const auto& item : m_advanced1) {
            tools::copyBytes(  pStart + shift
                             , item.Unk
                             , sizeof(sGMDAdvanced1));

            shift += sizeof(sGMDAdvanced1);
        }

        tools::copyBytes(  pStart + shift
                         , m_advanced2.Unk
                         , sizeof(sGMDAdvanced2));

        shift += sizeof(sGMDAdvanced2);
    }

    /// Write labels
    auto writeStringVector = [&](const std::vector<std::string>& vec)
    {
        for (const auto& str : vec) {
            tools::copyBytes(  pStart + shift
                             , str.data()
                             , str.length());

            shift += str.length() + NULL_TERMINATOR_SIZE;
        }
    };

    writeStringVector(m_labels);
    writeStringVector(m_items);
}

auto cGMD::readHeader(const CContainer &container) noexcept -> bool
{
    if (container.size() < sizeof(Header))
        return false;

    m_header = container.castAs<Header>();

    return true;
}

auto cGMD::readFilename(const CContainer &container) noexcept -> bool
{
    const Header& header = container.castAs<Header>();
    std::string filename;

    filename = reinterpret_cast<const char*>(container.data() + sizeof(Header));

    if (filename.length() != header.FilenameSize)
        return false;

    m_filename = std::move(filename);

    return true;
}

auto cGMD::readAdvanced1(const CContainer &container) noexcept -> void
{
    const Header& header = container.castAs<Header>();

    auto offset = sizeof(Header)
                  + header.FilenameSize
                  + NULL_TERMINATOR_SIZE;
    auto shift = 0;

    m_advanced1.reserve(header.LabelsNum);

    for (auto i = 0u; i < header.LabelsNum; i++) {
        m_advanced1.emplace_back(container.castAt<sGMDAdvanced1>(offset + shift));

        shift += sizeof(sGMDAdvanced1);
    }
}

auto cGMD::readAdvanced2(const CContainer &container) noexcept -> void
{
    const Header& header = container.castAs<Header>();

    auto advanced1Size = sizeof(sGMDAdvanced1) * header.LabelsNum;
    auto offset = sizeof(Header)
                  + header.FilenameSize
                  + NULL_TERMINATOR_SIZE
                  + advanced1Size;

    m_advanced2 = container.castAt<sGMDAdvanced2>(offset);
}

auto cGMD::readLabels(const CContainer& container) noexcept -> void
{
    const Header& header = container.castAs<Header>();

    auto advanced = header.LabelsNum > 0 ? true : false;

    auto advanced1Size = sizeof(sGMDAdvanced1) * header.LabelsNum;
    auto offset = sizeof(Header)
                  + header.FilenameSize
                  + NULL_TERMINATOR_SIZE
                  + advanced1Size
                  + (advanced ? sizeof(sGMDAdvanced2) : 0);
    auto shift = 0;

    m_labels.reserve(header.LabelsNum);

    for (auto i = 0u; i < header.LabelsNum; i++) {
        const char* cStr = reinterpret_cast<const char*>(container.data() + offset + shift);

        std::string str(cStr);

        shift += str.length() + NULL_TERMINATOR_SIZE;

        m_labels.emplace_back(std::move(str));
    }
}

auto cGMD::readItems(const CContainer &container) noexcept -> void
{
    const Header& header = container.castAs<Header>();

    auto advanced = header.LabelsNum > 0 ? true : false;

    auto advanced1Size = sizeof(sGMDAdvanced1) * header.LabelsNum;
    auto offset = sizeof(Header)
                  + header.FilenameSize
                  + NULL_TERMINATOR_SIZE
                  + advanced1Size
                  + (advanced ? sizeof(sGMDAdvanced2) : 0)
                  + header.LabelsSize;
    auto shift = 0;

    m_items.reserve(header.ItemsNum);

    for (auto i = 0u; i < header.ItemsNum; i++) {
        const char* cStr = reinterpret_cast<const char*>(container.data() + offset + shift);

        std::string str(cStr);

        shift += str.length() + NULL_TERMINATOR_SIZE;

        m_items.emplace_back(std::move(str));
    }
}

auto cGMD::updateHeader(bool updateMagicAndVersion) noexcept -> u32
{
    auto filenameSize = 0;
    auto labelsSize = 0;
    auto itemsSize = 0;
    auto totalSize = 0;

    filenameSize = m_filename.length();

    for (const auto& label : m_labels)
        labelsSize += label.length() + NULL_TERMINATOR_SIZE;

    for (const auto& item : m_items)
        itemsSize += item.length() + NULL_TERMINATOR_SIZE;

    totalSize = sizeof(Header)
                + filenameSize
                + NULL_TERMINATOR_SIZE
                + labelsSize
                + itemsSize;

    if (!m_advanced1.empty()) {
        auto advanced1Size = m_advanced1.size() * sizeof(sGMDAdvanced1);
        auto advanced2Size = sizeof(sGMDAdvanced2);

        totalSize += advanced1Size + advanced2Size;
    }

    m_header.FilenameSize = std::move(filenameSize);
    m_header.LabelsSize = std::move(labelsSize);
    m_header.ItemsSize = std::move(itemsSize);
    m_header.LabelsNum = m_labels.size();
    m_header.ItemsNum = m_items.size();

    if (updateMagicAndVersion) {
        m_header.Magic = Header::MAGIC;
        m_header.Version = Header::VERSION;
    }

    return totalSize;
}

auto cGMD::getItemsNum(void) const noexcept -> u32 { return m_items.size(); }
auto cGMD::getLabelsNum(void) const -> u32 { return m_labels.size(); }

auto cGMD::getLabel(u32 id) -> std::string&
{
    if (id < m_labels.size())
        return m_labels[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cGMD::getLabel(u32 id) const -> const std::string&
{
    if (id < m_labels.size())
        return m_labels[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cGMD::getLabel(u32 id, std::string& str) const noexcept -> bool
{
    if (id < m_labels.size()) {
        str = m_labels[id];
        return true;
    }

    return false;
}

auto cGMD::getItem(u32 id) -> std::string&
{
    if (id < m_items.size())
        return m_items[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cGMD::getItem(u32 id) const -> const std::string&
{
    if (id < m_items.size())
        return m_items[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cGMD::getItem(u32 id, std::string& str) const noexcept -> bool
{
    if (id < m_items.size()) {
        str = m_items[id];
        return true;
    }

    return false;
}

auto cGMD::appendLabel(const std::string &str) noexcept -> void
{
    m_labels.push_back(str);
}

auto cGMD::replaceLabel(u32 id, const std::string &str) noexcept -> bool
{
    if (id < m_labels.size()) {
        m_labels[id] = str;
        return true;
    }

    return false;
}

auto cGMD::removeLabel(u32 id) noexcept -> bool
{
    if (id < m_labels.size()) {
        m_labels.erase(m_labels.begin() + id);
        return true;
    }

    return false;
}

auto cGMD::appendItem(const std::string &str) noexcept -> void
{
    m_items.push_back(str);
}

auto cGMD::replaceItem(u32 id, const std::string &str) noexcept -> bool
{
    if (id < m_items.size()) {
        m_items[id] = str;
        return true;
    }

    return false;
}

auto cGMD::removeItem(u32 id) noexcept -> bool
{
    if (id < m_items.size()) {
        m_items.erase(m_items.begin() + id);
        return true;
    }

    return false;
}

auto cGMD::getGMDHeader(void) noexcept -> Header& { return m_header; }
auto cGMD::getGMDHeader(void) const noexcept -> const Header& { return m_header; }

auto cGMD::getFilename(void) noexcept -> std::string& { return m_filename; }
auto cGMD::getFilename(void) const noexcept -> const std::string& { return m_filename; }
auto cGMD::setFilename(const std::string &str) noexcept -> void { m_filename = str; }

auto cGMD::getAdvanced1(void) noexcept -> std::vector<sGMDAdvanced1>& { return m_advanced1; }
auto cGMD::getAdvanced1(void) const noexcept -> const std::vector<sGMDAdvanced1>& { return m_advanced1; }

auto cGMD::getAdvanced2(void) noexcept -> sGMDAdvanced2& { return m_advanced2; }
auto cGMD::getAdvanced2(void) const noexcept -> const sGMDAdvanced2& { return m_advanced2; }

auto cGMD::getLabels(void) noexcept -> std::vector<std::string>& { return m_labels; }
auto cGMD::getLabels(void) const noexcept -> const std::vector<std::string>& { return m_labels; }

auto cGMD::getItems(void) noexcept -> std::vector<std::string>& { return m_items; }
auto cGMD::getItems(void) const noexcept -> const std::vector<std::string>& { return m_items; }

auto cGMD::getExpectedSize(void) const noexcept -> u32
{
    const Header& header = m_header;
    auto advanced = header.LabelsNum > 0 ? true : false;
    auto expectedSize = sizeof(Header);

    expectedSize += header.FilenameSize + NULL_TERMINATOR_SIZE;
    expectedSize += header.ItemsSize + header.LabelsSize;

    if (advanced)
        expectedSize += header.LabelsNum * sizeof(sGMDAdvanced1) + sizeof(sGMDAdvanced2);

    return expectedSize;
}

}; /// namespase gmd
}; /// namespase mhxx
}; /// namespace mh
