#include "libkorra/MHXX/Extentions/esl.hpp"
#include "libkorra/tools/ccontainer.hpp"
#include "libkorra/tools/magicVersion.h"

namespace mh {
namespace mhxx {
namespace esl {

auto parse(const CContainer &container, cESL &obj) noexcept -> bool
{
    if (container.size() < sizeof(Header::MAGIC) + sizeof(Header::VERSION))
        return false;

    if (!tools::MagicVersion(Header::MAGIC, Header::VERSION)(container.data()))
        return false;

    if (   obj.readHeader(container)
        && obj.readESD(container)) {
        obj.m_valid = true;

        return true;
    }

    return false;
}

auto cESL::isValid(void) const noexcept -> bool { return m_valid; }

auto cESL::getESLHeader(void) noexcept -> Header& { return m_header; }
auto cESL::getESLHeader(void) const noexcept -> const Header& { return m_header; }

auto cESL::getESDVector(void) noexcept -> std::vector<ESD>& { return m_esdVec; }
auto cESL::getESDVector(void) const noexcept -> const std::vector<ESD>& { return m_esdVec; }

auto cESL::readHeader(const CContainer &container) noexcept -> bool
{
    if (container.size() < sizeof(Header))
        return false;

    m_header = container.castAs<Header>();

    return true;
}

auto cESL::readESD(const CContainer &container) -> bool
{
    auto totalESD = 0;

    for (const auto& pESD : m_header.pESD)
        if (pESD != 0)
            ++totalESD;

    m_esdVec.reserve(totalESD);

    for (const auto& offset : m_header.pESD) {
        // Should probably break?
        if (offset == 0)
            continue;

        sESD esData = *reinterpret_cast<sESD*>(container.data() + offset);
        std::vector<sEnemySmall> esdVec;
        auto shift = sizeof(sESD);

        if (   esData.Magic0 != sESD::MAGIC0
            || esData.Magic1 != sESD::MAGIC1)
            continue;

        esdVec.reserve(esData.EnemySmallNum);

        for (auto i = 0u; i < esData.EnemySmallNum; i++) {
            sEnemySmall enemySmall = *reinterpret_cast<sEnemySmall*>(container.data() + offset + shift);

            esdVec.push_back(std::move(enemySmall));

            shift += sizeof(sEnemySmall);
        }

        m_esdVec.emplace_back(std::move(esData), std::move(esdVec));
    }

    return true;
}

}; /// namespase esl
}; /// namespase mhxx
}; /// namespace mh
