#include "libkorra/MHXX/Quest/Common.hpp"
#include <string>
#include "libkorra/function_name.h"

namespace mh {
namespace mhxx {

constexpr const std::array<const char*, 32> sMaps::str = {
    "(None)",
    "J. Frontier (D)",
    "J. Frontier (N)",
    "V. Hills (D)",
    "V. Hills (N)",
    "A. Ridge (D)",
    "A. Ridge (N)",
    "M. Peaks (D)",
    "M. Peaks (N)",
    "Dunes",
    "D. Island",
    "Marshlands",
    "Volcano",
    "Arena",
    "V. Slayground",
    "A. Steppe",
    "V. Hollow",
    "Primal Forest",
    "F. Seaway",
    "F. Slayground",
    "Sanctuary",
    "Forlorn Arena",
    "S. Pinnacle",
    "Ingle Isle",
    "Polar Field",
    "Wyvern's End",
    "Desert",
    "Jungle",
    "Ruined Pinnacle",
    "Castle Schrade",
    "Fortress",
    "Forlorn Citadel"
};

const char* sMaps::getStr(const sMaps::size id) noexcept
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;
    auto choice = 0;

    switch (id) {
    case sMaps::e::J_Frontier_D:
        choice = id;
        break;
    case sMaps::e::J_Frontier_N:
        choice = (id - Special) + 1;
        break;
    case sMaps::e::V_Hills_D:
        choice = id + 1;
        break;
    case sMaps::e::V_Hills_N:
        choice = (id - Special) + 2;
        break;
    case sMaps::e::A_Ridge_D:
        choice = id + 2;
        break;
    case sMaps::e::A_Ridge_N:
        choice = (id - Special) + 3;
        break;
    case sMaps::e::M_Peaks_D:
        choice = id + 3;
        break;
    case sMaps::e::M_Peaks_N:
        choice = (id - Special) + 4;
        break;
    default:
        if (id > sMaps::e::Forlorn_Citadel || id < 0) {
            return err.c_str();
        } else {
            choice = id + 4;
        }
    };

    return sMaps::str[choice];
}

constexpr const std::array<const char*, 4> sEnemyLv::str = {
    "Training",
    "Low Rank",
    "High Rank",
    "G Rank"
};

const char* sEnemyLv::getStr(const size level) noexcept
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    switch (level) {
    default:return err.c_str();
    case e::Training: return sEnemyLv::str.at(0);
    case e::LR: return sEnemyLv::str.at(1);
    case e::HR: return sEnemyLv::str.at(2);
    case e::G: return sEnemyLv::str.at(3);
    };
}

constexpr const std::array<const char*, 17> sQuestLv::str = {
    "Training",
    "1*",
    "2*",
    "3*",
    "4*",
    "5*",
    "6*",
    "7*",
    "8*",
    "9*",
    "10*",
    "G1",
    "G2",
    "G3",
    "G4",
    "G5",
    "EX"
};

const char* sQuestLv::getStr(const sQuestLv::size level) noexcept
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (level < str.size())
        return str[level];
    else if (   level > Special
             && level - Special < str.size())
        return str[level - Special];

    return err.c_str();
}

constexpr const std::array<const char*, 6> sQuestType0::str = {
    "Hunting",
    "Slaying",
    "Capture",
    "Gathering",
    "hunt-a-ton",
    "hunt-a-ton arena"
};

const char* sQuestType0::getStr(const sQuestType0::size type) noexcept
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (type < sQuestType0::e::Hunting || type > sQuestType0::e::dummy1)
        return err.c_str();
    else if (type == sQuestType0::e::dummy0 || type == sQuestType0::e::dummy1)
        return "dummy";

    if (type < str.size())
        return str[type];

    return err.c_str();
}

constexpr const std::array<const char*, 12> sQuestType1::str = {
    "Training/Arena/Dummy",

    "Kokoto (Unlockable)",
    "Pokke (Unlockable",
    "Yukumo (Unlockable",
    "Bherna (Unlockable",

    "Kokoto (Main)",
    "Pokke (Main)",
    "Yukumo (Main)",
    "Bherna (Main)",

    "ProwlerOnly",
    "Default",
    "SpecialPermit"
};

const char* sQuestType1::getStr(const size type) noexcept
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (type < str.size())
        return str[type];

    return err.c_str();
}

constexpr const std::array<const char*, 3> sStartType::str = {
    "Base Camp",
    "Random",
    "Elder Dragon Fight"
};

const char* sStartType::getStr(const size type) noexcept
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (type < str.size())
        return str[type];

    return err.c_str();
}

constexpr const std::array<const char*, 3> sBGMType::str = {
    "Default",
    "Prowler Special",
    "Training"
};

const char* sBGMType::getStr(const size type) noexcept
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (type < str.size())
        return str[type];

    return err.c_str();
}

constexpr const std::array<const char*, 50> sEntryType::str = {
    "None",
    "HR 2 and up",
    "HR 3 and up",
    "HR 4 and up",
    "HR 5 and up",
    "HR 6 and up",
    "HR 7 and up",
    "HR 8 and up",
    "HR 9 and up",
    "HR 10 and up",
    "HR 11 and up",
    "HR 12 and up",
    "HR 13 and up",
    "HR 20 and up",
    "HR 25 and up",
    "HR 30 and up",
    "HR 45 and up",
    "HR 50 and up",
    "HR 60 and up",
    "HR 100 and up",
    "Great Swords only",
    "Long Swords only",
    "Sword & Shields only",
    "Dual Blades only",
    "Lances only",
    "Gunlances only",
    "Hammers only",
    "Hunting Horns only",
    "Switch Axes only",
    "Charge Blades only",
    "Insect Glaives only",
    "Light Bowguns only",
    "Heavy Bowguns only",
    "Bows only",
    "Blademasters only",
    "Gunners only",
    "Guild Style only",
    "Striker Style only",
    "Aerial Style only",
    "Adept Style only",
    "Valor Style only",
    "Alchemy Style only",
    "No Prowlers",
    "Prowlers only",
    "RARE 1 weapons only",
    "No armor/talismans",
    "On-site items only",
    "One player max",
    "Two players max",
    "Three players max"
};

const char* sEntryType::getStr(const size type) noexcept
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (type < str.size())
        return str[type];

    return err.c_str();
}

constexpr const std::array<const char*, 3> sClearType::str = {
    "One target",
    "Two targets",
    "One target and ticket"
};

const char* sClearType::getStr(const size type) noexcept
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (type < str.size())
        return str[type];

    return err.c_str();
}

constexpr const std::array<const char*, 5> sCarvingLv::str = {
    "LR Special",
    "LR",
    "HR",
    "Arena",
    "G"
};

const char* sCarvingLv::getStr(const size level) noexcept
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (level < str.size())
        return str[level];

    return err.c_str();
}

constexpr const std::array<const char*, 8> sGatheringLv::str = {
    "Arena 0",
    "LR",
    "HR",
    "Arena 1",
    "Special 0",
    "Special 1",
    "Special 2",
    "G"
};

const char* sGatheringLv::getStr(const size level) noexcept
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (level < str.size())
        return str[level];
    else if (level == sGatheringLv::e::G)
        return sGatheringLv::str.at(7);

    return err.c_str();
}

constexpr const std::array<const char*, 4> sFishingLv::str = {
    "LR",
    "HR",
    "Arena",
    "G"
};

const char* sFishingLv::getStr(const size level) noexcept
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (level == 0)
        return err.c_str();
    else if (level - 1 < str.size())
        return str[level - 1];

    return err.c_str();
}

auto sGMDLink::getProgNo(void) const noexcept -> u32 { return ProgNo; }
void sGMDLink::setProgNo(const u32 val) noexcept { ProgNo = val; }

auto sGMDLink::getResource(void) const noexcept -> u32 { return Resource; }
void sGMDLink::setResource(const u32 val) noexcept { Resource = val; }

auto sGMDLink::getGMDFileName(void) const noexcept -> std::string { return GMDFileName; }
auto sGMDLink::setGMDFileName(const std::string &val) noexcept -> bool
{
    if (val.empty())
        return false;

    if (val.length() <= STRING_SIZE_MAX) {
        val.copy(GMDFileName, val.length());

        GMDFileName[val.length() + 1] = '\0';

        return true;
    }

    return false;
}

constexpr const std::array<const char*, sIcon::ICONS_MAX> sIcon::str = {
    "(None)",
    "Rathian",
    "Gold Rathian",
    "Dreadqueen",
    "Rathalos",
    "Silver Rathalos",
    "Dreadking",
    "Khezu",
    "Yiak Kut-Ku",
    "Gypceros",
    "Plesioth",
    "Kirin",
    "Velocidrome",
    "Gendrome",
    "Iodrome",
    "Cephadrome",
    "Yian Garuga",
    "Deadeye",
    "Daimyo Hermitaur",
    "Stonefist",
    "Shogun Ceanataur",
    "Blangonga",
    "Rajang",
    "Furious Rajang",
    "Kushala Daora",
    "Chameleos",
    "Teostra",
    "Bulldrome",
    "Tigrex",
    "Grimclaw",
    "Akantor",
    "Lavasioth",
    "Nargacuga",
    "Silverwind",
    "Ukanlos",
    "Deviljho",
    "Savage Deviljho",
    "Uragaan",
    "Crystalbeard",
    "Lagiacrus",
    "Royal Ludroth",
    "Agnaktor",
    "Alatreon",
    "Duramboros",
    "Nibelsnarf",
    "Zinogre",
    "Thunderlord",
    "Amatsu",
    "Arzuros",
    "Redhelm",
    "Lagombi",
    "Snowbaron",
    "Volvidon",
    "Brachydios",
    "Kecha Wacha",
    "Tetsucabra",
    "Drilltusk",
    "Zamtrios",
    "Najarala",
    "Seltas Queen",
    "Gore Magala",
    "Shagaru Magala",
    "Seltas",
    "Seregios",
    "Malfestio",
    "Glavenus",
    "Hellblade",
    "Astalos",
    "Mizutsune",
    "Gammoth",
    "Nakarkos",
    "Great Maccao",
    "Apnototh",
    "Apceros",
    "Kelbi",
    "Mosswine",
    "Hornetaur",
    "Vespoid ",
    "Felyne",
    "Melynx",
    "Velociprey",
    "Genprey",
    "Ioprey",
    "Cephalos",
    "Bullfango",
    "Popo",
    "Giaprey",
    "Anteka",
    "Remobra",
    "Hermitaur",
    "Ceanataur",
    "Blango",
    "Rhenoplos",
    "Bnahabra",
    "Altaroth",
    "Jaggi",
    "Jaggia",
    "Ludroth",
    "Uroktor",
    "Slagtoth",
    "Gargwa",
    "Zamite",
    "Konchu",
    "Maccao",
    "Larinoth",
    "Moofah",
    "Danger",
    "Cross Symbol",
    "Palico",
    "Egg",
    "Rocks",
    "Fish",
    "Bones",
    "Insect",
    "Mushroom",
    "Accounting",
    "Harvest Tour",
    "Box",
    "Nakarkos Head",
    "Nakarkos Tail",
    "Basarios",
    "Gravios",
    "Diablos",
    "Lao-Shan Lung",
    "Congalala",
    "Giadrome",
    "Barioth",
    "Barroth",
    "Raging Brachydios",
    "Nerscylla",
    "Chaotic Gore Magala",
    "Conga",
    "Great Thunderbug",
    "Bloodbath",
    "Boltreaver",
    "Elderfrost",
    "Soulseer",
    "Rustrazor",
    "Nightcloak",
    "Ahtal-Ka Mecha",
    "Ahtal-Ka",
    "Valstrax",
    "None (Hyper)",
    "Rathian (Hyper)",
    "Gold Rathian (Hyper)",
    "Rathalos (Hyper)",
    "Silver Rathalos (Hyper)",
    "Khezu (Hyper)",
    "Basarios (Hyper)",
    "Gravios (Hyper)",
    "Diablos (Hyper)",
    "Yian Kut-Ku (Hyper)",
    "Gypceros (Hyper)",
    "Plesioth (Hyper)",
    "Yian Garuga (Hyper)",
    "Daimyo Hermitaur (Hyper)",
    "Shogun Ceanataur (Hyper)",
    "Congalala (Hyper)",
    "Blangonga (Hyper)",
    "Rajang (Hyper)",
    "Tigrex (Hyper)",
    "Lavasioth (Hyper)",
    "Nargacuga (Hyper)",
    "Barioth (Hyper)",
    "Deviljho (Hyper)",
    "Barroth (Hyper)",
    "Uragaan (Hyper)",
    "Lagiacrus (Hyper)",
    "Royal Ludroth (Hyper)",
    "Agnaktor (Hyper)",
    "Duramboros (Hyper)",
    "Nibelsnarf (Hyper)",
    "Zinogre (Hyper)",
    "Brachydios (Hyper)",
    "Kecha Wacha (Hyper)",
    "Tetsucabra (Hyper)",
    "Zamtrios (Hyper)",
    "Najarala (Hyper)",
    "Seltas Queen (Hyper)",
    "Nerscylla (Hyper)",
    "Gore Magala (Hyper)",
    "Seregios (Hyper)",
    "Malfestio (Hyper)",
    "Glavenus (Hyper)",
    "Astalos (Hyper)",
    "Mizutsune (Hyper)",
    "Gammoth (Hyper)"
};

const char* sIcon::getStr(const size id) noexcept
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id];

    return err.c_str();
}

}; /// namespace mhxx
}; /// namespace mh
