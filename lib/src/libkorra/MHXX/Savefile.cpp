#include "libkorra/MHXX/Savefile.hpp"
#include "libkorra/tools/tools.hpp"
#include "libkorra/tools/ccontainer.hpp"

namespace mh {
namespace mhxx {

Savefile::Savefile(const CContainer& container)
{
    if (container.size() == SaveFile_s::VALID_SAVEFILE_SIZE) {
        tools::copyBytes(&this->savefile, container.data(), sizeof(SaveFile_s));
    }
    else return; // notify error
}

void Savefile::print_Flags(void) const
{
}

void SaveSlot::extract(CContainer& container) const
{
    container.resize(SaveSlot::SLOT_SIZE);

    tools::copyBytes(container.data(), this, SaveSlot::SLOT_SIZE);
}

}; /// namespace mhxx
}; /// namespace mh
