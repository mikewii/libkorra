#include "libkorra/tools/tools.hpp"
#include "libkorra/tools/ccontainer.hpp"

#include <iostream>
#include <string.h>
#include <algorithm>
#include <boost/algorithm/string.hpp>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

#include "libkorra/MHXX/Quest/Common.hpp"

namespace mh {
namespace tools {

namespace fs = boost::filesystem;

void *copyBytes(void *dest,
                const void *src,
                const size_t size)
{
    if ((reinterpret_cast<u64>(dest) % sizeof(dest)) == 0)
        return memcpy(dest, src, size);

    return memmove(dest, src, size);
}

extern void* Copy_UTF16_String(void* dest, const std::u16string& str)
{
    return copyBytes(dest, reinterpret_cast<const void*>(str.data()), str.size() * sizeof(u16));
}

u32 calculate_checksum(const CContainer& cc)
{
    u32 i = 0, checksum = 0;

    while (i < cc.size()) {
        checksum += cc.castAt<u8>(i) & 0xFF; i++;
    };

    return checksum;
}

u32 CalculateChecksum(u8* _data, u32 _size)
{
    u32 i = 0, checksum = 0;

    while (i < _size) {
        checksum += _data[i] & 0xFF; i++;
    }

    return checksum;
}

std::pair<u8*, u8*> FindDiff(u8* _data0, u8* _data1, u32 _size)
{
    u32 i = 0, sum0 = 0, sum1 = 0;
    u8* p0 = nullptr;
    u8* p1 = nullptr;

    while (i < _size)
    {
        sum0 += _data0[i] & 0xFF;
        sum1 += _data1[i] & 0xFF;

        if (sum0 != sum1)
        {
            p0 = &_data0[i];
            p1 = &_data1[i];

            break;
        }

        i++;
    }

    return {p0, p1};
}


#ifndef N3DS
void Collector::set_filter_value(const u32 value)
{
    Collector::m_filter_value = value;
}

void Collector::set_filter_string(const std::string &str)
{
    this->m_flter_str = str;
}

void Collector::set_output_path(const std::string &path)
{
    m_output_path = path;
}

void Collector::Add(const Collector::Info& in)
{
    if (!Collector::IsActive()) return;

    switch(Collector::m_filtering_operation){
    default:break;
    case Collector::Op::Equal:          { if (in.value == Collector::m_filter_value) Collector::m_info_vec.push_back(in); break; }
    case Collector::Op::NotEqual:       { if (in.value != Collector::m_filter_value) Collector::m_info_vec.push_back(in); break; }
    case Collector::Op::Less:           { if (in.value <  Collector::m_filter_value) Collector::m_info_vec.push_back(in); break; }
    case Collector::Op::Greater:        { if (in.value >  Collector::m_filter_value) Collector::m_info_vec.push_back(in); break; }
    case Collector::Op::LessEqual:      { if (in.value <= Collector::m_filter_value) Collector::m_info_vec.push_back(in); break; }
    case Collector::Op::GreaterEqual:   { if (in.value >= Collector::m_filter_value) Collector::m_info_vec.push_back(in); break; }
    case Collector::Op::BitPresent:     { if (in.value & Collector::m_filter_value)  Collector::m_info_vec.push_back(in); break; }
    case Collector::Op::Unique:
    {
        if (!Collector::m_info_vec.empty())
            for (const auto& item : Collector::m_info_vec)
                if (in.value == item.value) return;

        Collector::m_info_vec.push_back(in);
        break;
    }
    case Collector::Op::Collect:
    {
        Collector::m_info_vec.push_back(in);

        if (!Collector::m_vec_unique_ids.empty())
            for (const auto& value : Collector::m_vec_unique_ids)
                if (in.value == value) return;

        Collector::m_vec_unique_ids.push_back(in.value);
    }
    }; // switch
}

void Collector::Show(const bool sorted)
{
    if (!this->IsActive() || this->m_info_vec.empty()) return;

    this->prepare_str_formats();

    if (this->m_filtering_operation == Collector::Op::Collect)
    {
        if (this->m_output_path.empty())
        {
        } else {
            if (this->Flush())
                printf("Collector:: data saved as %s at path %s\n", this->m_output_filename.c_str(), this->m_output_path.c_str());
            else printf("Collector:: Error happened trying to write file at path %s\n", this->m_output_path.c_str());
        }
    }

    if (sorted)
        std::sort
        (
            this->m_info_vec.begin(),
            this->m_info_vec.end(),
            [](const Collector::Info& a, const Collector::Info& b){ return a.value < b.value; }
        );

//    for (auto& info : this->m_info_vec)
//        fmt::print(this->get_formatted_string(info));
}

bool Collector::Flush(void)
{
    if (!Collector::IsActive()) return false;

    fs::fstream fout(Collector::m_output_path.append(Collector::m_output_filename), std::ios::out);


    std::sort
    (
        Collector::m_info_vec.begin(),
        Collector::m_info_vec.end(),
        [](const Collector::Info& a, const Collector::Info& b){ return a.quest_id < b.quest_id; }
    );

    std::sort
    (
        Collector::m_vec_unique_ids.begin(),
        Collector::m_vec_unique_ids.end(),
        [](const s32& a, const s32& b){ return a < b; }
    );

    if (!fout.is_open()) return false;

    for (u32 i = 0; i < Collector::m_vec_unique_ids.size(); i++)
    {
        const auto& token = Collector::m_vec_unique_ids.at(i);

        for (auto& info : Collector::m_info_vec)
            if (info.value == token) // format and write to file
            {
                auto str = this->get_formatted_string(info);

                fout.write(str.data(), str.size());
            }
        // add 3 new lines
        if (i != (Collector::m_vec_unique_ids.size() - 1))
            for (auto i = 0; i < 3; i++)
                fout << std::endl;
    }

    fout.close();
    return true;
}

void Collector::prepare_str_formats(const bool counter)
{
    u32 count = 0;
    std::vector<u32> str_length;

    // find longest string for vector
    for (size_t i = 0; i < this->m_info_vec.at(0).strings.size(); i++) {
        str_length.push_back(1);

        for (const auto& info : this->m_info_vec)
            if (info.strings.at(i).size() > str_length.at(i))
                str_length.at(i) = info.strings.at(i).size();
    }

//    if (counter) {
//        for (const auto& length : str_length)
//            this->m_str_formats.push_back(fmt::format(" [{0:02}] \"{{:<{1}}}\"", ++count, length));
//    } else {
//        for (const auto& length : str_length)
//            this->m_str_formats.push_back(fmt::format(" \"{{:<{0}}}\"", length));
//    }
}

std::string Collector::get_formatted_string(Collector::Info &info)
{
    constexpr const char* format0 = "q{:07} [{:>3}] ({:>4}|0x{:<2X}) |";
    std::string formatted;

//    formatted += fmt::format(format0, info.quest_id, MHXX::sQuestLv::getStr(info.quest_evel), info.value, info.value);

//    for (size_t i = 0; i < info.strings.size(); i++) {
//        boost::algorithm::replace_all(info.strings.at(i), "\n", " ");
//        formatted += fmt::format(m_str_formats.at(i), info.strings.at(i));
//    }

//    formatted += fmt::format("\n");

    return formatted;
}

void Collector::clear(void)
{
    this->m_flter_str.clear();
    this->m_output_path.clear();
    this->m_info_vec.clear();
    this->m_vec_unique_ids.clear();
    this->m_str_formats.clear();
}

bool Filter::Is_InVector(const std::string &path, const std::vector<std::string> &vector)
{
    fs::path fsPath{path};

    if (!vector.empty())
    {
        bool found = false;

        for (auto& str : vector)
            if (str.compare(fsPath.filename().string()) == 0)
            {
                found = true;
                break;
            }

        if (!found) return false;;
    }

    return true;
}

#endif

}; /// namespace tools
}; /// namespace mh
