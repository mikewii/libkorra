#include "libkorra/tools/pair.hpp"
#include "libkorra/tools/tools.hpp"

namespace mh {
namespace tools {

void PairInfoKeeper::Set_PairInfo(const Pair& _pp)
{
    tools::copyBytes(&m_info.Filename, _pp.info.Filename, FNAME_SIZE);

    m_info.ResourceHash   = _pp.info.ResourceHash;
    m_info.XORLock        = _pp.info.XORLock;
    m_info.DecSize        = _pp.info.DecSize;
    m_info.isDecompressed = _pp.info.isDecompressed;

    m_isSet = true;
}

bool PairInfoKeeper::GetPairInfo(Pair &_pp)
{
    if (_pp.cc.size() <= 0)
        return false;

    tools::copyBytes(_pp.info.Filename, m_info.Filename, FNAME_SIZE);

    _pp.info.ResourceHash    = m_info.ResourceHash;
    _pp.info.XORLock         = m_info.XORLock;
    _pp.info.DecSize         = m_info.DecSize;
    _pp.info.isDecompressed  = m_info.isDecompressed;

    return true;
}

void PairInfo::print(void) const
{
    printf("\n##### Pair info #####\n");
    printf("Filename:       %s\n", Filename);
    printf("ResourceHash:   0x%08X\n", ResourceHash);
    printf("XORLock:        0x%08X\n", XORLock);
    printf("DecSize:        %d\n", DecSize);
    printf("isDecompressed: %s\n", isDecompressed ? "true" : "false");
}

void PairInfo::print_Filename(void) const
{
    printf("Filename:       %s\n", Filename);
}

}; /// namespace tools
}; /// namespace mh
