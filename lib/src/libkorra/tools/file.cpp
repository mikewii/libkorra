#include "libkorra/tools/file.hpp"

namespace mh {
namespace tools {

bool File::file_to_cc(const fs::path& path, CContainer& cc, const u32 magic)
{
    fs::path prefered(path);

    prefered = prefered.make_preferred();

    fs::fstream file(prefered, std::ios::binary | std::ios::in);
    const auto fsize = fs::file_size(prefered);
    bool proceed = true;

    if (file.is_open()) {
        if (magic)
            proceed = File::Probe(file, magic);

        if (proceed) {
            cc.resize(fsize);

            file.read(&cc.castAs<char>(0), fsize);
        }

        return proceed;
    }

    return false;
}

bool File::file_to_cc_size(const fs::path &path, CContainer &cc, const u32 size)
{

    fs::path prefered(path); prefered = prefered.make_preferred();
    fs::fstream file(prefered, std::ios::binary | std::ios::in);
    const auto fsize = fs::file_size(prefered);

    if (file.is_open() && fsize >= size) {
        file.read(&cc.castAs<char>(0), size);

        return true;
    }

    return false;
}

void File::CC_To_File(const fs::path& path, const CContainer& cc, const bool flush)
{
    fs::path prefered(path); prefered = prefered.make_preferred();

    fs::fstream file(prefered, std::ios::binary | std::ios::out);

    if (file.is_open()) {
        file.write(&cc.castAs<char>(0), cc.size());

        if (flush)
            file.flush();
    }
}

void File::Data_To_File(const fs::path& path, const void* data, const int size, const bool flush)
{
    fs::path prefered(path); prefered = prefered.make_preferred();

    fs::fstream file(prefered, std::ios::binary | std::ios::out);

    const char* data0 = reinterpret_cast<const char*>(data);
    if (file.is_open()) {
        file.write(data0, size);

        if (flush)
            file.flush();
    }
}

bool File::Probe(fs::fstream& file, const u32 magic)
{
    u32 buffer = 0;

    file.read(reinterpret_cast<char*>(&buffer), 4);
    file.seekg(0);

    return buffer == magic;
}

}; /// namespace tools
}; /// namespace mh
