#include "libkorra/tools/MCA.hpp"
#include "libkorra/tools/ccontainer.hpp"

namespace mh {
namespace tools {

MCA::MCA()
{
}

MCA::MCA(const CContainer &_cc)
{
    if (_cc.castAt<u32>(0) != MAGIC) {
//        NotifyError("Not an MCA");
        return;
    }

    this->h = _cc.castAt<MCA::header>(0);
}

MCA::~MCA()
{

}

void MCA::print(void) const
{
    printf("\n##### MCA Header #####\n");

    printf("Magic:                  0x%08X\n", h.Magic);
    printf("Version:                0x%08X | %d\n", h.Version, h.Version);

    printf("Channel_count:          0x%04X | %d\n", h.Channel_count, h.Channel_count);
    printf("Interleave_block_size:  0x%04X | %d\n", h.Interleave_block_size, h.Interleave_block_size);
    printf("Num_samples:            0x%08X | %d\n", h.Num_samples, h.Num_samples);
    printf("Sample_rate:            0x%08X | %d\n", h.Sample_rate, h.Sample_rate);
    printf("Loop_start_sample:      0x%08X | %d\n", h.Loop_start_sample, h.Loop_start_sample);
    printf("Loop_end_sample:        0x%08X | %d\n", h.Loop_end_sample, h.Loop_end_sample);

    printf("Header_size:            0x%08X | %d\n", h.Header_size, h.Header_size);
    printf("Data_size:              0x%08X | %d\n", h.Data_size, h.Data_size);
    printf("Duration_seconds:       0x%08X | %f\n", *reinterpret_cast<const u32*>( &h.Duration_seconds ), this->h.Duration_seconds);

    printf("Coef_shift:             0x%08X | %d\n", h.Header_size, h.Header_size);
    printf("unk0:                   0x%08X | %d\n", h.Data_size, h.Data_size);
    printf("Coef_spacing:           0x%08X | %d\n", h.Coef_spacing, h.Coef_spacing);

}

void MCA::Fix_LoopEndSample(void) {
    if (h.Loop_end_sample > h.Num_samples)
        h.Loop_end_sample = h.Num_samples;
}

}; /// namespace tools
}; /// namespace mh
