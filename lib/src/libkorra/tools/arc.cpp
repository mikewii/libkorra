#include "libkorra/tools/arc.hpp"

#include "libkorra/attributes.h"
#include "libkorra/tools/tools.hpp"

#include <cassert>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <sstream>
#include <iomanip>

#include <iostream>




#include "libkorra/tools/magicVersion.h"

#include <numeric>
#include <mutex>
#include <deque>
#include <future>


#define ONE_THREAD
#define ARC_CHUNK 16384

namespace mh {
namespace tools {

static constexpr u32 getPadding(const ARCVersion version) noexcept
{
    switch (version) {
    case ARCVersion::LP1: return 0;
    case ARCVersion::LP2: return 0;
    case ARCVersion::MH3U_3DS: return 4;
    case ARCVersion::MH4U_MHXX: return 4;
    case ARCVersion::MH4U_1: return 4;
    default:return 0;
    };

    return 0;
}

USED static constexpr u32 extract_XORLock(const u32 decSize)
{
    // lock will be first 4 bits
    return decSize & 0xF0000000;
}

static constexpr u32 align(u32 value)
{
    constexpr const u32 alignment = sizeof(u64) * 2; // file alignment

    while (value % alignment != 0)
        value += 4;

    return value;
}

auto ARC::checkMagic(const void* data) const noexcept -> bool
{
    const ARCHeader* header = static_cast<const ARCHeader*>(data);

    return header->isARC() || header->isCRA();
}

auto ARC::checkVersion(const void* data) const noexcept -> bool
{
    const ARCHeader* header = static_cast<const ARCHeader*>(data);
    auto version = header->Version;

    if (header->isCRA())
        version = tools::endianess::swap<ARCVersion>(version);

    switch (version) {
    default:return false;
    case ARCVersion::LP1:
    case ARCVersion::LP2:
    case ARCVersion::MH3U_3DS:
    case ARCVersion::MH4U_MHXX:
    case ARCVersion::MH4U_1:
        return true;
    };

    return false;
}

auto ARC::checkARCHeaderSize(const std::size_t size) const noexcept -> bool
{
    return size >= sizeof(ARCHeader);
}

auto ARC::checkARCFileHeadersSize(const std::size_t size) const noexcept -> bool
{
    auto expectedSize =   sizeof(ARCHeader)
                        + m_padding
                        + m_header.FilesNum * sizeof(ARCFileHeader);

    return size >= expectedSize;
}

auto ARC::checkZLIBDataSize(const std::size_t size) const noexcept -> bool
{
    auto expectedSize =   sizeof(ARCHeader)
                        + m_padding
                        + m_header.FilesNum * sizeof(ARCFileHeader);

    for (const auto& header : m_arcFileHeaders)
        expectedSize += header.CompressedSize;

    return size >= expectedSize;
}

auto ARC::readHeader(const void* data) noexcept -> bool
{
    if (   !checkMagic(data)
        || !checkVersion(data))
        return false;

    m_header = *static_cast<const ARCHeader*>(data);

    if (m_header.isCRA())
        m_header.BESwap();

    return true;
}

auto ARC::readARCFileHeaders(const void *data) noexcept -> void
{
    auto shift = sizeof(ARCHeader) + m_padding;
    const char* c = static_cast<const char*>(data);

    m_arcFileHeaders.reserve(m_header.FilesNum);

    for (auto i = 0u; i < m_header.FilesNum; i++) {
        auto offset = shift + sizeof(ARCFileHeader) * i;

        const ARCFileHeader* header = reinterpret_cast<const ARCFileHeader*>(c + offset);

        m_arcFileHeaders.push_back(*header);
    }

    if (m_header.isCRA())
        toLittleEndian();
}

ARC::ARC(CContainer&& container)
    : m_container{container}
{
    m_valid = read(container);
}

auto ARC::isValid(void) const noexcept -> bool { return m_valid; }

auto ARC::readZLIBData(void) noexcept -> bool { return readZLIBData(m_container); }

auto ARC::readZLIBData(const CContainer &container) noexcept -> bool
{
    if (!checkZLIBDataSize(container.size()))
        return false;

    readZLIBData(container.data());

    return true;
}

auto ARC::readZLIBData(const void *data) noexcept -> void
{
    m_zlibData.reserve(m_arcFileHeaders.size());

    for (const auto& header : m_arcFileHeaders) {
        CContainer container;

        container.resize(header.CompressedSize);

        tools::copyBytes(  container.data()
                         , static_cast<const char*>(data) + header.pZData
                         , header.CompressedSize);

        m_zlibData.push_back(std::move(container));
    }
}

auto ARC::read(const CContainer &container) noexcept -> bool
{
    if (   !checkARCHeaderSize(container.size())
        || !readHeader(container.data()))
        return false;

    m_padding = getPadding(m_header.Version);

    if (!checkARCFileHeadersSize(container.size()))
        return false;

    readARCFileHeaders(container.data());

    return true;
}

auto ARC::uncompress(const std::size_t id,
                     const void* data,
                     CContainer &output) noexcept -> bool
{
    if (id >= m_arcFileHeaders.size())
        return false;

    if (!data)
        return false;

    const ARCFileHeader& header = m_arcFileHeaders[id];
    const Bytef* source = nullptr;
    uLongf uncompressedSize = 0;

    uncompressedSize = header.UncompressedSize;
    source = static_cast<const Bytef*>(data) + header.pZData;

    output.resize(header.UncompressedSize, true);

    auto res = ::uncompress(  output.data()
                            , &uncompressedSize
                            , source
                            , header.CompressedSize);

    if (res == Z_OK)
        return true;

    return false;
}

auto ARC::uncompress(const ARCFileHeader &header,
                     const CContainer &zlibData,
                     CContainer &output) noexcept -> bool
{
    const Bytef* source = nullptr;
    uLongf uncompressedSize = 0;

    uncompressedSize = header.UncompressedSize;
    source = static_cast<const Bytef*>(zlibData.data());

    output.resize(header.UncompressedSize, true);

    auto res = ::uncompress(  output.data()
                            , &uncompressedSize
                            , source
                            , header.CompressedSize);

    if (res == Z_OK)
        return true;

    return false;
}

auto ARC::uncompressAll(std::vector<CContainer> &output) -> bool
{
    if (m_arcFileHeaders.size() != m_zlibData.size())
        return false;

    bool good = true;

    output.resize(m_arcFileHeaders.size());

    for (auto i = 0u; i < m_arcFileHeaders.size(); i++)
        good &= uncompress(  m_arcFileHeaders[i]
                           , m_zlibData[i]
                           , output[i]);

    return good;
}

auto ARC::uncompressAll_async(std::vector<CContainer>& output,
                              const std::size_t threadNum) -> bool
{
    if (m_arcFileHeaders.size() != m_zlibData.size())
        return false;

    auto uncompress_async = [this](std::vector<CContainer>& output,
                                   std::deque<std::size_t>& deque,
                                   std::mutex& dequeMutex) noexcept -> bool
    {
        bool good = true;
        auto id = 0;

        while (true) {
            {
                std::lock_guard<std::mutex> lock(dequeMutex);

                if (deque.empty())
                    break;

                id = deque.front();
                deque.pop_front();
            }

            good &= uncompress(  m_arcFileHeaders[id]
                               , m_zlibData[id]
                               , output[id]);
        }

        return good;
    };

    std::size_t threads = threadNum == 0 ? std::thread::hardware_concurrency() : threadNum;
    std::vector<std::future<bool>> futures;
    std::deque<std::size_t> deque(m_arcFileHeaders.size());
    std::mutex dequeMutex;
    bool good = true;

    output.resize(m_arcFileHeaders.size());

    std::iota(  deque.begin()
              , deque.end()
              , 0);

    for (auto i = 0u; i < threads; i++)
        futures.emplace_back(std::async(  std::launch::async
                                        , uncompress_async
                                        , std::ref(output)
                                        , std::ref(deque)
                                        , std::ref(dequeMutex)));

    for (auto& f : futures)
        good &= f.get();

    return good;
}

auto ARC::clear(void) noexcept -> void
{
    m_out_path.clear();
    m_arc_path.clear();

    m_file_headers.clear();
    m_file_header_locks.clear();

}

void ARC::make_arc(const std::string &path)
{
    make_header(path);

    m_out_file.open(m_out_file_path, fs::fstream::ios_base::binary | fs::fstream::ios_base::out);

    if (!m_out_file.is_open()) {
        return;
    }

    prepare_write_header();

    for (size_t i = 0; i < m_file_out_headers.size(); i++) {
        auto& file_header = m_file_out_headers.at(i);
        const auto& input_file_path = m_file_out_paths.at(i);

        compress(file_header, input_file_path);
    }

    write_header(path);
}

void ARC::make_header(const std::string &path)
{
    if (fs::exists(path) && fs::is_directory(path)) {
        boost::system::error_code ec;

// TODO: fix
#if BOOST_VERSION == 106000
        for (const auto& entry : fs::recursive_directory_iterator(path, ec)) {
#else
        for (const auto& entry : fs::recursive_directory_iterator(path, fs::directory_options::skip_permission_denied, ec)) {
#endif
            if (ec) {
                continue;
            }

            if (!fs::is_regular_file(entry))
                continue;

            ARCFileHeader    header;
            fs::path            relative_path = fs::relative(entry.path(), path);
            std::string         relative_path_str = relative_path.string();
            std::string         resource_hash_str = extract_resource_hash(relative_path_str);

            // TODO: think on better way
            if (!resource_hash_str.empty()) {
                union Mask {
                    u32 value_u32;
                    struct {
                        u8 value_u8[4];
                    };
                };

                Mask mask{0};

                for (size_t i = 0, j = 0; i < resource_hash_str.size(); i += 2, j++) {
                    const auto& chars = resource_hash_str.substr(i, 2);

                    mask.value_u8[j] = strtoul(chars.c_str(), NULL, 16);
                }

                header.ResourceHash = mask.value_u32;
            }

            header.setFilePath(relative_path_str);
            header.fixFilePath();

            header.UncompressedSize = fs::file_size(entry.path());;
            header.Flags = 2; // ?

            m_file_out_headers.push_back(header);
            m_file_out_paths.push_back(entry.path());
        }
    } else {
        return;
    }
}

void ARC::prepare_write_header(void)
{
    u32 header_size = sizeof(ARCHeader) + 0 /*padding*/ + (sizeof(ARCFileHeader) * m_file_out_headers.size());
    auto aligned_size = align(header_size);

    m_out_file.seekp(aligned_size);
    auto pos = m_out_file.tellp();

    return;
}

void ARC::write_header(const std::string &path)
{
    m_out_file.seekp(0 + sizeof(ARCHeader) + 0);

    for (const auto& header : m_file_out_headers) {
        m_out_file.write(reinterpret_cast<const char*>(&header), sizeof(ARCFileHeader));
    }
}

int ARC::decompress(const ARCFileHeader &file_header, const fs::path& dir)
{
    namespace fs = boost::filesystem;

    u32 CHUNK = file_header.CompressedSize < ARC_CHUNK ? file_header.CompressedSize : ARC_CHUNK;
    int res;
    unsigned have;
    z_stream z_strm;
    unsigned char in[CHUNK];
    unsigned char out[CHUNK];
    fs::path dirs;
    fs::path filename;
    fs::path file_out_path;
    fs::fstream file_in;
    fs::fstream file_out;

    /* allocate inflate state */
    z_strm.zalloc = Z_NULL;
    z_strm.zfree = Z_NULL;
    z_strm.opaque = Z_NULL;
    z_strm.avail_in = 0;
    z_strm.next_in = Z_NULL;


    res = inflateInit(&z_strm);

    if (res != Z_OK) {
        return res;
    }

    // output folder and filename handling
    {
        if (fs::exists(dir) && fs::is_directory(dir)) {
            file_out_path = dir;
            file_out_path = file_out_path.make_preferred();

            filename = file_header.getFilePathUnix();
            filename = filename.filename();
        } else {
            dirs = this->create_root_folder();

            dirs.append(file_header.getFilePathUnix());

            filename = dirs.filename();

            dirs.remove_filename();

            file_out_path = dirs;

            fs::create_directories(dirs);
        }

        filename += ".(" + tools::convert::to_string<u32>(file_header.ResourceHash, true) + ")";

        file_out_path.append(filename.string());

        if (fs::exists(file_out_path)) {
            u32 count = 0;

            while (fs::exists(file_out_path.string() + tools::convert::to_string<u32>(++count))) {
                continue;
            }

            file_out_path += tools::convert::to_string<u32>(++count);
        }
    }

    file_in.open(m_arc_path, fs::fstream::ios_base::binary | fs::fstream::ios_base::in);
    file_out.open(file_out_path, fs::fstream::ios_base::binary | fs::fstream::ios_base::out);

    if (!file_in.is_open() || !file_out.is_open()) {
        return Z_ERRNO;
    } else {
        file_in.seekg(file_header.pZData);
    }

    do {
        file_in.read(reinterpret_cast<char*>(in), CHUNK);
        z_strm.avail_in = file_in.gcount();
        if (z_strm.avail_in == 0)
            break;
        z_strm.next_in = in;

        do {
            z_strm.avail_out = CHUNK;
            z_strm.next_out = out;
            res = inflate(&z_strm, Z_NO_FLUSH);
            assert(res != Z_STREAM_ERROR);  /* state not clobbered */
            switch (res) {
            case Z_NEED_DICT:
                res = Z_DATA_ERROR;     /* and fall through */
            case Z_DATA_ERROR:
            case Z_MEM_ERROR:
                (void)inflateEnd(&z_strm);
                return res;
            }

            have = CHUNK - z_strm.avail_out;
            file_out.write(reinterpret_cast<const char*>(out), have);
        } while (z_strm.avail_out == 0);


    } while (res != Z_STREAM_END);

    //if (z_strm.total_out != file_header.UncompressedSize)

    /* clean up and return */
    (void)inflateEnd(&z_strm);
    return res == Z_STREAM_END ? Z_OK : Z_DATA_ERROR;
}

void ARC::decompress_t(void)
{
    while (!m_file_headers_index_queue.empty()) {
        m_mutex.lock();
        auto id = m_file_headers_index_queue.front();
        m_file_headers_index_queue.pop();
        m_mutex.unlock();

        auto res = decompress(m_file_headers.at(id));

        m_mutex.lock();
        m_res.at(id) = res;
        m_mutex.unlock();
    }
}

// compress require prepared ARC::File_header and arc file to write to
int ARC::compress(ARCFileHeader &file_header, const fs::path& input_file_path, const int level)
{
    u32 CHUNK = file_header.UncompressedSize < ARC_CHUNK ? file_header.UncompressedSize : ARC_CHUNK;
    int ret, flush;
    unsigned have;
    z_stream z_strm;
    unsigned char in[CHUNK];
    unsigned char out[CHUNK];
    fs::fstream file_in;
    fs::fstream file_out;

    /* allocate deflate state */
    z_strm.zalloc = Z_NULL;
    z_strm.zfree = Z_NULL;
    z_strm.opaque = Z_NULL;

    ret = deflateInit(&z_strm, level);

    if (ret != Z_OK)
        return ret;

    file_in.open(input_file_path, fs::fstream::ios_base::binary | fs::fstream::ios_base::in);
    file_header.pZData = m_out_file.tellp();

    /* compress until end of file */
    do {
        file_in.read(reinterpret_cast<char*>(in), CHUNK);
        z_strm.avail_in = file_in.gcount();

        if (z_strm.avail_in == 0) {
            (void)deflateEnd(&z_strm);
            return Z_ERRNO;
        }

        flush = file_in.eof() ? Z_FINISH : Z_NO_FLUSH;
        z_strm.next_in = in;

        /* run deflate() on input until output buffer not full, finish
           compression if all of source has been read in */
        do {
            z_strm.avail_out = CHUNK;
            z_strm.next_out = out;
            ret = deflate(&z_strm, flush);    /* no bad return value */
            assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
            have = CHUNK - z_strm.avail_out;

            m_out_file.write(reinterpret_cast<const char*>(out), have);
        } while (z_strm.avail_out == 0);
        assert(z_strm.avail_in == 0);     /* all input will be used */

        /* done when last data in file processed */
    } while (flush != Z_FINISH);
    assert(ret == Z_STREAM_END);        /* stream will be complete */

    file_header.CompressedSize = z_strm.total_out;

    /* clean up and return */
    (void)deflateEnd(&z_strm);
    return Z_OK;
}

void ARC::print_Header(void)
{
    printf("Magic:          %s\n", ARC::m_header.Magic);
    printf("Version:        %hu\n", ARC::m_header.Version);
    printf("Num of Files:   %d\n", ARC::m_header.FilesNum);
}

void ARC::print_FileInfo(ARCFileHeader* f, u32 n)
{
    printf("\n");
    printf("File #%d\n", n);
    printf("Filepath:           %s\n",   f->FilePath);
    printf("Resource hash:      0x%X\n", f->ResourceHash);
    printf("Compressed size:    0x%X\n", f->CompressedSize);
    printf("Decompressed size:  0x%X\n", f->UncompressedSize);
    printf("Pointer to zdata:   0x%X\n", f->pZData);
}

void ARC::print_PairsInfo(void)
{
    for (const auto& p : *m_pairVec)
        p.info.print();
}

void ARC::Read(const CContainer& container)
{
//    auto padding = 0;

//    if (this->trust)
//        padding = this->Is_NeedPadding(this->m_header.version);
//    else padding = container.as_const_ref<u32>(2) == 0 ? 4 : 0;

//    const auto& file_Header = container.at<ARC::File_Header[]>(sizeof(ARC::Header) + padding);

//    this->__pairVec->resize(this->m_header.files_ammount);

//    for (auto i = 0; i < this->m_header.files_ammount; i++)
//    {
//        auto xorLock = this->extract_XORLock(file_Header[i].decompressed_size);
//        auto file_header_normalized = file_Header[i];


//        if (this->m_is_big_endian) file_header_normalized.BE_Swap();

//        file_header_normalized.decompressed_size &= ~xorLock;

//        this->m_file_headers.push_back(file_header_normalized);

//        // pair:
//        auto& pair = this->__pairVec->at(i);
//        Utils::copybytes(pair.info.Filename, &file_header_normalized.file_name, FNAME_SIZE);
//        pair.info.ResourceHash  = file_header_normalized.resource_hash;
//        pair.info.XORLock       = xorLock;
    //    }
}

const fs::path ARC::create_root_folder(void)
{
    namespace fs = boost::filesystem;

    fs::path filename;
    fs::path root = m_arc_path;

    root.make_preferred();

    filename = root.filename();

    root.remove_filename();
    root.append(filename.string() + ".dearc");

    if (!fs::exists(root))
        fs::create_directory(root);

    if (this->m_out_path.empty())
        this->m_out_path = root.string();

    return root;
}

const std::string ARC::extract_resource_hash(std::string &path) const
{
//    const static boost::regex pattern(R"(\.\(([a-fA-F0-9]{8})\))");
//    boost::smatch results;
//    std::string out;

//    boost::regex_search(path, results, pattern);

//    out = results[1];

//    if (!out.empty())
//        boost::algorithm::erase_last(path, results[0]);

//    return out;
}

void ARC::ExtractAll(void)
{
    for (int i = 0; i < ARC::m_header.FilesNum; i++)
        Decompress(i);
}

void ARC::Decompress(const u32 id)
{
    const ARCFileHeader& file_header = ARC::m_file_headers.at(id);
    const Bytef*            source = nullptr;
    Pair&                   pair = m_pairVec->at(id);
    uLongf                  decSize = 0;

    decSize = file_header.UncompressedSize;
    pair.cc.resize(decSize);

    source = reinterpret_cast<Bytef*>(reinterpret_cast<u64>(m_container_old->data()) + file_header.pZData);

    if (::uncompress(pair.cc.data(), &decSize, source, file_header.CompressedSize) == Z_OK)
    {
        pair.info.DecSize           = decSize;
        pair.info.isDecompressed    = true;
    }
}

int ARC::Decompress(Pair& sourcePair, Pair& destPair)
{
    CContainer      temp;
    Bytef*          pTemp = nullptr;
    Bytef*          pSrc = nullptr;
    uLongf          decSize = 0;
    int             err;

    if (sourcePair.cc.data() == nullptr)
        return Z_ERRNO;

    /* prepare temp bufer */
    decSize     = sourcePair.info.DecSize;
    temp.resize(decSize);
    pTemp       = reinterpret_cast<Bytef*>( temp.data() );

    /* prepare source */
    pSrc = reinterpret_cast<Bytef*>( sourcePair.cc.data() );

    err = ::uncompress(pTemp, &decSize, pSrc, sourcePair.cc.size());
    if (err == Z_OK) {
        destPair.info.DecSize           = decSize;
        destPair.info.isDecompressed    = true;
        destPair.cc.resize(decSize);
        tools::copyBytes(destPair.cc.data(), temp.data(), decSize);
    }

    return err;

}

int ARC::Compress(const Pair& sourcePair, Pair& destPair)
{
    CContainer      temp;
    Bytef*          pTemp = nullptr;
    const Bytef*    pSrc = nullptr;
    uLongf          compSize = 0;
    int             err;

    if (sourcePair.cc.data() == nullptr)
        return Z_ERRNO;

    /* prepare temp buffer */
    compSize = compressBound(sourcePair.cc.size());
    temp.resize(compSize);
    pTemp = reinterpret_cast<Bytef*>( temp.data() );

    /* prepare source */
    pSrc = reinterpret_cast<Bytef*>( sourcePair.cc.data() );

    //err = compress(pTemp, &compSize, pSrc, sourcePair.cc.size());
    if (err == Z_OK)
    {
        destPair.info.DecSize        = sourcePair.info.DecSize;
        destPair.info.XORLock        = sourcePair.info.XORLock;
        destPair.info.ResourceHash   = sourcePair.info.ResourceHash;

        destPair.cc.resize(compSize);
        tools::copyBytes(destPair.cc.data(), temp.data(), compSize);
        tools::copyBytes(destPair.info.Filename, sourcePair.info.Filename, FNAME_SIZE);
    }

    return err;
}

int ARC::decompress_all(void)
{
    std::vector<boost::thread> threads;
#ifdef ONE_THREAD
    unsigned cores = 1;
#else
    auto cores = boost::thread::hardware_concurrency();
#endif

    threads.reserve(cores);
    m_res.resize(m_file_headers.size());

    // clear queue in case we have anything left
    while(!m_file_headers_index_queue.empty())
        m_file_headers_index_queue.pop();

    for (size_t i = 0; i < m_file_headers.size(); i++)
        m_file_headers_index_queue.push(i);

    for (size_t i = 0; i < cores; i++) {
        boost::thread thread(&ARC::decompress_t, this);

        threads.push_back(boost::move(thread));
    }

    for (auto& thread : threads)
        thread.join();

    for (const auto& err : m_res)
        if (err != Z_OK)
            return err;

    return Z_OK;
}

void ARC::MakeARC(CContainer& container, std::vector<Pair>& vPair, ARCVersion version)
{
    ARCHeader         header;
    std::vector<Pair>   listAfter;
    u32                 finalSize;
    u64                 zDataStart;
    u32                 padding;

    /* Making header */
    /**/    header.FilesNum = vPair.size();
    /**/    if (version != ARCVersion::None)
    /**/    {
    /**/        header.Version = version;
    /**/        padding = getPadding(version);
    /**/    }
    /**/    else
    /**/    {
    /**/        // TODO: fix
    /**/        header.Version = m_header.Version;
    /**/        padding = m_padding;
    /**/    }
    /**/
    /**/    header.Magic = ARCHeader::ARC_MAGIC;
    /**/    //Tools::copyBytes(header.Magic, ARCHeader::ARC_MAGIC, sizeof(header.Magic)); // TODO: BE LE depending on version or arg
    /**/


    /* Compress all data */
    /**/    listAfter.resize(vPair.size());
    /**/    for (u32 i = 0; i < vPair.size(); i++)
    /**/        Compress(vPair.at(i), listAfter.at(i));


    /* Calculate final size */
    /**/    finalSize = sizeof(ARCHeader) + padding + (sizeof(ARCFileHeader) * listAfter.size());
    /**/    finalSize = align(finalSize);
    /**/    zDataStart = finalSize += 16; // padding, maybe decided by version
    /**/    for (const auto& pp : listAfter)
    /**/        finalSize += pp.cc.size();


    /* Copy data to ARC file */
    /**/    container.resize(finalSize, true);
    /**/
    /**/    /* HEADER */
    /**/    tools::copyBytes(container.data(), &header, sizeof(header));
    /**/
    /**/    /* ARC FILES LIST */
    /**/    MakeARC_File_s_Header(container, listAfter, padding, zDataStart);
    /**/
    /**/    /* ZDATA */
    /**/    CopyZData(container, listAfter, zDataStart);
}

void ARC::CopyZData(CContainer& _cc, std::vector<Pair>& _list, u32 _zDataStart)
{
    u32     shift = 0;
    u8*     to = _cc.data() + _zDataStart;

    for (u32 i = 0; i < _list.size(); i++)
    {
        Pair&   pp = _list.at(i);
        u32     ammount;
        u8*     from;

        to          += shift;
        from        = pp.cc.data();
        ammount     = pp.cc.size();

        tools::copyBytes(to, from, ammount);

        shift = ammount;
    }
}

void ARC::MakeARC_File_s_Header(
        CContainer& _cc,
        std::vector<Pair>& _list,
        u32 _padding,
        u32 _zDataStart)
{
    ARCFileHeader*   arc;
    u32                 pZData;

    arc     = reinterpret_cast<ARCFileHeader*>( _cc.data() + sizeof(ARCHeader) + _padding );
    pZData  = _zDataStart;

    for (u32 i = 0; i < _list.size(); i++)
    {
        Pair& pair = _list.at(i);

        tools::copyBytes(&arc[i], pair.info.Filename, FNAME_SIZE);
        arc[i].UncompressedSize = pair.info.DecSize ^ pair.info.XORLock;
        arc[i].CompressedSize   = pair.cc.size();
        arc[i].ResourceHash     = pair.info.ResourceHash;
        arc[i].pZData           = pZData;

        pZData  += pair.cc.size();
    }
}

auto ARC::read_header(void) -> bool
{
    bool isArc = false;
    u32 padding;
    fs::fstream file(m_arc_path, std::ios::in | std::ios::binary);

    if (!file.is_open())
        return false;

    file.read(reinterpret_cast<char*>(&m_header), sizeof(ARCHeader));

    isArc |= m_header.isARC();
    isArc |= m_is_big_endian = m_header.isCRA();

    if (!isArc)
        return false;

    if (m_is_big_endian)
        m_header.BESwap();

    file.read(reinterpret_cast<char*>(&padding), sizeof(padding));

    // if there is no padding, rewind
    if (padding != 0) {
        file.seekg(sizeof(ARCHeader));
        m_padding = 0;
    } else {
        m_padding = 4;
    }

    m_file_headers.reserve(m_header.FilesNum);
    m_file_header_locks.reserve(m_header.FilesNum);

    for (auto i = 0u; i < m_header.FilesNum; i++) {
        m_file_headers.push_back({});

        file.read(reinterpret_cast<char*>(&m_file_headers.back()), sizeof(ARCFileHeader));

        if (m_is_big_endian)
            m_file_headers.back().toLittleEndian();
    }

    return true;
}

auto ARC::isBigEndian(void) const noexcept -> bool { return m_isBE; }

auto ARC::toLittleEndian(void) noexcept -> void
{
    for (auto it = m_arcFileHeaders.begin(); it != m_arcFileHeaders.end(); it++)
        it->toLittleEndian();
}

auto ARC::toBigEndian(void) noexcept -> void
{
    for (auto it = m_arcFileHeaders.begin(); it != m_arcFileHeaders.end(); it++)
        it->toBigEndian();
}

auto ARC::getHeader(void) noexcept -> ARCHeader& { return m_header; }
auto ARC::getHeader(void) const noexcept -> const ARCHeader& { return m_header; }

auto ARC::getARCFileHeaders(void) noexcept -> std::vector<ARCFileHeader>& { return m_arcFileHeaders; }
auto ARC::getARCFileHeaders(void) const noexcept -> const std::vector<ARCFileHeader>& { return m_arcFileHeaders; }

#define CHUNK 16384

/* Decompress from file source to file dest until stream ends or EOF.
   inf() returns Z_OK on success, Z_MEM_ERROR if memory could not be
   allocated for processing, Z_DATA_ERROR if the deflate data is
   invalid or incomplete, Z_VERSION_ERROR if the version of zlib.h and
   the version of the library linked do not match, or Z_ERRNO if there
   is an error reading or writing the files. */
int inf(FILE *source, FILE *dest)
{
    int ret;
    unsigned have;
    z_stream strm;
    unsigned char in[CHUNK];
    unsigned char out[CHUNK];

    /* allocate inflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.avail_in = 0;
    strm.next_in = Z_NULL;
    ret = inflateInit(&strm);
    if (ret != Z_OK)
        return ret;

    /* decompress until deflate stream ends or end of file */
    do {
        strm.avail_in = fread(in, 1, CHUNK, source);
        if (ferror(source)) {
            (void)inflateEnd(&strm);
            return Z_ERRNO;
        }
        if (strm.avail_in == 0)
            break;
        strm.next_in = in;

        /* run inflate() on input until output buffer not full */
        do {
            strm.avail_out = CHUNK;
            strm.next_out = out;
            ret = inflate(&strm, Z_NO_FLUSH);
            assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
            switch (ret) {
            case Z_NEED_DICT:
                ret = Z_DATA_ERROR;     /* and fall through */
            case Z_DATA_ERROR:
            case Z_MEM_ERROR:
                (void)inflateEnd(&strm);
                return ret;
            }
            have = CHUNK - strm.avail_out;
            if (fwrite(out, 1, have, dest) != have || ferror(dest)) {
                (void)inflateEnd(&strm);
                return Z_ERRNO;
            }
        } while (strm.avail_out == 0);

        /* done when inflate() says it's done */
    } while (ret != Z_STREAM_END);

    /* clean up and return */
    (void)inflateEnd(&strm);
    return ret == Z_STREAM_END ? Z_OK : Z_DATA_ERROR;
}

/* Compress from file source to file dest until EOF on source.
   def() returns Z_OK on success, Z_MEM_ERROR if memory could not be
   allocated for processing, Z_STREAM_ERROR if an invalid compression
   level is supplied, Z_VERSION_ERROR if the version of zlib.h and the
   version of the library linked do not match, or Z_ERRNO if there is
   an error reading or writing the files. */
int def(FILE *source, FILE *dest, int level)
{
    int ret, flush;
    unsigned have;
    z_stream strm;
    unsigned char in[CHUNK];
    unsigned char out[CHUNK];

    /* allocate deflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    ret = deflateInit(&strm, level);
    if (ret != Z_OK)
        return ret;

    /* compress until end of file */
    do {
        strm.avail_in = fread(in, 1, CHUNK, source);
        if (ferror(source)) {
            (void)deflateEnd(&strm);
            return Z_ERRNO;
        }
        flush = feof(source) ? Z_FINISH : Z_NO_FLUSH;
        strm.next_in = in;

        /* run deflate() on input until output buffer not full, finish
           compression if all of source has been read in */
        do {
            strm.avail_out = CHUNK;
            strm.next_out = out;
            ret = deflate(&strm, flush);    /* no bad return value */
            assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
            have = CHUNK - strm.avail_out;
            if (fwrite(out, 1, have, dest) != have || ferror(dest)) {
                (void)deflateEnd(&strm);
                return Z_ERRNO;
            }
        } while (strm.avail_out == 0);
        assert(strm.avail_in == 0);     /* all input will be used */

        /* done when last data in file processed */
    } while (flush != Z_FINISH);
    assert(ret == Z_STREAM_END);        /* stream will be complete */

    /* clean up and return */
    (void)deflateEnd(&strm);
    return Z_OK;
}

auto ARCHeader::BESwap(void) noexcept -> void
{
    Version = tools::endianess::swap<ARCVersion>(Version);
    FilesNum = tools::endianess::swap<u16>(FilesNum);
}

auto ARCHeader::isARC(void) const noexcept -> bool
{
    return Magic == ARC_MAGIC;
}

auto ARCHeader::isCRA(void) const noexcept -> bool
{
    return Magic == CRA_MAGIC;
}

auto ARCFileHeader::toLittleEndian(void) noexcept -> void
{
    ResourceHash        = tools::endianess::swap<u32>(ResourceHash);
    CompressedSize      = tools::endianess::swap<u32>(CompressedSize);

    auto bothLE = tools::endianess::swap<u32>(Raw);

    UncompressedSize    = bothLE >> 3;
    Flags               = bothLE & 0x00000007;

    pZData              = tools::endianess::swap<u32>(pZData);
}

auto ARCFileHeader::toBigEndian(void) noexcept -> void
{
    ResourceHash        = tools::endianess::swap<u32>(ResourceHash);
    CompressedSize      = tools::endianess::swap<u32>(CompressedSize);
    Raw                 = tools::endianess::swap<u32>(Flags | UncompressedSize << 3);
    pZData              = tools::endianess::swap<u32>(pZData);
}

auto ARCFileHeader::getFilePath(void) const noexcept -> const char* { return static_cast<const char*>(FilePath); }

auto ARCFileHeader::setFilePath(const std::string &path) noexcept -> bool
{
    if (path.empty())
        return false;

    if (path.length() <= ARC_FILEPATH_MAX) {
        path.copy(FilePath, path.length());

        FilePath[path.length() + 1] = '\0';

        return true;
    }

    return false;

}

auto ARCFileHeader::getFilePathUnix(void) const noexcept -> std::string
{
    std::string out(FilePath);

    std::replace(  out.begin()
                 , out.end()
                 , '\\'
                 , '/');

    return out;
}

auto ARCFileHeader::fixFilePath(void) noexcept -> void
{
    std::replace(  std::begin(FilePath)
                 , std::end(FilePath)
                 , '/'
                 , '\\');
}

}; /// namespace tools
}; /// namespace mh
