#include "libkorra/tools/folder.hpp"

#include <boost/filesystem.hpp>

namespace mh {
namespace tools {

Folder::Folder(const fs::path& path)
{
    if (fs::is_directory(path)) {
        auto prefered = path;

        prefered.make_preferred();

        m_path.assign(prefered);
    }
    // TODO: action if not folder
}

const std::vector<fs::path> Folder::Get_ListOfFiles(const bool recursive) const
{
    std::vector<fs::path> out;

    if (fs::exists(m_path)) {
        if (recursive) {
// TODO: fix
#if BOOST_VERSION == 106000
            for (const auto& entry : fs::recursive_directory_iterator(__path))
#else
            for (const auto& entry : fs::recursive_directory_iterator(m_path, fs::directory_options::skip_permission_denied))
#endif
                if (fs::is_regular_file(entry.path()))
                    out.push_back(entry.path());
        } else {
// TODO: fix
#if BOOST_VERSION == 106000
            for (const auto& entry : fs::directory_iterator(__path))
#else
            for (const auto& entry : fs::directory_iterator(m_path, fs::directory_options::skip_permission_denied))
#endif
                if (fs::is_regular_file(entry.path()))
                    out.push_back(entry.path());
        }
    }

    return out;
}

}; /// namespace tools
}; /// namespace mh
