#include <libkorra/tools/ccontainer.hpp>
#include <libkorra/tools/tools.hpp>
#include <libkorra/tools/file.hpp>

namespace mh {
namespace tools {

CContainer::CContainer()
    : m_root(nullptr)
    , m_data(nullptr)
    , m_size(0)
    , m_reservedBefore(0)
    , m_reservedAfter(0)
{
}

CContainer::~CContainer()
{
    free();
}

CContainer::CContainer(const std::string& path)
    : m_root(nullptr)
    , m_data(nullptr)
    , m_size(0)
    , m_reservedBefore(RESERVED_BEFORE)
    , m_reservedAfter(RESERVED_AFTER)
{
    fs::path prefered{path};

    read(prefered.make_preferred().string());
}

CContainer::CContainer(CContainer& other)
    : CContainer{}
{
    m_reservedBefore    = other.m_reservedBefore;
    m_reservedAfter     = other.m_reservedAfter;

    if (other.size() > 0) {
        (void)resize(other.size(), true);

        tools::copyBytes(  data()
                         , other.data()
                         , other.size());
    }
}

CContainer::CContainer(const CContainer& other)
    : CContainer{}
{
    m_reservedBefore    = other.m_reservedBefore;
    m_reservedAfter     = other.m_reservedAfter;

    if (other.size() > 0) {
        (void)resize(other.size(), true);

        tools::copyBytes(  data()
                         , other.data()
                         , other.size());
    }
}

CContainer::CContainer(CContainer&& other)
    : m_root{other.m_root}
    , m_data{other.m_data}
    , m_size{other.m_size}
    , m_reservedBefore{other.m_reservedBefore}
    , m_reservedAfter{other.m_reservedAfter}
{
    other.m_data            = nullptr;
    other.m_root            = nullptr;
    other.m_size            = 0;
    other.m_reservedBefore  = 0;
    other.m_reservedAfter   = 0;
}

CContainer::CContainer(const std::size_t size)
    : m_root(nullptr)
    , m_data(nullptr)
    , m_reservedBefore(RESERVED_BEFORE)
    , m_reservedAfter(RESERVED_AFTER)
{
    (void)resize(size, true);
}

CContainer::CContainer(const void *data, const std::size_t size)
    : m_root(nullptr)
    , m_data(nullptr)
    , m_reservedBefore(RESERVED_BEFORE)
    , m_reservedAfter(RESERVED_AFTER)
{
    (void)resize(size, true);

    tools::copyBytes(  m_data
                     , data
                     , size);
}

auto CContainer::operator = (CContainer&& other) noexcept -> CContainer&
{
    if (this == &other)
        return *this;

    if (m_root != nullptr)
        std::free(m_root);

    m_data                  = std::move(other.m_data);
    m_root                  = std::move(other.m_root);
    m_size                  = std::move(other.m_size);
    m_reservedBefore        = std::move(other.m_reservedBefore);
    m_reservedAfter         = std::move(other.m_reservedAfter);

    other.m_data            = nullptr;
    other.m_root            = nullptr;
    other.m_size            = 0;
    other.m_reservedBefore  = 0;
    other.m_reservedAfter   = 0;

    return *this;
}

auto CContainer::operator = (const CContainer& other) -> CContainer&
{
    if (this == &other)
        return *this;

    if (m_root != nullptr)
        std::free(m_root);

    m_reservedBefore = other.m_reservedBefore;
    m_reservedAfter = other.m_reservedAfter;

    if (other.size() > 0) {
        (void)resize(other.size(), true);

        tools::copyBytes(  data()
                         , other.data()
                         , other.size());
    } else {
        m_root = nullptr;
        m_data = nullptr;
        m_size = 0;
    }

    return *this;
}

auto CContainer::operator == (const CContainer &other) const noexcept -> bool
{
    if (m_size != other.m_size)
        return false;

    auto res = std::memcmp(  m_data
                           , other.m_data
                           , m_size);

    return res == 0;
}

auto CContainer::swap(CContainer& other) noexcept -> void
{
    std::swap(m_root, other.m_root);
    std::swap(m_data, other.m_data);
    std::swap(m_size, other.m_size);
    std::swap(m_reservedBefore, other.m_reservedBefore);
    std::swap(m_reservedAfter, other.m_reservedAfter);
}

auto CContainer::clear(void) -> void
{
    free();
}

auto CContainer::size(void) const noexcept -> std::size_t
{
    return m_size;
}

auto CContainer::free(void) -> void
{
    if (m_root == nullptr)
        return

    std::free(m_root);

    m_root              = nullptr;
    m_data              = nullptr;
    m_size              = 0;
    m_reservedBefore    = 0;
    m_reservedAfter     = 0;
}

auto CContainer::allocate(const std::size_t size, const bool zeroed) -> bool
{
    if (m_root != nullptr)
        free();

    u32 alloc_size =   size
                     + m_reservedBefore
                     + m_reservedAfter;

    if (zeroed)
        m_root = static_cast<u8*>(std::calloc(alloc_size, sizeof(u8)));
    else
        m_root = static_cast<u8*>(std::malloc(alloc_size));

    if (m_root == nullptr)
        return false;

    m_data = m_root + m_reservedBefore;
    m_size = size;

    return true;
}

auto CContainer::addBefore(std::size_t size) -> bool
{
    if (m_root == nullptr)
        return false;

    if (   size <= m_reservedBefore
        && m_reservedBefore != 0) {
        m_reservedBefore    -= size;
        m_data              -= size;

        m_size              += size;

        return true;
    }

    return false;
}

auto CContainer::addAfter(std::size_t size) -> bool
{
    if (m_root == nullptr)
        return false;

    if (   size <= m_reservedAfter
        && m_reservedAfter != 0) {
        m_reservedAfter -= size;
        m_size          += size;
    } else {
        m_root = static_cast<u8*>(std::realloc(m_root, m_size += size));
        m_data = m_root + m_reservedBefore;
    }

    return true;
}

auto CContainer::subBefore(std::size_t size) -> bool
{
    if (m_root == nullptr)
        return false;

    if (size < m_size) {
        m_reservedBefore += size;
        m_data           += size;
        m_size           -= size;

        return true;
    }

    return false;
}

auto CContainer::subAfter(std::size_t size) -> bool
{
    if (m_root == nullptr)
        return false;

    if (size < m_size) {
        m_reservedAfter += size;
        m_size          -= size;

        return true;
    }

    return false;
}

auto CContainer::resize(std::size_t size, bool zeroed) -> bool
{
    if (m_root == nullptr)
        return allocate(size, zeroed);
    else if (   m_root != nullptr
             && !zeroed)
        return addAfter(size);

    return allocate(size, zeroed);
}

auto CContainer::setDataPointer(u8* pointer) noexcept -> void
{
    m_data = pointer;
}

auto CContainer::setSize(const std::size_t size) noexcept -> void
{
    m_size = size;
}

auto CContainer::read(const std::string& path) -> void
{
    File::file_to_cc(path, *this);
}

auto CContainer::write(const std::string& path) const -> void
{
    File::CC_To_File(path, *this);
}

auto CContainer::data(void) noexcept -> u8* { return m_data; }
auto CContainer::data(void) const noexcept -> u8* { return m_data; }

}; /// namespace tools
}; /// namespace mh
